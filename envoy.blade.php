@servers(['laravel-app' => 'envoy@laravel-app.app.gitlabdemo.cloud'])

@setup
    $env = isset($env) ? $env : "prod";

    if ($env == 'staging') {
        $servers = ['laravel-app'];
        $domain = "staging.gitlabdemo.cloud";
        $path = "staging.gitlabdemo.cloud";
        $branch = isset($branch) ? $branch : "master";
    } elseif ($env == 'prod') {
        $servers = ['laravel-app'];
        $domain = "gitlabdemo.cloud";
        $path = "gitlabdemo.cloud";
        $branch = isset($branch) ? $branch : "master";
    }

    $test = 'Connection successful. Awaiting command!';
    $title = 'GitLab Demo Portal';
    $name = 'gitlab-com/demo-systems/management-apps/gitlabdemo-cloud-app';
    $repo = 'git@gitlab.com:'.$name.'.git';
    $root_dir = '/srv/www';
    $app_dir = $root_dir . '/' . $path;
    $storage_dir = $app_dir . '/storage';
    $framework_dir = $app_dir . '/storage/framework';
    $release_dir = $app_dir . '/releases';
    $app_symlink = $app_dir . '/current';
    $rollback_symlink = $app_dir . '/rollback';
    $release = 'release_' . date('YmdHis');
    $php_version = '7.4'
@endsetup

@task('test', ['on' => $servers, 'parallel' => true])
    #test
    echo {{ $test }}
@endtask

@task('rollback', ['on' => $servers, 'parallel' => true])
    #rollback
    if [[ -L {{ $rollback_symlink }} && -d {{ $rollback_symlink }} ]]
    then
        ln -nfs $(readlink {{ $rollback_symlink }}) {{ $app_symlink }};
        rm {{ $rollback_symlink }};
    fi

    sudo service php{{ $php_version }}-fpm restart;
    sleep 1;
    php {{ $app_symlink }}/artisan queue:restart;
@endtask

@task('deploy', ['on' => $servers, 'parallel' => true])
    #remove-rollback
    if [[ -L {{ $rollback_symlink }} && -d {{ $rollback_symlink }} ]]
    then
        rm -rf $(readlink {{ $rollback_symlink }});
        rm {{ $rollback_symlink }};
    fi

    #fetch-repo
    if [[ ! -d {{ $app_dir }} ]]
    then
        mkdir {{ $app_dir }};
        chmod ug+rwx {{ $app_dir }};
    fi

    if [[ ! -d {{ $storage_dir }} ]]
    then
        mkdir {{ $storage_dir }};
        chmod ug+rwx {{ $storage_dir }};
    fi

    if [[ ! -d {{ $storage_dir }}/app ]]
    then
        mkdir {{ $storage_dir }}/app;
        chmod ug+rwx {{ $storage_dir }}/app;
    fi

    if [[ ! -d {{ $framework_dir }} ]]
    then
        mkdir {{ $framework_dir }};
        chmod ug+rwx {{ $framework_dir }};
    fi

    if [[ ! -d {{ $framework_dir }}/cache ]]
    then
        mkdir {{ $framework_dir }}/cache;
        chmod ug+rwx {{ $framework_dir }}/cache;
    fi

    if [[ ! -d {{ $framework_dir }}/sessions ]]
    then
        mkdir {{ $framework_dir }}/sessions;
        chmod ug+rwx {{ $framework_dir }}/sessions;
    fi

    if [[ ! -d {{ $framework_dir }}/views ]]
    then
        mkdir {{ $framework_dir }}/views;
        chmod ug+rwx {{ $framework_dir }}/views;
    fi

    if [[ ! -d {{ $storage_dir }}/logs ]]
    then
        mkdir {{ $storage_dir }}/logs;
        chmod ug+rwx {{ $storage_dir }}/logs;
    fi

    if [[ ! -d {{ $release_dir }} ]]
    then
        mkdir {{ $release_dir }};
        chmod ug+rwx {{ $release_dir }};
    fi

    cd {{ $release_dir }};
    git clone -b {{ $branch }} --depth=1 {{ $repo }} {{ $release }};

    #add-storage-symlink
    rm -rf {{ $release_dir }}/{{ $release }}/storage;
    cd {{ $release_dir }}/{{ $release }};
    ln -nfs {{ $storage_dir }} storage;

    #add-env-symlink
    cd {{ $release_dir }}/{{ $release }};
    ln -nfs {{ $app_dir }}/.env .env;

    #add-hackystack-config-symlink
    cd {{ $release_dir }}/{{ $release }}/config;
    ln -nfs {{ $app_dir }}/config/demosys.php demosys.php;

    #run-composer
    cd {{ $release_dir }}/{{ $release }};

    ln -nfs {{ $root_dir }}/auth.json auth.json;

    composer install --prefer-dist --no-scripts --no-suggest;

    #run-migrations
    php artisan migrate --force;

    php {{ $release_dir }}/{{ $release }}/artisan clear-compiled --env=production;
    php artisan route:cache --env=production;

    #update-permissions
    cd {{ $release_dir }};
    chmod -R ug+rwx {{ $release }};

    #update-app-symlinks
    if [[ -L {{ $app_symlink }} ]]
    then
        cd $app_dir;
        ln -nfs $(readlink {{ $app_symlink }}) {{ $rollback_symlink }};
    fi

    ln -nfs {{ $release_dir }}/{{ $release }} {{ $app_symlink }};

@endtask
