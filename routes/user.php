<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;

// Redirect any root paths to /dashboard. If the user has not authenticated
// yet, the 'auth' middleware will redirect to the /login route.
Route::get('/', function () {
    return redirect()->route('user.dashboard.index');
})->middleware(['auth']);

Route::middleware(['auth','permission'])->namespace('\App\Http\Controllers\User')->group(function () {

    // Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('user.dashboard.index');

    // Environments
    Route::get('/environments', 'EnvironmentController@index')->name('user.environments.index');
    Route::post('/environments/instances/users', 'EnvironmentInstanceUserController@store')->name('user.environments.instances.users.store');

    // Analytics
    // Route::get('/analytics', 'AnalyticController@index')->name('user.analytics.index');

    // Profile
    Route::get('/account/profile', 'AccountProfileController@index')->name('user.account.profile.index');

});
