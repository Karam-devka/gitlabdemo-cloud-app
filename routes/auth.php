<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

Route::namespace('App\Http\Controllers\Auth')->group(function () {

    // Login Form
    Route::get('/login', 'LoginController@show')->name('auth.login');

    // Logout Action
    Route::post('/logout', 'LogoutController@endSession')->name('auth.logout');

    if(env('DB_DATABASE') != null) {

        try {
            // Get list of enabled SSO providers from database
            $enabled_sso_providers = \App\Models\Auth\AuthProvider::query()
                ->where('slug', '!=', 'local')
                ->where('flag_enabled', 1)
                ->get();

            foreach($enabled_sso_providers as $provider) {

                // Define variable to pass into route group function
                $provider_slug = $provider->slug;

                // Dynamically create routes for login and callback for enabled SSO provider
                Route::get('/login/'.$provider_slug, Str::title($provider_slug).'\LoginController@redirectToProvider')->name('auth.'.$provider_slug.'.login');
                Route::get('/login/'.$provider_slug.'/callback', Str::title($provider_slug).'\LoginController@handleProviderCallback')->name('auth.'.$provider_slug.'.callback');

            }
        } catch(\Exception $e) {
            // Ignore exception
        }

    }

});
