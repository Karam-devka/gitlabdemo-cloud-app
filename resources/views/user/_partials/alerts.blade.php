@if(config('demosys.motd.enabled') == true)
    <div class="alert alert-{{ config('demosys.motd.color') }} page-alert alert-dismissible fade show" role="alert">
        {{ config('demosys.motd.text') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if($errors->any())
    <div class="alert alert-danger page-alert alert-dismissible fade show" role="alert">
        @foreach ($errors->all() as $error)
            {{ $error }}<br />
        @endforeach
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if($request->session()->exists('alert-success'))
    <div class="alert alert-success page-alert alert-dismissible fade show" role="alert">
        {{ $request->session()->get('alert-success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if($request->session()->exists('alert-warning'))
    <div class="alert alert-warning page-alert alert-dismissible fade show" role="alert">
        {{ $request->session()->get('alert-warning') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if($request->session()->exists('alert-info'))
    <div class="alert alert-info page-alert alert-dismissible fade show" role="alert">
        {{ $request->session()->get('alert-info') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if($request->session()->exists('alert-danger'))
    <div class="alert alert-danger page-alert alert-dismissible fade show" role="alert">
        {{ $request->session()->get('alert-danger') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
