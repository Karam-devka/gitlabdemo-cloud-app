<footer class="c-footer">
    <div>
        @if(config('demosys.footer.copyright'))
            {{ config('demosys.footer.copyright') }}<br />
        @endif
    </div>
    <div class="ml-auto text-right align-middle">
        @if(config('demosys.footer.support_instructions'))
            {{ config('demosys.footer.support_instructions') }}<br />
        @endif
        @if(config('demosys.footer.documentation_link'))
            <a class="mr-1" href="{{ config('demosys.footer.documentation_link') }}">Documentation</a>|
        @endif
        @if(config('demosys.footer.support_link'))
            <a class="ml-1" href="{{ config('demosys.footer.support_link') }}">Support</a>
        @endif
    </div>
</footer>
