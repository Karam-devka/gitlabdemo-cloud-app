<div class="c-wrapper">
    <header class="c-header c-header-light c-header-fixed">

        <a class="c-header-brand mx-3" href="{{ route('user.dashboard.index') }}">
            <img src="{{ asset('assets/'.config('demosys.app.logo')) }}" style="height: 50px;" alt="">
        </a>
        <div class="d-flex align-items-center mr-4">
            <h4 class="mt-1" style="font-weight: 500;">{{ config('demosys.app.name') }}</h4>
        </div>

        <ul class="c-header-nav">
            @if(in_array('user.dashboard.index', $request->user()->permissions))
                <li class="c-header-nav-item px-3">
                    <a class="c-header-nav-link {{ Request::is('dashboard*') ? 'c-active' : '' }}" href="{{ route('user.dashboard.index') }}">
                        <i class="ri-dashboard-fill mr-2"></i>
                        <span class="d-md-down-none">Dashboard</span>
                    </a>
                </li>
            @endif
            @if(in_array('user.environments.index', $request->user()->permissions))
                <li class="c-header-nav-item px-3">
                    <a class="c-header-nav-link {{ Request::is('environments*') ? 'c-active' : '' }}" href="{{ route('user.environments.index') }}">
                        <i class="ri-code-box-fill mr-2"></i>
                        <span class="d-md-down-none">Environments</span>
                    </a>
                </li>
            @endif
            {{--
            @if(in_array('user.analytics.index', $request->user()->permissions))
                <li class="c-header-nav-item px-3">
                    <a class="c-header-nav-link {{ Request::is('analytics*') ? 'c-active' : '' }}" href="{{ route('user.analytics.index') }}">
                        <i class="ri-bar-chart-2-fill mr-2"></i>
                        <span class="d-md-down-none">Usage Analytics</span>
                    </a>
                </li>
            @endif
            --}}
        </ul>

        <ul class="c-header-nav ml-auto mr-4">
            <li class="c-header-nav-item dropdown">
                <a class="c-header-nav-link {{ Request::is('account*') ? 'c-active' : '' }}" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="c-avatar">
                        <img class="c-avatar-img mr-3" src="{{ 'https://www.gravatar.com/avatar/'.md5(strtolower(trim($request->user()->email))).'?d='.urlencode(asset('hackystack-avatar-default.png')).'&s=50' }}" alt="Avatar">
                    </div>
                    {{ $request->user()->full_name ? $request->user()->full_name : 'My User Account' }}
                    <i class="ri-arrow-drop-down-line ml-1"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right pt-2 mt-4">
                    @if(in_array('user.account.profile.index', $request->user()->permissions))
                        <a class="dropdown-item {{ Request::is('account/profile*') ? 'active' : '' }}" href="{{ route('user.account.profile.index') }}">
                            <i class="ri-user-settings-line mr-2"></i>
                            User Profile
                        </a>
                    @endif
                    <div class="dropdown-divider"></div>
                    <form action="{{ route('auth.logout') }}" id="logout" method="POST">
                        @csrf
                        <button class="dropdown-item" type="submit">
                            <i class="ri-logout-box-r-line mr-2"></i>
                            Logout
                        </button>
                    </form>
                </div>
            </li>
        </ul>
    </header>
