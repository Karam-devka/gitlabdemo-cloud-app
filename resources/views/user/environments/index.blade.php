@extends('user._layouts.core')

@section('content')

    <div class="card">
        <div class="card-header border-bottom-0 d-flex">
            <div class="flex-grow-1">
                <h5 class="mb-0 font-weight-bold">Environment Instances</h5>
            </div>
            <div class="">
                &nbsp;
            </div>
        </div>
        @if($available_instances->count() == 0 && $provisioned_instances->count() ==0)
            <div class="card-body">
                <div class="alert alert-info">
                    You do not currently have access to any environment instances.
                </div>
            </div>
        @else
            <table class="table mb-0">
                @foreach($provisioned_instances as $provisioned_instance)
                    @foreach($provisioned_instance->environmentInstanceUsers as $provisioned_instance_user)
                        @if($provisioned_instance->type == 'gitlab')
                            <tr>
                                <td style="width: 10%;"><code>{{ $provisioned_instance->short_id }}</code></td>
                                <td style="width: 50%;">
                                    {{ $provisioned_instance->name }}
                                    @if($provisioned_instance->authUsers()->count() > 1)
                                        <span class="badge badge-primary mr-1">Group</span>
                                    @endif
                                    <br />
                                    @if($provisioned_instance->user_base_url)
                                        <small><a href="{{ $provisioned_instance->user_base_url }}">{{ $provisioned_instance->user_base_url }}</a></small>
                                    @else
                                        <span class="badge badge-secondary">{{ $provisioned_instance->slug }}</span>
                                    @endif
                                </td>
                                <td class="d-flex flex-row-reverse buttons" style="text-align: right; vertical-align: middle; ">
                                    <div class="btn-toolbar">
                                        <div class="btn-group m-2">

                                            <a class="btn btn-outline-primary" style="display: flex;" data-toggle="modal" data-target="#credentials-{{ $provisioned_instance_user->short_id }}" href="">
                                                <i class="ri-shield-user-line ri-lg mr-1"></i> View Credentials
                                            </a>
                                            @if($provisioned_instance->user_base_url)
                                                <a class="btn btn-outline-primary" style="display: flex;" target="_blank" data-toggle="tooltip" data-placement="top" title="Access Instance" data-original-title="Access Instance" href="{{ $provisioned_instance->user_base_url }}">
                                                    <i class="ri-external-link-line ri-lg mr-1"></i> Access Instance
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
                @foreach($available_instances as $available_instance)
                    @if($available_instance->type == 'gitlab')
                        <tr>
                            <td style="width: 10%;"><code>{{ $available_instance->short_id }}</code></td>
                            <td style="width: 50%;">
                                {{ $available_instance->name }}
                                @if($available_instance->authUsers()->count() > 1)
                                    <span class="badge badge-primary mr-1">Group</span>
                                @endif
                                <br />
                                @if($available_instance->user_base_url)
                                    <small>{{ $available_instance->user_base_url }}</small>
                                @else
                                    <span class="badge badge-secondary">{{ $available_instance->slug }}</span>
                                @endif
                            </td>
                            <td class="d-flex flex-row-reverse buttons" style="text-align: right; vertical-align: middle; ">
                                <div class="btn-toolbar">
                                    <div class="btn-group m-2">
                                        <form method="POST" action="{{ route('user.environments.instances.users.store') }}">
                                            @csrf
                                            <input type="hidden" name="environment_instance_id" value="{{ $available_instance->id }}" />
                                            <button type="submit" class="btn btn-outline-success" style="display: flex;">
                                                <i class="ri-shield-user-line ri-lg mr-1"></i> Generate Credentials
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </table>

        @endif

    </div>

    @foreach($provisioned_instances as $environment_instance)
        @foreach($environment_instance->environmentInstanceUsers as $environment_instance_user)

            {{-- Include the modal and pass variable through with user credentials --}}
            @include('user.environments.instances._modals.view_credentials', [
                'environment_instance' => $environment_instance,
                'environment_instance_user' => $environment_instance_user
            ])

        @endforeach
    @endforeach

@endsection

@section('javascript')

@endsection
