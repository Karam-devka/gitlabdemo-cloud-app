<div class="modal fade" id="credentials-{{ $environment_instance_user->short_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Instance User Credentials</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Environment Instance</label>
                    <div class="col-md-9 col-form-label">
                        {{ $environment_instance->name }}<br />
                        <span class="badge badge-primary">{{ $environment_instance->short_id }}</span>
                        <span class="badge badge-primary">{{ $environment_instance->slug }}</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">User Profile</label>
                    <div class="col-md-9 col-form-label">
                        {{ $environment_instance_user->authUser->full_name }}
                    </div>
                </div>

                <hr />

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Instance URL</label>
                    <div class="col-md-9 col-form-label">
                        @if($environment_instance->user_base_url != null)
                            <a target="_blank" href="{{ $environment_instance->user_base_url }}">{{ $environment_instance->user_base_url }}</a>
                        @else
                            No URL has been configured for this environment instance.
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Username</label>
                    <div class="col-md-9 col-form-label">
                        @if($environment_instance_user->instance_username != null)
                            <code>{{ decrypt($environment_instance_user->instance_username) }}</code>
                        @else
                            No username has been configured for this user.
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Password</label>
                    <div class="col-md-9 col-form-label">
                        @if($environment_instance_user->instance_password != null)
                            <code>{{ decrypt($environment_instance_user->instance_password) }}</code>
                        @else
                            No password has been configured for this user.
                        @endif
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
