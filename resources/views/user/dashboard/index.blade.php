@extends('user._layouts.core')

@section('content')

    <div class="card-deck">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mb-3">Shared Environment</h5>
                <p class="card-text">Fully managed shared Omnibus instances, runners and clusters. You can import projects into your group and run pipelines with similar features to GitLab.com. You cannot change instance-level settings.</p>
                <p class="card-text"><small class="text-muted">Recommended for ease of use</small></p>
                <div class="d-flex justify-content-center">
                    <a class="btn btn-primary mr-3" href="{{ route('user.environments.index') }}">View Environments</a>
                    <a class="btn btn-outline-info" target="_blank" href="https://about.gitlab.com/handbook/customer-success/demo-systems/environments">Read the Docs</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mb-3">Isolated Environment</h5>
                <p class="card-text">Your own isolated Omnibus instance, runners, and cluster that you control using a GitLab infrastructure-as-code project and a CI pipeline with easy buttons for running Terraform, Ansible, and kubectl commands.</p>
                <p class="card-text"><small class="text-muted">Recommended for admin and infrastructure controls</small></p>
                <div class="d-flex justify-content-center">
                    <a class="btn btn-secondary mr-3 disabled" disabled href="#">Coming Soon</a>
                    <a class="btn btn-outline-info" target="_blank" href="https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gitlab/gitlab-omnibus-sandbox-tf-module">Read the Docs</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mb-3">Sandbox Cloud</h5>
                <p class="card-text">Self service AWS account and/or GCP project to build what you want, how you want. Crowd sourced community support and tutorials to get you started. We handle the authentication and billing for you.</p>
                <p class="card-text"><small class="text-muted">Recommended for custom use cases</small></p>
                <div class="d-flex justify-content-center">
                    <a class="btn btn-primary mr-3" target="_blank" href="{{ route('user.environments.index') }}">Manage Accounts</a>
                    <a class="btn btn-outline-info" target="_blank" href="https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/">Read the Docs</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
