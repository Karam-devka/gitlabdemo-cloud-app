@extends('user._layouts.core')

@section('content')

    <div class="row">
        <div class="col-6">

            @if($auth_user->authProvider->slug == 'local')

                <!-- TODO User Account -->

                <!-- TODO Change Password -->


                <!-- TODO 2FA for Local Accounts -->

            @endif

            @if($auth_user->authProvider->slug != 'local')

                <!-- My User Account -->
                <div class="card">
                    <div class="card-header d-flex">
                        <div class="flex-fill flex-grow-1 card-title">
                            My User Profile
                        </div>
                        <div class="flex-fill text-right">
                            <code>ID {{ $request->user()->short_id }}</code>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-info">
                            Your user profile is managed by the SSO provider and cannot be changed in this application.
                        </div>
                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label font-weight-bold">Name</label>
                                <div class="col-md-9 col-form-label">{{ $request->user()->full_name }}</div>
                            </div>
                            @if($request->user()->job_title)
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label font-weight-bold">Job Title</label>
                                    <div class="col-md-9 col-form-label">{{ $request->user()->job_title }}</div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label font-weight-bold">Email</label>
                                <div class="col-md-9 col-form-label">{{ $request->user()->email }}</div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END My User Account -->

                <!-- Provider Meta Data -->
                <div class="card">
                    <div class="card-header d-flex">
                        <div class="flex-fill flex-grow-1 card-title">
                            Meta Data (Personal Information Disclosure)
                        </div>
                        <div class="flex-fill text-right">
                            <code>{{ Str::title($auth_user->authProvider->slug) }}</code>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>The following metadata was provided by {{ Str::title($auth_user->authProvider->slug) }} when you signed in. We use this information for managing group access, automated notifications, manager approval and cost center statistics reporting.</p>
                        <div class="card">
                            <div class="card-body">
                                <code>
                                    @foreach($provider_meta_data as $meta_data_key => $meta_data_value)
                                        {{ $meta_data_key }}: {{ $meta_data_value }}<br />
                                    @endforeach
                                </code>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Provider Meta Data -->

            @endif

        </div>
        <div class="col-6">

            <!-- My Groups -->
            <div class="card">
                <div class="card-header">
                    <div class="card-title">My Groups</div>
                </div>
                <div class="card-body">
                    <div class="list-group">
                        @foreach($auth_user->authGroups()->orderBy('name')->get() as $group)
                            <div class="list-group-item">
                                <div class="d-flex">
                                    <div class="flex-fill flex-grow-1 font-weight-bold">
                                        {{ $group->name }}
                                    </div>
                                    <div class="">
                                        <span class="badge badge-secondary">{{ Str::title($group->namespace) }}</span>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="flex-fill">
                                        <code class="small">{{ $group->slug }}</code>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- END My Groups -->

        </div>
    </div>

@endsection

@section('javascript')

@endsection
