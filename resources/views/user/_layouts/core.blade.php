<!DOCTYPE html>

<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="HackyStack is an open source cloud infrastructure management and orchestration platform for ephemeral demo, sandbox, testing, and training environments and labs.">
    <meta name="keyword" content="hackystack,open,source,infrastructure,terraform,devops,management,orchestration,labs,ephemeral,demo,sandbox,testing,training,environment,aws,gcp">
    <title>{{ config('demosys.app.name') }}</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/coreui.css') }}" rel="stylesheet">
    <link href="{{ asset('css/coreui-chartjs.css') }}" rel="stylesheet">
    <link href="{{ asset('fonts/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('fonts/inter.css') }}" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics-->
    @if(config('demosys.analytics.google.enabled'))
        <script async="" src="https://www.googletagmanager.com/gtag/js?id={{ config('demosys.analytics.google.tracking_id') }}"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', {{ config('demosys.analytics.google.tracking_id') }});
        </script>
    @endif

</head>
<body class="c-app">

    <div class="c-wrapper">
        @include('user._partials.header')

        <div class="c-body">
            <main class="c-main">
                <div class="container">
                    <div class="container-fluid">
                        <div class="fade-in">
                            @include('user._partials.alerts')
                            @yield('content')
                        </div>
                    </div>
                </div>
            </main>
        </div>

        @include('user._partials.footer')
    </div>

    <script src="{{ asset('js/coreui.bundle.min.js') }}"></script>
    <script src="{{ asset('js/coreui-chartjs.bundle.js') }}"></script>

    <script>
        $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    @yield('javascript')

</body>
</html>
