@extends('auth._layouts.core')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-{{ $enabled_sso_providers->count() > 0 ? '8' : '6' }} col-md-{{ $enabled_sso_providers->count() > 0 ? '11' : '9' }}">
                <div class="card-group">
                    <div class="card p-4">
                        <div class="card-header">
                            <div class="text-center mb-2">
                                <img src="{{ asset('assets/'.config('demosys.app.logo')) }}" style="height: 50px;" alt="">
                            </div>
                            <div class="text-center mb-3">
                                <h2 class="mb-0">{{ config('demosys.app.name') }}</h2>
                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Sign In</h3>
                            <p class="text-muted mb-4">Please use your credentials to access your account.</p>
                            <form method="POST" action="{{ route('auth.local.login.authenticate') }}">
                                @csrf
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="ri-shield-user-fill ri-xl"></i>
                                        </span>
                                    </div><!-- input-group-prepend -->
                                    <input class="form-control form-control-lg" type="text" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autofocus>
                                </div><!-- input-group -->
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="ri-lock-password-fill ri-xl"></i>
                                        </span>
                                    </div><!-- input-group-prepend -->
                                    <input class="form-control form-control-lg" type="password" placeholder="{{ __('Password') }}" name="password" required>
                                </div><!-- input-group -->
                                <div class="row">
                                    <div class="col-6">
                                        <button class="btn btn-primary btn-lg px-4" type="submit">{{ __('Login') }}</button>
                                    </div><!-- col -->
                                    <div class="col-6 text-right">
                                        @if(config('demosys.auth.local.register_enabled') == true)
                                            <a href="{{ route('auth.local.register.create') }}">{{ __('Create Account') }}</a><br />
                                        @endif
                                        @if(config('demosys.auth.local.reset_enabled') == true)
                                            <a href="{{ route('auth.local.reset.create') }}">{{ __('Forgot Password') }}</a><br />
                                        @endif
                                    </div><!-- col -->
                                </div><!-- row -->
                            </form>
                        </div><!-- card-body -->
                    </div><!-- card -->
                    @if($enabled_sso_providers->count() > 0)
                        <div class="card py-2 d-md-down-none" style="flex: 0 0 35%;">
                            <div class="card-body text-center align-middle d-flex align-items-center">
                                <div class="list-group w-100">
                                    {{-- Loop through all SSO providers --}}
                                    @foreach($enabled_sso_providers as $provider)
                                        <a class="list-group-item list-group-item-action" href="{{ route('auth.'.$provider->slug.'.login') }}">
                                            <span class="ri {{ config('demosys.auth.'.$provider->slug.'.icon') }} ri-fw mr-1"></span>
                                            {{ config('demosys.auth.'.$provider->slug.'.human_name') }}
                                        </a>
                                    @endforeach
                                    {{-- End of SSO providers --}}
                                </div><!-- list-group -->
                            </div><!-- card-body -->
                        </div><!-- card -->
                    @endif
                </div><!-- card-group -->
            </div><!-- col -->
        </div><!-- row -->
        <div class="d-flex flex-column text-center mt-3">
            @include('auth._partials.footer')
        </div><!-- d-flex -->
    </div><!-- container -->

@endsection

@section('javascript')

@endsection
