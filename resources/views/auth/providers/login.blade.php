@extends('auth._layouts.core')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card-group">
                    <div class="card p-4">
                        <div class="card-header">
                            <div class="text-center mb-2">
                                <img src="{{ asset('assets/'.config('demosys.app.logo')) }}" style="height: 50px;" alt="">
                            </div>
                            <div class="text-center mb-3">
                                <h2 class="mb-0">{{ config('demosys.app.name') }}</h2>
                            </div>
                        </div>
                        <div class="card-body">
                            <h3>Sign In</h3>
                            <p class="text-muted mb-4">You can use single sign on to access your account.</p>
                            <div class="list-group text-center w-100">
                                {{-- Loop through all SSO providers --}}
                                @foreach($enabled_sso_providers as $provider)
                                    <a class="list-group-item list-group-item-action" href="{{ route('auth.'.$provider->slug.'.login') }}">
                                        <span class="ri {{ config('demosys.auth.'.$provider->slug.'.icon') }} ri-fw mr-1"></span>
                                        {{ config('demosys.auth.'.$provider->slug.'.human_name') }}
                                    </a>
                                @endforeach
                                {{-- End of SSO providers --}}
                            </div><!-- list-group -->
                        </div><!-- card-body -->
                    </div><!-- card -->
                </div><!-- card-group -->
            </div><!-- col -->
        </div><!-- row -->
        <div class="d-flex flex-column text-center mt-3">
            @include('auth._partials.footer')
        </div><!-- d-flex -->
    </div><!-- container -->

@endsection

@section('javascript')

@endsection
