@extends('errors._layouts.core')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="clearfix">
                    <h1 class="float-left display-3 mr-4"><i class="ri ri-git-pull-request-line"></i></h1>
                    <h4 class="pt-3 text-success">System Upgrade in Progress</h4>
                    <p class="text-muted">This merge request is worth the wait. Please try again later.</p>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
