<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class BaseModel extends EloquentModel
{

    use SoftDeletes;

    // Disable auto incrementing ID since we use UUID
    public $incrementing = false;

    // Since primary key is not an integer, set to a string
    protected $keyType = 'string';

    // Hide these fields from arrays
    protected $hidden = [];

    // Append these fields to arrays
    //protected $appends = [''];

    // Run whenever a new model is instantiated
    protected static function boot()
    {
        parent::boot();

        // Attach to the 'creating' Model Event to provide a UUID
        // for the `uuid` field and set created_by user ID
        static::creating(function ($model) {
            $generated_uuid = (string) Str::uuid();
            $model->id = $generated_uuid;
            $model->short_id = substr($generated_uuid, 0, 8);
            $model->created_by = Auth::check() ? Auth::user()->id : null;
            $model->state = 'active';
        });

        if(! method_exists(static::class, 'getHasSoftDeletesDates')) {
            return;
        }

        // Attach to the 'updating' Model Event to set updated_by user ID
        static::updating(function ($model) {
            $model->updated_by = Auth::check() ? Auth::user()->id : null;
        });

        // Attach to the 'deleting' Model Event to set deleted_by user ID
        static::deleting(function ($model) {
            $model->deleted_by = Auth::check() ? Auth::user()->id : null;
            $model->state = 'deleted';
        });

    }

    //
    // Validation Rules
    // ------------------------------------------------------------------------
    // We define arrays with the validation rules that are used to validate
    // form request data in the store() and update() methods of a controller.
    // In basic Laravel tutorials, you will see that validation rules can be
    // added directly in the controller method, however we have moved them
    // to the model to allow reusability throughout the Laravel application,
    // including in different controllers (user and admin for the same model),
    // in services, and in API requests.
    // https://laravel.com/docs/6.x/validation#available-validation-rules
    //
    // The difference between storeRules and validateRules is that some fields
    // may be required on store (ex. a name field) but not required (nullable)
    // when the record is updated. Our standard practice is that any field that
    // is null/nullable will use the existing value (if set) or default value.
    //
    // To access model variables, we need to create a function that can be
    // accessed when the model is instantiated.
    //
    // To validate a request matches the rules, simply add this line at the
    // top of the controller method. The request will be redirected to the
    // referring page and no additional lines of that method will execute.
    //
    // use Illuminate\Support\Facades\Validator;
    // use App\Models;
    //
    // Creating a new record [without a $this->model() defined]
    // $validator = Validator::make(
    //         $request->all(),
    //         Models\ModelName::$storeRules,
    //         Models\ModelName::$validationMessages
    //     )->validate();
    //
    // Updating a record [without a $this->model() defined]
    // $validator = Validator::make(
    //         $request->all(),
    //         Models\ModelName::$updateRules,
    //         Models\ModelName::$validationMessages
    //     )->validate();
    //
    // Creating a new record [with a $this->model() defined]
    // $validator = Validator::make(
    //         $request->all(),
    //         $this->model()->storeRules(),
    //         $this->model()->validationMessages()
    //     )->validate();
    //
    // Updating a record [with a $this->model() defined]
    // $validator = Validator::make(
    //         $request->all(),
    //         $this->model()->updateRules(),
    //         $this->model()->validationMessages()
    //     )->validate();
    //
    //

    public $storeRules = [];
    public $updateRules = [];
    public $validationMessages = [];

    public function storeRules()
    {
        return $this->storeRules;
    }

    public function updateRules()
    {
        return $this->updateRules;
    }

    public function validationMessages()
    {
        return $this->validationMessages;
    }

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically mutated to timestamps
    // and formatted as datetime. These fields have the added benefit of being
    // Carbon date objects so you do not need to Carbon::parse($value),
    // and can use $record->created_at->addDay().
    //
    // This variable is usually overriden in each model.
    //

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

    //
    // Scopes
    // ------------------------------------------------------------------------
    // We can use scope with Eloquent queries to filter results more easily
    // than unnecessary if() statements.
    //

    public function scopeHasHistoricalDateLessThanDays($query, $date_field, $days) {

        // Calculate a date to check against using Carbon time
        $calculated_date = now()->subDays($days);

        return $query->where($date_field, '>', $calculated_date);
    }

    public function scopeHasHistoricalDateGreaterThanDays($query, $date_field, $days) {

        // Calculate a date to check against using Carbon time
        $calculated_date = now()->subDays($days);

        return $query->where($date_field, '<', $calculated_date);
    }

    public function scopeHasFutureDateLessThanDays($query, $date_field, $days) {

        // Calculate a date to check against using Carbon time
        $calculated_date = now()->addDays($days);

        return $query->where($date_field, '<', $calculated_date);
    }

    public function scopeHasFutureDateGreaterThanDays($query, $date_field, $days) {

        // Calculate a date to check against using Carbon time
        $calculated_date = now()->addDays($days);

        return $query->where($date_field, '>', $calculated_date);
    }

    //
    // Custom Arrays, Helpers and Variables
    // ------------------------------------------------------------------------
    // When it's not appropriate to implement a full trait for something that
    // is used across multiple models, you can define it here.
    //

}
