<?php

namespace App\Models\Auth;

use App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class AuthUser extends Authenticatable
{
    use SoftDeletes;

    public $table = 'auth_users';

    //
    // The methods below are included in the BaseModel, however since
    // this is extending the Authenticatable class, we have copied them
    // here to avoid dependency injection issues.
    //

    // Disable auto incrementing ID since we use UUID
    public $incrementing = false;

    // Since primary key is not an integer, set to a string
    protected $keyType = 'string';

    // Hide these fields from arrays
    protected $hidden = [];

    // Append these fields to arrays
    protected $appends = [];

    // Run whenever a new model is instantiated
    protected static function boot()
    {
        parent::boot();

        // Attach to the 'creating' Model Event to provide a UUID
        // for the `uuid` field and set created_by user ID
        static::creating(function ($model) {
            $generated_uuid = (string) Str::uuid();
            $model->id = $generated_uuid;
            $model->short_id = substr($generated_uuid, 0, 8);
            $model->created_by = Auth::check() ? Auth::user()->id : null;
            $model->state = 'active';
        });

        if(! method_exists(static::class, 'getHasSoftDeletesDates')) {
            return;
        }

        // Attach to the 'updating' Model Event to set updated_by user ID
        static::updating(function ($model) {
            $model->updated_by = Auth::check() ? Auth::user()->id : null;
        });

        // Attach to the 'deleting' Model Event to set deleted_by user ID
        static::deleting(function ($model) {
            $model->deleted_by = Auth::check() ? Auth::user()->id : null;
            $model->state = 'deleted';
        });

    }

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    protected $fillable = [
        'auth_tenant_id',
        'auth_provider_id',
        'full_name',
        'job_title',
        'organization_name',
        'email',
        'email_recovery',
        'password',
        'remember_token',
        'provider_meta_data',
        'provider_token', // Security audit
        '2fa_token', // Security audit
        'phone_number',
        'phone_country_code',
        'count_current_failed_logins',  // Security audit
        'timezone',
        'flag_passwordless',  // Security audit
        'flag_2fa_enabled',  // Security audit
        'flag_must_change_password',
        'flag_account_expired',  // Security audit
        'flag_account_locked',  // Security audit
        'flag_account_verified',  // Security audit
        'flag_terms_accepted',
        'flag_privacy_accepted',
        'expires_at',  // Security audit
        'locked_at',  // Security audit
        'verified_at',  // Security audit
        'terms_accepted_at',  // Security audit
        'privacy_accepted_at', // Security audit
        'password_changed_at', // Security audit
        'last_successful_login_at', // Security audit
        'last_failed_login_at', // Security audit
        'last_activity_at', // Security audit
        'state',
    ];

    //
    // Form Validation Rules
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/validation#available-validation-rules
    // See additional documentation in App\Models\BaseModel about how we use
    // form validation rules and implementation examples.
    //

    // TODO reevaluate copy/paste of these

    public $storeRules = [
        'auth_tenant_id' => 'required|uuid|exists:auth_tenants,id',
        'auth_provider_id' => 'required|uuid|exists:auth_providers,id',
        'full_name' => 'required|max:55', // TODO fix regex to allow spaces - regex:/^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$/
        'job_title' => 'nullable|string|max:255',
        'organization_name' => 'nullable|string|max:55',
        'email' => 'required|email|unique:auth_users,email',
        'email_recovery' => 'nullable|email',
        'provider_meta_data' => 'nullable|array',
        'provider_token' => 'nullable|string',
        'phone_number' => 'nullable|numeric|digits_between:4,14',
        'phone_country_code' => 'nullable|numeric|digits_between:1,3|required_with:phone_number',
        'expires_at' => 'nullable|date',
        'password' => 'nullable|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        'timezone' => 'nullable|timezone',
        'flag_terms_accepted' => 'nullable|integer|between:0,1',
        'flag_privacy_accepted' => 'nullable|integer|between:0,1',
        'notify_send_verification_email' => 'nullable|integer|between:0,1',
        'notify_send_temp_password_email' => 'nullable|integer|between:0,1|required_without:password'
    ];

    public $updateRules = [
        'auth_tenant_id' => 'nullable|uuid|exists:auth_tenants,id',
        'auth_provider_id' => 'nullable|uuid|exists:auth_providers,id',
        'full_name' => 'nullable|max:55', // TODO fix regex to allow spaces - regex:/^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$/
        'job_title' => 'nullable|string|max:255',
        'organization_name' => 'nullable|string|max:55',
        'email' => 'nullable|email',
        'email_recovery' => 'nullable|email',
        'provider_meta_data' => 'nullable|array',
        'provider_token' => 'nullable|string',
        'phone_number' => 'nullable|numeric|digits_between:4,14',
        'phone_country_code' => 'nullable|numeric|digits_between:1,3|required_with:phone_number',
        'expires_at' => 'nullable|date',
        'password' => 'nullable|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        'timezone' => 'nullable|timezone',
        'flag_2fa_enabled' => 'nullable|integer|between:0,1',
        'flag_must_change_password' => 'nullable|integer|between:0,1',
        'flag_account_expired' => 'nullable|integer|between:0,1',
        'flag_account_locked' => 'nullable|integer|between:0,1',
        'flag_account_verified' => 'nullable|integer|between:0,1',
        'flag_terms_accepted' => 'nullable|integer|between:0,1',
        'flag_privacy_accepted' => 'nullable|integer|between:0,1',
        'action_set_temporary_password' => 'nullable|integer|between:0,1',
        'notify_send_temp_password_email' => 'nullable|integer|between:0,1',
        'notify_send_password_change_email' => 'nullable|integer|between:0,1',
        'notify_send_verification_email' => 'nullable|integer|between:0,1'
    ];

    //
    // If the default error messages are not friendly enough for users, you
    // can set a custom error message for each field and condition not met.
    // https://laravel.com/docs/6.x/validation#custom-error-messages
    //

    public $validationMessages = [
        // 'field.condition' => 'This is a custom error message.'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/eloquent-mutators#attribute-casting
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response. This is not
    // required since the database column type is used by default, however we
    // want to be explicit for engineering clarity.
    //

    protected $casts = [
        // Common fields
        'id' => 'string',
        'created_at' => 'datetime',
        'created_by' => 'string',
        'updated_at' => 'datetime',
        'updated_by' => 'string',
        'deleted_at' => 'datetime',
        'deleted_by' => 'string',
        // Model specific fields
        'auth_tenant_id' => 'string',
        'auth_provider_id' => 'string',
        'full_name' => 'string',
        'job_title' => 'string',
        'organization_name' => 'string',
        'email' => 'string',
        'email_recovery' => 'string',
        'password' => 'string',
        'remember_token' => 'string',
        'provider_meta_data' => 'array',
        'provider_token' => 'string',
        '2fa_token' => 'string',
        'phone_number' => 'integer',
        'phone_country_code' => 'integer',
        'count_current_failed_logins' => 'integer',
        'timezone' => 'string',
        'flag_passwordless' => 'integer',
        'flag_2fa_enabled' => 'integer',
        'flag_must_change_password' => 'integer',
        'flag_account_expired' => 'integer',
        'flag_account_locked' => 'integer',
        'flag_account_verified' => 'integer',
        'flag_terms_accepted' => 'integer',
        'flag_privacy_accepted' => 'integer',
        'expires_at' => 'datetime',
        'locked_at' => 'datetime',
        'verified_at' => 'datetime',
        'terms_accepted_at' => 'datetime',
        'privacy_accepted_at' => 'datetime',
        'password_changed_at' => 'datetime',
        'last_successful_login_at' => 'datetime',
        'last_failed_login_at' => 'datetime',
        'status' => 'string',
        // Custom mutators
        'permissions' => 'array',
        'groups' => 'array',
        'roles' => 'array',
        'username' => 'string',
    ];

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //

    protected $dates = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'expires_at' => 'datetime',
        'locked_at' => 'datetime',
        'verified_at' => 'datetime',
        'terms_accepted_at' => 'datetime',
        'privacy_accepted_at' => 'datetime',
        'password_changed_at' => 'datetime',
        'last_successful_login_at' => 'datetime',
        'last_failed_login_at' => 'datetime',
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`packageWidget`). For a child
    // or many-to-many relationship, use a plural name (`packageWidgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    public function authTenant() {
        return $this->belongsTo(AuthTenant::class, 'auth_tenant_id');
    }

    public function authProvider() {
        return $this->belongsTo(AuthProvider::class, 'auth_provider_id');
    }

    public function createdBy() {
        return $this->belongsTo(AuthUser::class, 'created_by');
    }

    public function updatedBy() {
        return $this->belongsTo(AuthUser::class, 'updated_by');
    }

    public function deletedBy() {
        return $this->belongsTo(AuthUser::class, 'deleted_by');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    public function environmentInstanceUsers() {
        return $this->hasMany(Models\Environment\EnvironmentInstanceUser::class, 'auth_user_id');
    }

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //     DistantModel::class,
    //     IntermediateModel::class,
    //     'intermediate_key_id',
    //     'distant_key_id',
    //     'id'
    // );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(
    //     ForeignModel::class,
    //     'package_this_foreign_table',
    //     'this_table_id',
    //     'foreign_table_id'
    // )->withPivot([
    //     'id',
    //     'field_name',
    //     'created_at',
    //     'updated_at',
    //     'deleted_at'
    // ])->as('pivotModelName');
    //

    public function authGroups() {
        return $this->belongsToMany(
                AuthGroup::class,
                'auth_groups_users',
                'auth_user_id',
                'auth_group_id'
            )->withPivot([
                'id',
                'short_id',
                'expires_at',
                'created_at',
                'updated_at',
                'deleted_at'
            ])->as('authGroupUser');
    }

    public function environmentInstances() {
        return $this->belongsToMany(
                Models\Environment\EnvironmentInstance::class,
                'environment_instances_users',
                'auth_user_id',
                'environment_instance_id'
            )->withPivot([
                'id',
                'short_id',
                'expires_at',
                'created_at',
                'updated_at',
                'deleted_at'
            ])->as('environmentInstanceUser');
    }

    //
    // Custom Attribute Getters
    // ------------------------------------------------------------------------
    // When getting attributes in the database, these methods add additional
    // business logic to be invoked when a field is fetched.
    //

    public function getPermissionsAttribute()
    {
        $role_permissions = [];

        foreach($this->authGroups()->get() as $group) {
            foreach($group->authRoles()->get() as $role) {
                foreach($role->permissions as $permission) {
                    $role_permissions[] = $permission;
                }
            }
        }

        // Create a new collection using the array
        $role_permissions_collection = collect($role_permissions);

        // The unique method returns all of the unique items in the collection.
        // The returned collection keeps the original array keys, so we'll use
        // the values method to reset the keys to consecutively numbered indexes.
        $user_permissions = $role_permissions_collection->unique()->values()->all();

        return $user_permissions;

    }

    public function getGroupsAttribute()
    {
        $groups = [];

        foreach($this->authGroups()->get() as $group) {
            $groups[] = $group->slug;
        }

        // Create a new collection using the array
        // TODO Evaluate if this is necessary for groups since the relationship
        // should be unique already. We are doing it for consistency with the
        // other non-unique relationships for this iteration.
        $groups_collection = collect($groups);

        // The unique method returns all of the unique items in the collection.
        // The returned collection keeps the original array keys, so we'll use
        // the values method to reset the keys to consecutively numbered indexes.
        $user_groups = $groups_collection->unique()->values()->all();

        return $user_groups;

    }

    public function getRolesAttribute()
    {
        $roles = [];

        foreach($this->authGroups()->get() as $group) {
            foreach($group->authRoles()->get() as $role) {
                $roles[] = $role->slug;
            }
        }

        // Create a new collection using the array
        $roles_collection = collect($roles);

        // The unique method returns all of the unique items in the collection.
        // The returned collection keeps the original array keys, so we'll use
        // the values method to reset the keys to consecutively numbered indexes.
        $user_roles = $roles_collection->unique()->values()->all();

        return $user_roles;

    }

    public function getUserHandleAttribute()
    {

        // Get the email address handle (before the @)
        $email_parts = explode("@", $this->email);
        $user_handle = Str::slug($email_parts[0]);

        return $user_handle;

    }

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

    public function setPasswordAttribute($value)
    {
        if($this->password != $value) {
            $this->attributes['password'] = $value;
            $this->attributes['password_changed_at'] = now();
        }
    }

    public function setFlagAccountExpiredAttribute($value)
    {
        // If value is different than current value
        if($this->flag_account_expired != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_account_expired'] = 1;
                $this->attributes['expired_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_account_expired'] = 0;
                $this->attributes['expired_at'] = null;
            }

        }
    }

    public function setFlagAccountLockedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_account_locked != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_account_locked'] = 1;
                $this->attributes['locked_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_account_locked'] = 0;
                $this->attributes['locked_at'] = null;
            }

        }
    }

    public function setFlagAccountVerifiedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_account_verified != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_account_verified'] = 1;
                $this->attributes['verified_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_account_verified'] = 0;
                $this->attributes['verified_at'] = null;
            }

        }
    }

    public function setFlagTermsAcceptedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_terms_accepted != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_terms_accepted'] = 1;
                $this->attributes['terms_accepted_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_terms_accepted'] = 0;
                $this->attributes['terms_accepted_at'] = null;
            }

        }
    }

    public function setFlagPrivacyAcceptedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_privacy_accepted != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_privacy_accepted'] = 1;
                $this->attributes['privacy_accepted_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_privacy_accepted'] = 0;
                $this->attributes['privacy_accepted_at'] = null;
            }

        }
    }

    //
    // Scopes
    // ------------------------------------------------------------------------
    // We can use scope with Eloquent queries to filter results more easily
    // than unnecessary if() statements.
    //

}
