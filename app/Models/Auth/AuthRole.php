<?php

namespace App\Models\Auth;

use App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuthRole extends Models\BaseModel
{
    use SoftDeletes;

    public $table = 'auth_roles';

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    protected $fillable = [
        'auth_tenant_id',
        'name',
        'slug',
        'description',
        'flag_is_elevated',
        'permissions'
    ];

    //
    // Form Validation Rules
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/validation#available-validation-rules
    // See additional documentation in App\Models\BaseModel about how we use
    // form validation rules and implementation examples.
    //

    public $storeRules = [
        'auth_tenant_id' => 'required|uuid|exists:auth_tenants,id',
        'name' => 'required|string|max:55',
        'slug' => 'nullable|alpha_dash|max:55',
        'description' => 'nullable|string', // TODO Add max after determining UI requirements
        'flag_is_elevated' => 'nullable|integer|between:0,1',
        'permissions' => 'required', // TODO change this to JSON or array
    ];

    public $updateRules = [
        'auth_tenant_id' => 'nullable|uuid|exists:auth_tenants,id',
        'name' => 'nullable|string|max:55',
        'slug' => 'nullable|alpha_dash|max:55',
        'description' => 'nullable|string', // TODO Add max after determining UI requirements
        'flag_is_elevated' => 'nullable|integer|between:0,1',
        'permissions' => 'required', // TODO change this to JSON or array
    ];

    //
    // If the default error messages are not friendly enough for users, you
    // can set a custom error message for each field and condition not met.
    // https://laravel.com/docs/6.x/validation#custom-error-messages
    //

    public $validationMessages = [
        // 'field.condition' => 'This is a custom error message.'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/eloquent-mutators#attribute-casting
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response. This is not
    // required since the database column type is used by default, however we
    // want to be explicit for engineering clarity.
    //

    protected $casts = [
        // Common fields
        'id' => 'string',
        'created_at' => 'datetime',
        'created_by' => 'string',
        'updated_at' => 'datetime',
        'updated_by' => 'string',
        'deleted_at' => 'datetime',
        'deleted_by' => 'string',
        'state' => 'string',
        // Model specific fields
        'auth_tenant_id' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'permissions' => 'array',
    ];

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //

    protected $dates = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`packageWidget`). For a child
    // or many-to-many relationship, use a plural name (`packageWidgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    public function authTenant() {
        return $this->belongsTo(AuthTenant::class, 'auth_tenant_id');
    }

    public function createdBy() {
        return $this->belongsTo(AuthUser::class, 'created_by');
    }

    public function updatedBy() {
        return $this->belongsTo(AuthUser::class, 'updated_by');
    }

    public function deletedBy() {
        return $this->belongsTo(AuthUser::class, 'deleted_by');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //     DistantModel::class,
    //     IntermediateModel::class,
    //     'intermediate_key_id',
    //     'distant_key_id',
    //     'id'
    // );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(
    //     ForeignModel::class,
    //     'package_this_foreign_table',
    //     'this_table_id',
    //     'foreign_table_id'
    // )->withPivot([
    //     'id',
    //     'field_name',
    //     'created_at',
    //     'updated_at',
    //     'deleted_at'
    // ])->as('pivotModelName');
    //

    public function authGroups() {
        return $this->belongsToMany(
                AuthGroup::class,
                'auth_groups_roles',
                'auth_role_id',
                'auth_group_id'
            )->withPivot([
                'id',
                'short_id',
                'created_at',
                'updated_at',
                'deleted_at'
            ])->as('authGroupRole');
    }

    public function authServiceAccounts() {
        return $this->belongsToMany(
                AuthServiceAccount::class,
                'auth_service_accounts_roles',
                'auth_service_account_id',
                'auth_role_id'
            )->withPivot([
                'id',
                'short_id',
                'created_at',
                'updated_at',
                'deleted_at'
            ])->as('autServiceAccountRole');
    }

    //
    // Custom Attribute Getters
    // ------------------------------------------------------------------------
    // When getting attributes in the database, these methods add additional
    // business logic to be invoked when a field is fetched.
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

    //
    // Scopes
    // ------------------------------------------------------------------------
    // We can use scope with Eloquent queries to filter results more easily
    // than unnecessary if() statements.
    //

}
