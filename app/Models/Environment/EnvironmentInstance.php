<?php

namespace App\Models\Environment;

use App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnvironmentInstance extends Models\BaseModel
{
    use SoftDeletes;

    public $table = 'environment_instances';

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    protected $fillable = [
        'auth_tenant_id',
        'name',
        'slug',
        'description',
        'type',
        'user_base_url',
        'api_base_url',
        'api_username',
        'api_token',
        'api_meta_data',
        'instance_meta_data'
    ];

    //
    // Form Validation Rules
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/validation#available-validation-rules
    // See additional documentation in App\Models\BaseModel about how we use
    // form validation rules and implementation examples.
    //

    public $storeRules = [
        'auth_tenant_id' => 'required|uuid|exists:auth_tenant,id',
        'name' => 'required|string|max:55',
        'slug' => 'required|string|max:55',
        'description' => 'nullable|string', // TODO Add max after determining UI requirements
        'type' => 'required|string',
        'user_base_url' => 'nullable|url',
        'api_base_url' => 'nullable|url',
        'api_username' => 'nullable',
        'api_token' => 'nullable',
        'instance_meta_data' => 'nullable|array',
    ];

    public $updateRules = [
        'name' => 'nullable|string|max:55',
        'slug' => 'required|string|max:55',
        'description' => 'nullable|string', // TODO Add max after determining UI requirements
        'type' => 'required|string',
        'user_base_url' => 'nullable|url',
        'api_base_url' => 'nullable|url',
        'api_username' => 'nullable',
        'api_token' => 'nullable',
        'instance_meta_data' => 'nullable|array',
    ];

    //
    // If the default error messages are not friendly enough for users, you
    // can set a custom error message for each field and condition not met.
    // https://laravel.com/docs/6.x/validation#custom-error-messages
    //

    public $validationMessages = [
        // 'field.condition' => 'This is a custom error message.'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/eloquent-mutators#attribute-casting
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response. This is not
    // required since the database column type is used by default, however we
    // want to be explicit for engineering clarity.
    //

    protected $casts = [
        // Common fields
        'id' => 'string',
        'created_at' => 'datetime',
        'created_by' => 'string',
        'updated_at' => 'datetime',
        'updated_by' => 'string',
        'deleted_at' => 'datetime',
        'deleted_by' => 'string',
        // Model specific fields
        'auth_tenant_id' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'type' => 'string',
        'user_base_url' => 'string',
        'api_base_url' => 'string',
        'api_username' => 'string',
        'api_token' => 'string',
        'api_meta_data' => 'array',
        'instance_meta_data' => 'array',
    ];

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //

    protected $dates = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`packageWidget`). For a child
    // or many-to-many relationship, use a plural name (`packageWidgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    public function authTenant() {
        return $this->belongsTo(Models\Auth\AuthTenant::class, 'auth_tenant_id');
    }

    public function createdBy() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'created_by');
    }

    public function updatedBy() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'updated_by');
    }

    public function deletedBy() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'deleted_by');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    public function environmentInstanceUsers() {
        return $this->hasMany(Models\Environment\EnvironmentInstanceUser::class, 'environment_instance_id');
    }

    public function environmentInstanceRoles() {
        return $this->hasMany(Models\Environment\EnvironmentInstanceRole::class, 'environment_instance_id');
    }

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //     DistantModel::class,
    //     IntermediateModel::class,
    //     'intermediate_key_id',
    //     'distant_key_id',
    //     'id'
    // );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(
    //     ForeignModel::class,
    //     'package_this_foreign_table',
    //     'this_table_id',
    //     'foreign_table_id'
    // )->withPivot([
    //     'id',
    //     'field_name',
    //     'created_at',
    //     'updated_at',
    //     'deleted_at'
    // ])->as('pivotModelName');
    //

    public function authRoles() {
        return $this->belongsToMany(
                Models\Auth\AuthRole::class,
                'environment_instances_roles',
                'environment_instance_id',
                'auth_role_id'
            )->withPivot([
                'id',
                'short_id',
                'expires_at',
                'created_at',
                'updated_at',
                'deleted_at'
            ])->as('environmentInstanceRole');
    }

    public function authUsers() {
        return $this->belongsToMany(
                Models\Auth\AuthUser::class,
                'environment_instances_users',
                'environment_instance_id',
                'auth_user_id'
            )->withPivot([
                'id',
                'short_id',
                'expires_at',
                'created_at',
                'updated_at',
                'deleted_at'
            ])->as('environmentInstanceUser');
    }

    //
    // Custom Attribute Getters
    // ------------------------------------------------------------------------
    // When getting attributes in the database, these methods add additional
    // business logic to be invoked when a field is fetched.
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

    //
    // Scopes
    // ------------------------------------------------------------------------
    // We can use scope with Eloquent queries to filter results more easily
    // than unnecessary if() statements.
    //

}
