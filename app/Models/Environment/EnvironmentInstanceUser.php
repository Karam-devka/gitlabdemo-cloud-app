<?php

namespace App\Models\Environment;

use App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnvironmentInstanceUser extends Models\BaseModel
{
    use SoftDeletes;

    public $table = 'environment_instances_users';

    // Hide these fields from arrays
    protected $hidden = [
        'instance_password'
    ];

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    protected $fillable = [
        'evironment_instance_id',
        'auth_user_id',
        'instance_username',
        'instance_password',
        'instance_user_meta_data',
        'expires_at',
    ];

    //
    // Form Validation Rules
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/validation#available-validation-rules
    // See additional documentation in App\Models\BaseModel about how we use
    // form validation rules and implementation examples.
    //

    public $storeRules = [
        'auth_tenant_id' => 'required|uuid|exists:auth_tenant,id',
        'auth_user_id' => 'required|uuid|exists:auth_users,id',
        'environment_instance_id' => 'required|uuid|exists:environment_instances,id',
        'instance_username' => 'nullable|string',
        'instance_password' => 'nullable|string',
        'expires_at' => 'nullable', // TODO add timestamp
    ];

    public $updateRules = [
        'auth_user_id' => 'required|uuid|exists:auth_users,id',
        'environment_instance_id' => 'required|uuid|exists:environment_instances,id',
        'instance_username' => 'nullable|string',
        'instance_password' => 'nullable|string',
        'expires_at' => 'nullable', // TODO add timestamp
    ];

    //
    // If the default error messages are not friendly enough for users, you
    // can set a custom error message for each field and condition not met.
    // https://laravel.com/docs/6.x/validation#custom-error-messages
    //

    public $validationMessages = [
        // 'field.condition' => 'This is a custom error message.'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/eloquent-mutators#attribute-casting
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response. This is not
    // required since the database column type is used by default, however we
    // want to be explicit for engineering clarity.
    //

    protected $casts = [
        // Common fields
        'id' => 'string',
        'short_id' => 'string',
        'created_at' => 'datetime',
        'created_by' => 'string',
        'updated_at' => 'datetime',
        'updated_by' => 'string',
        'deleted_at' => 'datetime',
        'deleted_by' => 'string',
        // Model specific fields
        'auth_tenant_id' => 'string',
        'auth_user_id' => 'string',
        'environment_instance_id' => 'string',
        'instance_username' => 'string',
        'instance_password' => 'string',
        'api_meta_data' => 'array',
        'flag_provisioned' => 'integer',
        'provisioned_at' => 'datetime',
        'expires_at' => 'datetime'
    ];

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //

    protected $dates = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'provisioned_at' => 'datetime',
        'expires_at' => 'datetime',
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`packageWidget`). For a child
    // or many-to-many relationship, use a plural name (`packageWidgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    public function authTenant() {
        return $this->belongsTo(Models\Auth\AuthTenant::class, 'auth_tenant_id');
    }

    public function environmentInstance() {
        return $this->belongsTo(Models\Environment\EnvironmentInstance::class, 'environment_instance_id');
    }

    public function authUser() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'auth_user_id');
    }

    public function createdBy() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'created_by');
    }

    public function updatedBy() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'updated_by');
    }

    public function deletedBy() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'deleted_by');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //     DistantModel::class,
    //     IntermediateModel::class,
    //     'intermediate_key_id',
    //     'distant_key_id',
    //     'id'
    // );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(
    //     ForeignModel::class,
    //     'package_this_foreign_table',
    //     'this_table_id',
    //     'foreign_table_id'
    // )->withPivot([
    //     'id',
    //     'field_name',
    //     'created_at',
    //     'updated_at',
    //     'deleted_at'
    // ])->as('pivotModelName');
    //

    //
    // Custom Attribute Getters
    // ------------------------------------------------------------------------
    // When getting attributes in the database, these methods add additional
    // business logic to be invoked when a field is fetched.
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //


    public function setFlagProvisionedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_provisioned != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_provisioned'] = 1;
                $this->attributes['provisioned_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_provisioned'] = 0;
                $this->attributes['provisioned_at'] = null;
            }

        }
    }

    //
    // Scopes
    // ------------------------------------------------------------------------
    // We can use scope with Eloquent queries to filter results more easily
    // than unnecessary if() statements.
    //

}
