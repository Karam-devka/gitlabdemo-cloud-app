<?php

namespace App\Services;

use App\Models;
use Illuminate\Support\Str;

class BaseService
{

    protected $model;
    protected $modelHumanName;

    /**
     *   Get the default (only) tenant from the database
     *   TODO Implement multiple tenant support.
     *
     *   @return [type] [description]
     */
    public function defaultAuthTenant()
    {

        // Verify that multiple tenants do not exist
        $auth_tenant_count = Models\Auth\AuthTenant::count();
        if($auth_tenant_count >= 2) {
            $error_message = 'There are multiple authentication tenants. HackyStack does not support multiple tenants yet. To fix this error, delete the newly created tenant using the `auth-provider:delete` CLI command.';

            Log::channel('hackystack-auth')->critical(
                $error_message,
                [
                    'log_event_type' => 'multiple-auth-providers-not-allowed',
                    'log_class' => get_class(),
                    'ip_address' => config('demosys.log.ip_address.enabled') ? $request->ip() : null,
                    'user_agent' => config('demosys.log.user_agent.enabled') ? $request->userAgent() : null,
                ]
            );

            abort('501', $error_message);

        } else {

            // Get the only tenant in the database
            $auth_tenant = Models\Auth\AuthTenant::firstOrFail();

            return $auth_tenant;

        }

    }

    /**
     * Return the model for the resource.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return app($this->model);
    }

    /**
     *   Generate a slug using a name or other string
     *
     *   This method uses the Laravel String helper to create a hypenated,
     *   lowercase slug from a string with capitals, spaces, etc.
     *
     *   @param  string $name       Name Of Record
     *   @param  int    $length     Length limit of string (Default 55)
     *
     *   @return string             name-of-record
     */
    public function generateSlug($name, $length = 55)
    {
        return Str::limit(Str::slug($name, '-'), $length, '');
    }

}
