<?php

namespace App\Services\V1\Environment;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class EnvironmentInstanceService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Environment\EnvironmentInstance::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id          required|uuid|exists:auth_tenants,id
     *      name                    required|string|max:55
     *      slug                    required|string|max:55
     *      description             nullable|text
     *      type                    required|string
     *      user_base_url           nullable|url
     *      api_base_url            nullable|url
     *      api_username            nullable
     *      api_token               nullable
     *      instance_meta_data      nullable|array
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $tenant->id;
        }

        // Text fields
        $record->name = Arr::get($request_data, 'name');
        $record->slug = Arr::get($request_data, 'slug');
        $record->description = Arr::get($request_data, 'description');
        $record->type = Arr::get($request_data, 'type');
        $record->user_base_url = Arr::get($request_data, 'user_base_url');
        $record->api_base_url = Arr::get($request_data, 'api_base_url');

        // Encrypt API Username
        if(Arr::has($request_data, 'api_username')) {
            if(!empty($request_data['api_username'])) {
                $record->api_username = encrypt($request_data['api_username']);
            } else {
                $record->api_username = null;
            }
        }

        // Encrypt API Token
        if(Arr::has($request_data, 'api_token')) {
            if(!empty($request_data['api_token'])) {
                $record->api_token = encrypt($request_data['api_token']);
            } else {
                $record->api_token = null;
            }
        }

        $record->instance_meta_data = Arr::get($request_data, 'instance_meta_data');

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      name                    nullable|string|max:55
     *      slug                    nullable|string|max:55
     *      description             nullable|text
     *      type                    required|string
     *      user_base_url           nullable|url
     *      api_base_url            nullable|url
     *      api_username            nullable
     *      api_token               nullable
     *      instance_meta_data      nullable|array
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Text fields
        $record->name = Arr::get($request_data, 'name', $record->name);
        $record->slug = Arr::get($request_data, 'slug', $record->slug);
        $record->description = Arr::get($request_data, 'description', $record->description);
        $record->type = Arr::get($request_data, 'type', $record->type);
        $record->user_base_url = Arr::get($request_data, 'user_base_url', $record->user_base_url);
        $record->api_base_url = Arr::get($request_data, 'api_base_url', $record->api_base_url);
        $record->instance_meta_data = Arr::get($request_data, 'instance_meta_data', $record->instance_meta_data);

        // Encrypt API Username
        if(Arr::has($request_data, 'api_username') && !empty($request_data['api_username'])) {
            $record->api_username = encrypt($request_data['api_username']);
        }

        // Encrypt API Token
        if(Arr::has($request_data, 'api_token') && !empty($request_data['api_token'])) {
            $record->api_token = encrypt($request_data['api_token']);
        }

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        $record->environmentInstanceUsers()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        $record->environmentInstanceUsers()
            ->withTrashed()
            ->where('deleted_at', '>=', $deleted_at)
            ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        $record->environmentInstanceUsers()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

}
