<?php

namespace App\Services\V1\Environment;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Hackzilla\PasswordGenerator\Generator\RequirementPasswordGenerator;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class EnvironmentInstanceUserService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Environment\EnvironmentInstanceUser::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id          required|uuid|exists:auth_tenants,id
     *      auth_user_id            required|uuid|exists:auth_users,id
     *      environment_instance_id required|uuid|exists:environment_instances,id
     *      instance_username       nullable|string
     *      instance_password       nullable|string
     *      expires_at              nullable
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $auth_tenant->id;
        }

        // Get User relationship
        if(!empty($request_data['auth_user_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_user = Models\Auth\AuthUser::query()
                ->where('id', $request_data['auth_user_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_user_id = $auth_user->id;
        }

        // Get Environment Instance relationship
        if(!empty($request_data['environment_instance_id'])) {

            // Get relationship by ID to validate that it exists
            $environment_instance = Models\Environment\EnvironmentInstance::query()
                ->where('id', $request_data['environment_instance_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->environment_instance_id = $environment_instance->id;
        }

        // Get the email address handle (before the @)
        $email_parts = explode("@", $auth_user->email);
        $user_handle = Str::slug($email_parts[0]);

        // Generate password for instance user account
        // ----------------------------------------------------------------
        // Each password is randomly generated using complexity requirements
        // that can be customized in config/demosys.php.
        // https://github.com/hackzilla/password-generator
        $password_generator = new RequirementPasswordGenerator();
        $password_generator
          ->setLength(config('demosys.environment.instance.user.password_requirements.length'))
          ->setOptionValue(RequirementPasswordGenerator::OPTION_UPPER_CASE, config('demosys.environment.instance.user.password_requirements.upper_case.enabled'))
          ->setOptionValue(RequirementPasswordGenerator::OPTION_LOWER_CASE, config('demosys.environment.instance.user.password_requirements.lower_case.enabled'))
          ->setOptionValue(RequirementPasswordGenerator::OPTION_NUMBERS, config('demosys.environment.instance.user.password_requirements.numbers.enabled'))
          ->setOptionValue(RequirementPasswordGenerator::OPTION_SYMBOLS, config('demosys.environment.instance.user.password_requirements.symbols.enabled'))
          ->setMinimumCount(RequirementPasswordGenerator::OPTION_UPPER_CASE, config('demosys.environment.instance.user.password_requirements.upper_case.min_count'))
          ->setMinimumCount(RequirementPasswordGenerator::OPTION_LOWER_CASE, config('demosys.environment.instance.user.password_requirements.lower_case.min_count'))
          ->setMinimumCount(RequirementPasswordGenerator::OPTION_NUMBERS, config('demosys.environment.instance.user.password_requirements.numbers.min_count'))
          ->setMinimumCount(RequirementPasswordGenerator::OPTION_SYMBOLS, config('demosys.environment.instance.user.password_requirements.symbols.min_count'))
          ->setMaximumCount(RequirementPasswordGenerator::OPTION_UPPER_CASE, config('demosys.environment.instance.user.password_requirements.upper_case.max_count'))
          ->setMaximumCount(RequirementPasswordGenerator::OPTION_LOWER_CASE, config('demosys.environment.instance.user.password_requirements.lower_case.max_count'))
          ->setMaximumCount(RequirementPasswordGenerator::OPTION_NUMBERS, config('demosys.environment.instance.user.password_requirements.numbers.max_count'))
          ->setMaximumCount(RequirementPasswordGenerator::OPTION_SYMBOLS, config('demosys.environment.instance.user.password_requirements.symbols.max_count'));
        $generated_password = $password_generator->generatePassword();

        // Update value of record with username and password
        $record->instance_username = encrypt($user_handle);
        $record->instance_password = encrypt($generated_password);

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // If instance is a GitLab Omnibus instance, then provision a user
        // account and group based on instance meta data in the database
        if($environment_instance->type == 'gitlab') {

            // Initialize service for API connection
            $gitlabGroupService = new Services\V1\Vendor\Gitlab\GroupService($environment_instance->id);
            $gitlabUserService = new Services\V1\Vendor\Gitlab\UserService($environment_instance->id);

            // Create GitLab user account on instance
            // TODO Refactor permission levels for admin and create group
            $gitlab_user = $gitlabUserService->create([
                'name' => $auth_user->full_name,
                'username' => decrypt($record->instance_username),
                'email' => $auth_user->email,
                'password' => decrypt($record->instance_password),
                'flag_is_admin' => true,
                'flag_reset_password' => false,
                'flag_skip_confirmation' => true,
                'flag_can_create_group' => true
            ]);

            // Create GitLab group
            // TODO Refactor this to be dynamic based on database value
            $gitlab_group = $gitlabGroupService->create([
                'name' => $auth_user->full_name,
                'path' => decrypt($record->instance_username).'-group',
                'description' => 'Organization group for '.$auth_user->full_name,
                'visibility' => 'internal',
                'parent_id' => null
            ]);

            // Add Gitlab user to group with role-based permissions
            // TODO Refactor this to use EnvironmentInstanceRole relationship
            // to determine permission level based on user role.
            $gitlab_group_member = $gitlabGroupService->createMember([
                'gitlab_group_id' => $gitlab_group['id'],
                'gitlab_user_id' => $gitlab_user['id'],
                'access_level' => 50
            ]);

            // Update Environment Instance User with GitLab API meta data
            $record->api_meta_data = [
                'gitlab_groups' => [
                    $gitlab_group['id'] => [
                        'id' => $gitlab_group['id'],
                        'full_path' => $gitlab_group['full_path'],
                        'access_level' => $gitlab_group_member['access_level']
                    ],
                ],
                'gitlab_user' => [
                    $gitlab_user['id'] => [
                        'id' => $gitlab_user['id'],
                        'username' => $gitlab_user['username'],
                        'is_admin' => $gitlab_user['is_admin'],
                        'can_create_group' => $gitlab_user['can_create_group']
                    ],
                ],
            ];
            $record->save();

        }

        $record = $record->fresh();

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      TODO
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        // TODO
    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

}
