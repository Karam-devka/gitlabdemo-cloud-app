<?php

namespace App\Services\V1\Auth;

use App\Services\BaseService;
use App\Models;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthProviderService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Auth\AuthProvider::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id          required|uuid|exists:auth_tenants,id
     *      slug                    required|enum:local,{sso_provider}
     *                                  See the `available_sso_providers` array
     *                                  in ~/config/hackystack.php for a list of
     *                                  valid slug values.
     *      base_url                required|url|max:255
     *      client_id               required|string|max:255
     *      client_secret           required|string|max:255
     *      flag_enabled            nullable|integer|between:0,1
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $tenant->id;
        }

        // Get list of available providers by creating a new array with the
        // local authentication provider and the list of SSO providers.
        $available_providers = array_merge(['local'], config('demosys.auth.available_sso_providers'));

        // If the value of slug matches one of the providers, then update the
        // database with the value.
        // TODO fix array lookup for validate providers
        //if(Arr::has($available_providers, $request_data['slug'])) {
        //    $record->slug = $request_data['slug'];
        //} else {
        //    abort(501, 'The value provided for the provider `slug` ('.$request_data['slug'].') is not an available provider. Allowed values are local,'.implode(",", config('demosys.auth.available_sso_providers')).'.');
        //}
        $record->slug = $request_data['slug'];

        // TODO Add validation connection test before adding to database.

        // Text fields
        $record->base_url = Arr::get($request_data, 'base_url');
        $record->client_id = !empty($request_data['client_id']) ? encrypt($request_data['client_id']) : null;
        $record->client_secret = !empty($request_data['client_secret']) ? encrypt($request_data['client_secret']) : null;
        $record->flag_enabled = Arr::get($request_data, 'flag_enabled', 0);

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      slug                    nullable|enum:local,{sso_provider}
     *                                  See the `available_sso_providers` array
     *                                  in ~/config/hackystack.php for a list of
     *                                  valid slug values.
     *      base_url                nullable|url|max:255
     *      client_id               nullable|string|max:255
     *      client_secret           nullable|string|max:255
     *      flag_enabled            nullable|integer|between:0,1
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // If request data includes the slug
        if(!empty($request_data['slug'])) {

            // If request data value is different than record existing value
            if($record->slug != $request_data['slug']) {

                // Get list of available providers by creating a new array with the
                // local authentication provider and the list of SSO providers.
                $available_providers = array_merge(['local'], config('demosys.auth.available_sso_providers'));

                // If the value of slug matches one of the providers, then update the
                // database with the value.
                if(Arr::has($available_providers, $request_data['slug'])) {
                    $record->slug = $request_data['slug'];
                } else {
                    abort(501, 'The value provided for the provider `slug` is not an available provider. Allowed values are local,'.implode(",", config('demosys.auth.available_sso_providers')).'.');
                }

            }
        }

        // TODO Add validation connection test before adding to database. Can set flag_enabled to false if connection test fails.

        // Text fields
        $record->base_url = Arr::get($request_data, 'base_url', $record->base_url);
        $record->client_id = !empty($request_data['client_id']) ? encrypt($request_data['client_id']) : $record->client_id;
        $record->client_secret = !empty($request_data['client_secret']) ? encrypt($request_data['client_secret']) : $record->client_secret;
        $record->flag_enabled = Arr::get($request_data, 'flag_enabled', $record->flag_enabled);

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

}
