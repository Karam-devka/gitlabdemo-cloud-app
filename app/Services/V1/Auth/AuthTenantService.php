<?php

namespace App\Services\V1\Auth;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class AuthTenantService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Auth\AuthTenant::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      name                        required|string|max:55
     *      slug                        nullable|string|max:55
     *      description                 nullable|text
     *      git_provider_type           nullable|in:gitlab
     *      git_provider_base_url       nullable|string
     *      git_provider_base_group_id  nullable|string
     *      git_provider_api_token      nullable|string
     *      git_meta_data      nullable|array
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Text fields
        $record->name = Arr::get($request_data, 'name');
        $record->slug = Arr::get($request_data, 'slug');
        $record->description = Arr::get($request_data, 'description');
        $record->git_provider_type = Arr::get($request_data, 'git_provider_type');
        $record->git_provider_base_url = Arr::get($request_data, 'git_provider_base_url');
        $record->git_provider_base_group_id = Arr::get($request_data, 'git_provider_base_group_id');
        $record->git_provider_api_token = !empty($request_data['git_provider_api_token']) ? encrypt($request_data['git_provider_api_token']) : null;
        $record->git_meta_data = Arr::get($request_data, 'git_meta_data');

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Auth Providers
        // --------------------------------------------------------------------
        // When a new tenant is created, we create the authentication providers
        // that are supported, however only the `local` provider is enabled.
        //

        // Instantiate the provider service
        $authProviderService = new Services\V1\Auth\AuthProviderService();

        $available_providers = array_merge(['local'], config('demosys.auth.available_sso_providers'));

        foreach($available_providers as $provider_slug) {

            // Use the service method to create a new provider
            $authProviderService->store([
                'auth_tenant_id' => $record->id,
                'slug' => $provider_slug,
                'flag_enabled' => $provider_slug=='local' ? '1' : '0',
            ]);

        }

        //
        // Cloud Providers
        // --------------------------------------------------------------------
        //

        if(Schema::hasTable('cloud_providers')) {

            // Instantiate the provider service
            $cloudProviderService = new Services\V1\Cloud\CloudProviderService();

            $available_providers = config('demosys.cloud.available_providers');

            foreach($available_providers as $provider_type) {

                // Use the service method to create a new provider
                $cloudProviderService->store([
                    'auth_tenant_id' => $record->id,
                    'type' => $provider_type,
                    'name' => Str::upper($provider_type),
                    'slug' => $provider_type,
                    'flag_enabled' => 0,
                ]);

            }

        }

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Update Git Provider Meta Data
        $this->updateGitMetaData($record->id);

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      name                        nullable|string|max:55
     *      slug                        nullable|string|max:55
     *      description                 nullable|text
     *      git_provider_type           nullable|in:gitlab
     *      git_provider_base_url       nullable|string
     *      git_provider_base_group_id  nullable|string
     *      git_provider_api_token      nullable|string
     *      git_meta_data      nullable|array
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Text fields
        $record->name = Arr::get($request_data, 'name', $record->name);
        $record->slug = Arr::get($request_data, 'slug', $record->slug);
        $record->description = Arr::get($request_data, 'description', $record->description);
        $record->git_provider_type = Arr::get($request_data, 'git_provider_type', $record->git_provider_type);
        $record->git_provider_base_url = Arr::get($request_data, 'git_provider_base_url', $record->git_provider_base_url);
        $record->git_provider_base_group_id = Arr::get($request_data, 'git_provider_base_group_id', $record->git_provider_base_group_id);
        $record->git_provider_api_token = !empty($request_data['git_provider_api_token']) ? encrypt($request_data['git_provider_api_token']) : $record->git_provider_api_token;

        $record->save();

        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Update Git Provider Meta Data
        $this->updateGitMetaData($id);

        $record = $record->fresh();

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

    public function updateGitMetaData($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // If AuthTenant git_provider_type is GitLab
        if($record->git_provider_type == 'gitlab') {

            // Initialize GitLab group service
            $gitlabGroupService = new Services\V1\Vendor\Gitlab\GroupService($record->id);

            // Perform API call to get group
            $tenant_group = $gitlabGroupService->get($record->git_provider_base_group_id);

            // Update the database with the API result
            $record->git_meta_data = $tenant_group;
            $record->save();

            return true;

        }

        // If AuthTenant git_provider_type is null or invalid value
        else {
            return false;
        }

    }

}
