<?php

namespace App\Services\V1\Auth;

use App\Services\BaseService;
use App\Models;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class AuthUserService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Auth\AuthUser::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id                      required|uuid|exists:auth_tenants,id
     *      auth_provider_id                    required|uuid|exists:auth_providers,id
     *      full_name                           required|max:55 // TODO fix regex to allow spaces - regex:/^[a-zA-Z]+(([\. -][a-zA-Z ])?[a-zA-Z]*)*$/
     *      job_title                           nullable|string|max:255
     *      organization_name                   nullable|string|max:55
     *      email                               required|email|unique:auth_users,email
     *      email_recovery                      nullable|email
     *      provider_meta_data                  nullable|array
     *      provider_token                      nullable|string
     *      phone_number                        nullable|numeric|digits_between:4,14
     *      phone_country_code                  nullable|numeric|digits_between:1,3|required_with:phone_number
     *      expires_at                          nullable|date
     *      password                            nullable|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
     *      timezone                            nullable|timezone
     *      flag_terms_accepted                 nullable|integer|between:0,1
     *      flag_privacy_accepted               nullable|integer|between:0,1
     *      notify_send_verification_email      nullable|integer|between:0,1
     *      notify_send_temp_password_email     nullable|integer|between:0,1|required_without:password
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $tenant->id;
        }

        // Get Provider relationship
        if(!empty($request_data['auth_provider_id'])) {

            // Get relationship by ID to validate that it exists
            $provider = Models\Auth\AuthProvider::query()
                ->where('id', $request_data['auth_provider_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_provider_id = $provider->id;
        }

        // Text fields
        $record->full_name = Arr::get($request_data, 'full_name');
        $record->job_title = Arr::get($request_data, 'job_title');
        $record->organization_name = Arr::get($request_data, 'organization_name');
        $record->email = Arr::get($request_data, 'email');
        $record->email_recovery = Arr::get($request_data, 'email_recovery');
        $record->provider_token = Arr::has($request_data, 'provider_token') ? encrypt($request_data['provider_token']) : null;
        $record->phone_number = Arr::get($request_data, 'phone_number');
        $record->phone_country_code = Arr::get($request_data, 'phone_country_code');
        $record->expires_at = Arr::get($request_data, 'expires_at');
        $record->timezone = Arr::get($request_data, 'timezone');
        $record->flag_terms_accepted = Arr::get($request_data, 'flag_terms_accepted');
        $record->flag_privacy_accepted = Arr::get($request_data, 'flag_privacy_accepted');

        // If provider meta data is in request, encode as JSON
        if(Arr::has($request_data, 'provider_meta_data')) {
            //$record->provider_meta_data = json_encode($request_data['provider_meta_data']);
            $record->provider_meta_data = $request_data['provider_meta_data'];
        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // If a password was provided in the request, then update the account
        // with the new password. The false parameter ensures that we will not
        // send an email with a password confirmation change that's used for
        // user-initiated password changes after the account has been created.
        if(Arr::has($request_data, 'password')) {
            $this->setUserAccountPassword($record->id, $request_data['password'], false);
            $notify_send_temp_password_email = 0;
        }

        // Set a temporary password. If notify_send_temp_password_email is set to
        // true, the user will be sent an email with the temp password. We will
        // also set a variable for the return response based on the input.
        else {
            if($request_data['notify_send_temp_password_email'] == 1) {
                $this->setUserAccountTemporaryPassword($record->id, true);
                $notify_send_temp_password_email = 1;
            } elseif($request_data['notify_send_temp_password_email'] == 0) {
                $this->setUserAccountTemporaryPassword($record->id, false);
                $notify_send_temp_password_email = 0;
            }

        }

        // Unless the notify_send_verification_email boolean is set to false,
        // we will invoke the repository method that dispatches a job to
        // send the user verification email. The flag variable is used
        // for the return data to programatically indicate whether we
        // sent the user an email.
        if(Arr::has($request_data, 'notify_send_verification_email') && $request_data['notify_send_verification_email'] == 0) {
            $notify_send_verification_email = 0;
        } else {
            $this->sendUserVerificationEmail($record->id);
            $notify_send_verification_email = 1;
        }


        // TODO notify_send_verification_email
        // TODO notify_send_temp_password_email

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *
     *      auth_tenant_id                      nullable|uuid|exists:auth_tenants,id
     *      auth_provider_id                    nullable|uuid|exists:auth_providers,id
     *      full_name                           nullable|max:55 // TODO fix regex to allow spaces - regex:/^[a-zA-Z]+(([\. -][a-zA-Z ])?[a-zA-Z]*)*$/
     *      job_title                           nullable|string|max:255
     *      organization_name                   nullable|string|max:55
     *      email                               nullable|email
     *      email_recovery                      nullable|email
     *      provider_meta_data                  nullable|array
     *      provider_token                      nullable|string
     *      phone_number                        nullable|numeric|digits_between:4,14
     *      phone_country_code                  nullable|numeric|digits_between:1,3|required_with:phone_number
     *      expires_at                          nullable|date
     *      password                            nullable|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
     *      timezone                            nullable|timezone
     *      flag_2fa_enabled                    nullable|integer|between:0,1
     *      flag_must_change_password           nullable|integer|between:0,1
     *      flag_account_expired                nullable|integer|between:0,1
     *      flag_account_locked                 nullable|integer|between:0,1
     *      flag_account_verified               nullable|integer|between:0,1
     *      flag_terms_accepted                 nullable|integer|between:0,1
     *      flag_privacy_accepted               nullable|integer|between:0,1
     *      action_set_temporary_password       nullable|integer|between:0,1
     *      notify_send_temp_password_email     nullable|integer|between:0,1
     *      notify_send_password_change_email   nullable|integer|between:0,1
     *      notify_send_verification_email      nullable|integer|between:0,1'
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // If email address is different
        if(Arr::has($request_data, 'email') && Arr::get($request_data, 'email') != $record->email) {

            // Validate that email address (unique key) does not exist
            $email_lookup = $this->model()
                ->where('email', $request_data['email'])
                ->where('id', '!=', $record->id)
                ->first();

            if($email_lookup) {
                abort(409, 'The email address is already in use by another user.');
            }

            // Update email address
            $record->email = $request_data['email'];
            $record->save();

            // Update user verification status
            $record->flag_account_verified = 0;

            if(Arr::has($request_data, 'notify_send_verification_email') && Arr::get($request_data, 'notify_send_verification_email') == 0) {
                // Do not send verification email
            } else {
                // If flag is true (1) or not set, send verification email
                $this->sendUserVerificationEmail($record->id);
            }

        }

        //
        // Update Record in Database
        // --------------------------------------------------------------------
        // Use the request data to update the record in the database.
        //

        // Set $attribute value if set in request, otherwise set as null
        $record->full_name = Arr::has($request_data, 'full_name') ? $request_data['full_name'] : $record->full_name;
        $record->job_title = Arr::has($request_data, 'job_title') ? $request_data['job_title'] : $record->job_title;
        $record->organization_name = Arr::has($request_data, 'organization_name') ? $request_data['organization_name'] : $record->organization_name;
        $record->email_recovery = Arr::has($request_data, 'email_recovery') ? $request_data['email_recovery'] : $record->email_recovery;
        $record->provider_token = Arr::has($request_data, 'provider_token') ? encrypt($request_data['provider_token']) : $record->provider_token;
        $record->phone_number = Arr::has($request_data, 'phone_number') ? $request_data['phone_number'] : $record->phone_number;
        $record->phone_country_code = Arr::has($request_data, 'phone_country_code') ? $request_data['phone_country_code'] : $record->phone_country_code;
        $record->timezone = Arr::has($request_data, 'timezone') ? $request_data['timezone'] : $record->timezone;

        // If provider meta data is in request, encode as JSON
        // TODO Use collect() methods to perform key matching and only update changed values to avoid data loss
        if(Arr::has($request_data, 'provider_meta_data') && count($request_data['provider_meta_data']) > 0) {
            //$record->provider_meta_data = json_encode($request_data['provider_meta_data']);
            $record->provider_meta_data = $request_data['provider_meta_data'];
        }

        $record->save();

        //
        // Advanced Business Logic and Workflows
        // ----------------------------------------------------------------
        //

        // Get refreshed record
        $record = $record->fresh();

        // TODO Implement flag_2fa_enabled and authy_id

        // Set a variable for sending email notifications, with defaults if not set
        $notify_send_temp_password_email = Arr::has($request_data, 'notify_send_temp_password_email') ? $request_data['notify_send_temp_password_email'] : 0;
        $notify_send_password_change_email = Arr::has($request_data, 'notify_send_password_change_email') ? $request_data['notify_send_password_change_email'] : 1;
        $notify_send_verification_email = Arr::has($request_data, 'notify_send_verification_email') ? $request_data['notify_send_verification_email'] : 0;

        // If a password was provided in the request, then check if the password
        // matches and update the account with the new password. The notify
        // parameter determines whether we send an email with a password
        // confirmation change
        if(Arr::has($request_data, 'password')) {
            if($this->checkIfUserPasswordMatches($record->id, $request_data['password']) == false) {
                $this->setUserAccountPassword($record->id, $request_data['password'], $notify_send_password_change_email);
                if($notify_send_password_change_email == 1) {
                    $this->setUserAccountTemporaryPassword($record->id, true);
                    $notify_send_password_change_email = 1;
                } else {
                    $this->setUserAccountTemporaryPassword($record->id, false);
                    $notify_send_password_change_email = 0;
                }
            } else {
                $notify_send_password_change_email = 0;
            }
        } else {
            $notify_send_password_change_email = 0;
        }

        // If action to set a temporary password is set, then invoke the service
        // method to set a temporary password. If the notify_send_temp_password_email
        // flag is set to 1 (true), then an email will be sent with the new password
        if(Arr::has($request_data, 'action_set_temporary_password') && $request_data['action_set_temporary_password'] == 1) {
            if($notify_send_temp_password_email == 1) {
                $this->setUserAccountTemporaryPassword($record->id, true);
                $notify_send_temp_password_email = 1;
            } else {
                $this->setUserAccountTemporaryPassword($record->id, false);
                $notify_send_temp_password_email = 0;
            }
        } else {
            $notify_send_temp_password_email = 0;
        }

        //
        // Update Database with Flags
        //

        if(Arr::has($request_data, 'flag_must_change_password') && $request_data['flag_must_change_password'] != $record->flag_must_change_password) {
            if($request_data['flag_must_change_password'] == 1) {
                $record->flag_must_change_password = 1;
            } elseif($request_data['flag_must_change_password'] == 0) {
                $record->flag_must_change_password = 0;
            }
        }

        if(Arr::has($request_data, 'flag_account_locked') && $request_data['flag_account_locked'] != $record->flag_account_locked) {
            if($request_data['flag_account_locked'] == 1) {
                $record->flag_account_locked = 1;
            } elseif($request_data['flag_account_locked'] == 0) {
                $record->flag_account_locked = 0;
            }
        }

        if(Arr::has($request_data, 'flag_account_verified') && $request_data['flag_account_verified'] != $record->flag_account_verified) {
            if($request_data['flag_account_verified'] == 1) {
                $record->flag_account_verified = 1;
            } elseif($request_data['flag_account_verified'] == 0) {
                $record->flag_account_verified = 0;
            }
        }

        if(Arr::has($request_data, 'flag_privacy_accepted') && $request_data['flag_privacy_accepted'] != $record->flag_privacy_accepted) {
            if($request_data['flag_privacy_accepted'] == 1) {
                $record->flag_privacy_accepted = 1;
            } elseif($request_data['flag_privacy_accepted'] == 0) {
                $record->flag_privacy_accepted = 0;
            }
        }

        if(Arr::has($request_data, 'flag_terms_accepted') && $request_data['flag_terms_accepted'] != $record->flag_terms_accepted) {
            if($request_data['flag_terms_accepted'] == 1) {
                $record->flag_terms_accepted = 1;
            } elseif($request_data['flag_terms_accepted'] == 0) {
                $record->flag_terms_accepted = 0;
            }
        }

        $record->save();

        //
        // Update Database with Dates After Request Processed
        //

        if(Arr::has($request_data, 'expires_at') && Carbon::parse($request_data['expires_at']) != $record->expires_at) {
            $record->expires_at = Carbon::parse($request_data['expires_at']);
            $record->save();
        }

        $this->checkIfUserAccountHasExpired($record->id);

        //
        // Refresh Variable with Latest Model Data
        //

        $record = $record->fresh();

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

    /**
     *   Calculate User State based on conditional checks of account locks,
     *   expiration and email verification status. A "verified" state is
     *   considered okay and in good standing.
     *
     *   @param  uuid $id
     *   @return string locked|expired|unverified|verified|unknown
     */
    public function calculateUserState($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        // Use repository methods to conditionally check for statees
        if($this->checkIfUserAccountIsLocked($user->id) == true) {
            $state = 'locked';
        } elseif($this->checkIfUserAccountHasExpired($user->id) == true) {
            $state = 'expired';
        } elseif($this->checkIfUserAccountIsVerified($user->id) == false) {
            $state = 'unverified';
        } elseif($this->checkIfUserAccountIsVerified($user->id) == true) {
            $state = 'verified';
        } else {
            $state = 'unknown';
        }

        // If state is different than current state, update the user record
        if($user->state != $state) {
            $user->state = $state;
            $user->save();
        }

        return $state;

    }

    /**
     *   Check if email address exists in the database
     *   @example $this->authUserService()->checkIfUserEmailExists($email);
     *
     *   @param  string $email
     *   @return boolean
     */
    public function checkIfUserEmailExists($email)
    {
        $email_lookup = $this->model()
            ->where('email', $email)
            ->first();

        if($email_lookup) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *   Check if database password matches request password
     *   @example $this->authUserService()->checkIfUserPasswordMatches($id, $password);
     *
     *   @param  uuid $id
     *   @param  string $password           Unhashed Password from Form Request
     *   @return boolean
     */
    public function checkIfUserPasswordMatches($id, $password)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        return Hash::check($password, $user->password);
    }

    /**
     *   Check if user account is locked
     *   @example $this->authUserService()->checkIfUserAccountIsLocked($id);
     *
     *   @param  uuid $id
     *   @return boolean
     */
    public function checkIfUserAccountIsLocked($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        if($user->flag_account_locked == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *   Check if the user account has expired. If the expiration date is set
     *   and the expiration date has passed, the expireUserAccount method will
     *   be invoked to update the user account record.
     *   @example $this->authUserService()->checkIfUserAccountHasExpired($id);
     *
     *   @param  uuid $id
     *   @return boolean
     */
    public function checkIfUserAccountHasExpired($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        if($user->flag_account_expired == true) {
            return true;
        } elseif($user->expires_at != null && $user->expires_at < now()) {
            $user->flag_account_expired = true;
            $user->expired_at = $user->expires_at;
            $user->save();
            return true;
        } elseif($user->expires_at != null && $user->expires_at > now()) {
            $user->flag_account_expired = false;
            $user->expired_at = null;
            $user->save();
            return false;
        } else {
            return false;
        }
    }

    /**
     *   Check if user has accepted terms of use
     *   @example $this->authUserService()->checkIfUserHasAcceptedTerms($id);
     *
     *   @param  uuid $id
     *   @return boolean
     */
    public function checkIfUserHasAcceptedTerms($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        if($user->flag_terms_accepted == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *   Check if user account has been verified
     *   @example $this->authUserService()->checkIfUserAccountIsVerified($id);
     *
     *   @param uuid $id
     *   @return boolean
     */
    public function checkIfUserAccountIsVerified($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        if($user->flag_account_verified == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *   Check if user must change password. We use the application config file
     *   to determine the number of days before a password change is required.
     *   We then calculate the number of days since the current password was
     *   last changed and update the flag_must_change_password attribute if the
     *   current password age is too old.
     *   @example $this->authUserService()->checkIfUserMustChangePassword($id);
     *
     *   @param  uuid $id
     *   @return boolean
     */
    public function checkIfUserMustChangePassword($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        $current_password_age = $user->password_changed_at->diffInDays(now());

        if($user->flag_must_change_password == true) {
            return true;
        } elseif($current_password_age > config('demosys.auth.local.password.count_days_until_must_change')) {
            $user->flag_must_change_password = true;
            $user->save();
            return true;
        } else {
            return false;
        }
    }

    /**
     *   Check if user failed login attempts limit has been reached. If limit
     *   has been reached, and config parameters for force password reset or
     *   force account lock are true, those respective methods will be invoked.
     *   If the password is reset, an email will be sent to the user if the
     *   $notify parameter is set to true (will default to true if not set).
     *   @example $this->authUserService()->checkIfFailedLoginAttemptsLimitReached($id, true|false);
     *
     *   @param  uuid $id
     *   @param  boolean $notify             If true, email notification will be
     *                                          sent to user if password is reset
     *   @return boolean
     */
    public function checkIfFailedLoginAttemptsLimitReached($id, $notify = true)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        // If the user has too many failed login attempts
        if($user->count_current_failed_logins >= config('demosys.auth.local.failed_login.attempt_limit_max')) {

            // If the application configuration requires a password reset after
            // to many login attempts, we will set a temporary password
            if(config('demosys.auth.local.failed_login.force_password_reset') == true && $user->flag_must_change_password == false) {
                // Set a temporary password and do not send email notification,
                // since we will use the special email notification
                $temporary_password = $this->setUserAccountTemporaryPassword($user->id, false);
                if($notify == true) {
                    $this->sendUserAccountPasswordChangeFailedLoginEmail($user->id, $temporary_password);
                }
            }

            // If the application configuration requires the account to be locked
            // (requiring the user to contact support).
            if(config('demosys.auth.local.failed_login.force_account_lock') == true && $user->flag_account_locked == false) {
                $user->flag_account_locked = true;
                $user->save();
            }

            return true;
        } else {
            return false;
        }

    }

    /**
     *   Set a temporary password for a user account. The user will be forced to
     *   change their password the next time that they log in. If the notify
     *   parameter is not set to false, this method will dispatch a job that
     *   sends an automated email to the user with their temporary password.
     *   @example $this->authUserService()->setUserAccountTemporaryPassword($id, true|false);
     *
     *   @param  uuid $id
     *   @param  boolean $notify             If true, email notification will be
     *                                          sent to user with temp password
     *   @return string         Randomly generated password
     */
    public function setUserAccountTemporaryPassword($id, $notify = true)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        $random_password = Str::random(12);

        $user->password = Hash::make($random_password);
        $user->flag_must_change_password = true;
        $user->save();

        if($notify == true) {
            $this->sendUserTemporaryPasswordEmail($id, $random_password);
        }

        return $random_password;
    }

    /**
     *   Set a new password for a user account using the provided password that
     *   is provided in a form request for changing a user's password.
     *
     *   If the notify parameter is not set to false, this method will dispatch
     *   a job that sends an automated email to the user with confirmation that
     *   their password has been changed for security awareness.
     *   @example $this->authUserService()->setUserAccountPassword($id, $new_password, true);
     *
     *   @param  uuid $id
     *   @param  string $new_password    (required) New password for user
     *   @param  boolean $notify         (optional) Flag to send automated email
     *                                    that will default to true if not set
     *   @return null
     */
    public function setUserAccountPassword($id, $new_password, $notify = true)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        $user->password = Hash::make($new_password);
        $user->flag_must_change_password = false;
        $user->save();

        if($notify == true) {
            $this->sendUserAccountPasswordChangeConfirmationEmail($user);
        }

        return null;
    }

    /**
     *   For failed authentication attempt, increment failed login attempt count
     *
     *   @example $this->authUserService()->incrementFailedLoginAttemptCount($id);
     *
     *   @param uuid $id
     *   @return integer count of failed attempts
     */
    public function incrementFailedLoginAttemptCount($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        $user->count_current_failed_logins = $user->count_current_failed_logins++;
        $user->last_failed_login_at = now();
        $user->save();

        return $user->count_current_failed_logins;
    }

    /**
     *   For successful authentication attempt, set failed login attempt to 0
     *   @example $this->authUserService()->resetFailedLoginAttemptCount($id);
     *
     *   @param  uuid $id
     *   @return null
     */
    public function resetFailedLoginAttemptCount($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        $user->count_current_failed_logins = 0;
        $user->save();

        return null;
    }

    /**
     *   Dispatch a background job to send the user a verification email
     *
     *   @example $this->authUserService()->sendUserVerificationEmail($id);
     *   @param uuid $id
     *
     *   @return null
     */
    public function sendUserVerificationEmail($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        // TODO Create job class
        //dispatch(new Jobs\Auth\SendUserVerificationEmail($user));

        return null;
    }

    /**
     *   Dispatch a background job to send the user an email with a temp password
     *   @example $this->authUserService()->sendUserTemporaryPasswordEmail($id);
     *
     *   @param  uuid $id
     *   @param  string $temporary_password
     *   @return null
     */
    public function sendUserTemporaryPasswordEmail($id, $temporary_password)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        // TODO Create job class
        //dispatch(new Jobs\Auth\SendUserTemporaryPasswordEmail($user, $temporary_password));

        return null;
    }

    /**
     *   Dispatch a background job to send the user a password change required
     *   email due to failed login attempts limit being reached
     *   @example $this->authUserService()->sendUserAccountPasswordChangeFailedLoginEmail($id, $temporary_password);
     *
     *   @param  uuid $id
     *   @param  string $temporary_password
     *   @return null
     */
    public function sendUserAccountPasswordChangeFailedLoginEmail($id, $temporary_password)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        // TODO Create job class
        //dispatch(new Jobs\Auth\SendUserPasswordChangeFailedLoginEmail($user, $temporary_password));

        return null;
    }

    /**
     *   Dispatch a background job to send the user a password change confirmation
     *   email message.
     *   @example $this->authUserService()->sendUserAccountPasswordChangeConfirmationEmail($id);
     *
     *   @param uuid $id
     *   @return null
     */
    public function sendUserAccountPasswordChangeConfirmationEmail($id)
    {
        $user = $this->model()
            ->where('id', $id)
            ->firstOrFail();

        // TODO Create job class
        //dispatch(new Jobs\Auth\SendUserPasswordChangeConfirmationEmail($user));

        return null;
    }

}
