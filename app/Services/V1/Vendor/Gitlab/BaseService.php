<?php

namespace App\Services\V1\Vendor\Gitlab;

use App\Models;
use App\Services;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class BaseService
{

    protected $environment_instance;
    protected $gitlab_api_client;

    /**
     *   Set the Auth Tenant used for GitLab API calls
     *
     *   @param uuid $auth_tenant_id
     */
    public function setEnvironmentInstance($environment_instance_id)
    {
        // Get auth tenant from database
        $this->environment_instance = Models\Environment\EnvironmentInstance::query()
            ->where('id', $environment_instance_id)
            ->firstOrFail();
    }

    /**
     *   Initialize GitLab API client
     */
    public function setGitlabApiClient()
    {
        // Initialize GitLab API connection with HTTP (Personal Access Token)
        // https://github.com/GitLabPHP/Client
        $this->gitlab_api_client = new \Gitlab\Client();
        $this->gitlab_api_client->authenticate(decrypt($this->environment_instance->api_token), \Gitlab\Client::AUTH_HTTP_TOKEN);
        $this->gitlab_api_client->setUrl($this->environment_instance->api_base_url);
    }

    /**
     *   Handle GitLab API Exception
     *
     *   @param  [type] $e              An instance of the exception
     *   @param  string $log_class      get_class()
     *   @param  string $reference      Reference slug or identifier
     *
     *   @return string                 Error message
     */
    public function handleException($e, $log_class, $reference)
    {
        //$error_message = json_decode($e);

        // TODO
        dd($e);

        Log::channel('hackystack-git')->error('GitLab API returned a response error.', [
            'log_event_type' => 'gitlab-api-response-error',
            'log_class' => get_class(),
            'auth_tenant_id' => $this->auth_tenant->id,
            'error_code' => $error_message->code,
            'error_message' => $error_message->message,
            'error_reference' => $reference,
        ]);

        return $error_message;

    }

}
