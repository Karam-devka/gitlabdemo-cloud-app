<?php

namespace App\Services\V1\Vendor\Gitlab;

use App\Services\V1\Vendor\Gitlab\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class UserService extends BaseService
{

    public function __construct($environment_instance_id)
    {
        // Call BaseService methods to establish API connection
        $this->setEnvironmentInstance($environment_instance_id);
        $this->setGitlabApiClient();

    }

    /**
     *   Create a GitLab User
     *   https://github.com/GitLabPHP/Client/blob/11.4/src/Api/Users.php
     *   https://docs.gitlab.com/ee/api/users.html#user-creation
     *
     *   @param  array      $request_data
     *      name|string
     *      username|string
     *      email|string
     *      password|string                 Encrypted password
     *      flag_is_admin|bool              Default: false
     *      flag_reset_password|bool        Default: false
     *      flag_skip_confirmation|bool     Default: true
     *      flag_can_create_group|bool      Default: false
     *
     *   @return array                      API Response
     */
    public function create($request_data = [])
    {
        try {

            // Check if username exists already
            $existing_username = $this->gitlab_api_client->users()->all(['search' => $request_data['username']]);

            if(count($existing_username) > 0) {
                Log::notice('The username matched an existing user in the GitLab API. The GitLab user has not been created.', [
                    'environment_instance_id' => $this->environment_instance->id,
                    'name' => $request_data['name'],
                    'email' => $request_data['email'],
                    'username' => $request_data['username'],
                    'method' => 'Vendor/Gitlab/UserService::create'
                ]);
                return null;
            }

            // Check if email exists already
            $existing_email = $this->gitlab_api_client->users()->all(['search' => $request_data['email']]);

            if(count($existing_email) > 0) {
                Log::notice('The email matched an existing user in the GitLab API. The GitLab user has not been created.', [
                    'environment_instance_id' => $this->environment_instance->id,
                    'name' => $request_data['name'],
                    'email' => $request_data['email'],
                    'username' => $request_data['username'],
                    'method' => 'Vendor/Gitlab/UserService::create'
                ]);
                return null;
            }

            // Create GitLab perpetual user account
            $gitlab_user = $this->gitlab_api_client->users()->create(
                $request_data['email'],
                $request_data['password'],
                [
                    'username' => $request_data['username'],
                    'name' => $request_data['name'],
                    'admin' => Arr::get($request_data, 'flag_is_admin', false),
                    'reset_password' => Arr::get($request_data, 'flag_reset_password', false),
                    'skip_confirmation' => Arr::get($request_data, 'flag_skip_confirmation', true),
                    'can_create_group' => Arr::get($request_data, 'flag_can_create_group', false),
                ]
            );

            return $gitlab_user;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return null;
        }

    }

    /**
     *   Get a GitLab User
     *   https://github.com/GitLabPHP/Client/blob/11.4/src/Api/Users.php#L71-L79
     *   https://docs.gitlab.com/ee/api/users.html#for-admin
     *
     *   @param  integer  $id       API ID of the GitLab User
     *
     *   @return array              API response
     */
    public function get($id)
    {
        try {

            // Use the API to get the details of the existing GitLab user
            $user = $this->gitlab_api_client->users()->show($id);

            return $user;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return null;
        }
    }

    /**
     *   Update a GitLab User
     *   https://github.com/GitLabPHP/Client/blob/10.0/src/Api/Users.php
     *   https://docs.gitlab.com/ee/api/users.html#user-modification
     *
     *   @param  int    $id             API ID of the GitLab User
     *   @param  array  $request_data   See the API docs for fields and values
     *
     *   @return array                  API response
     */
    public function update($id, $request_data = [])
    {
        try {

            // Use the API to update the existing GitLab user
            $user = $this->gitlab_api_client->users()->update($id, $request_data);

            return $user;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return null;
        }
    }

    /**
     *   Destroy the GitLab user
     *   https://github.com/GitLabPHP/Client/blob/11.4/src/Api/Users.php#L212-L225
     *   https://docs.gitlab.com/ee/api/users.html#user-deletion
     *
     *   @param  uuid $id       GitLab User API ID
     *   @param  array $params  Optional parameters array
     *      hard_delete|bool    If true, contributions that would usually be
     *                              moved to the ghost user are deleted instead,
     *                              as well as groups owned solely by this user.
     *
     *   @return bool
     */
    public function destroy($id, array $params = [])
    {
        try {

            // Use the API to remove a group
            $user = $gitlab_api_client->users()->remove($id, $params);

            return true;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return false;
        }
    }
}
