<?php

namespace App\Services\V1\Vendor\Gitlab;

use App\Services\V1\Vendor\Gitlab\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class GroupService extends BaseService
{

    public function __construct($environment_instance_id)
    {
        // Call BaseService methods to establish API connection
        $this->setEnvironmentInstance($environment_instance_id);
        $this->setGitlabApiClient();
    }

    /**
     *   Create a GitLab Group
     *   https://github.com/GitLabPHP/Client/blob/10.0/src/Api/Groups.php
     *   https://docs.gitlab.com/ee/api/groups.html#new-group
     *
     *   @param  array  $request_data
     *      name|string                 Display name of the group
     *      path|string                 Alpha-dash slug of the group
     *      description|string|nullable Short description of the group
     *      visibility|string|nullable  private|internal|public
     *      parent_id|int               ID of the parent group
     *
     *   @return array              API response
     */
    public function create($request_data = [])
    {
        try {

            // Use the API to create a GitLab group
            // https://github.com/GitLabPHP/Client/blob/10.0/src/Api/Groups.php
            // @param string $name
            // @param string $path
            // @param string $description
            // @param string $visibility
            // @param bool   $lfs_enabled
            // @param bool   $request_access_enabled
            // @param int    $parent_id
            // @param int    $shared_runners_minutes_limit
            $group = $this->gitlab_api_client->groups()->create(
                $request_data['name'],
                $request_data['path'],
                array_key_exists('description', $request_data) ? $request_data['description'] : '',
                array_key_exists('visibility', $request_data) ? $request_data['visibility'] : 'private',
                array_key_exists('lfs_enabled', $request_data) ? $request_data['lfs_enabled'] : false,
                array_key_exists('request_access_enabled', $request_data) ? $request_data['request_access_enabled'] : false,
                array_key_exists('parent_id', $request_data) ? $request_data['parent_id'] : null,
                array_key_exists('shared_runners_minutes_limit', $request_data) ? $request_data['shared_runners_minutes_limit'] : null,
            );

            //
            // Remove select keys from the API response
            // ------------------------------------------------
            // We remove nested array keys from API result since
            // they contain unnecessary meta data and throw
            // errors with nested array outputs when parsing
            // as a string.
            //

            if(array_key_exists('shared_with_groups', $group)) {
                unset($group['shared_with_groups']);
            }

            // Deprecated and will be removed in API v5
            if(array_key_exists('projects', $group)) {
                unset($group['projects']);
            }

            // Deprecated and will be removed in API v5
            if(array_key_exists('shared_projects', $group)) {
                unset($group['shared_projects']);
            }

            return $group;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return null;
        }

    }

    /**
     *   Get a GitLab Group
     *   https://github.com/GitLabPHP/Client/blob/10.0/src/Api/Groups.php
     *   https://docs.gitlab.com/ee/api/groups.html#details-of-a-group
     *
     *   @param  integer  $id
     *
     *   @return array              API response
     */
    public function get($id)
    {
        try {

            // Use the API to get the details of the existing GitLab group
            // https://docs.gitlab.com/ee/api/groups.html#details-of-a-group
            $group = $this->gitlab_api_client->groups()->show($id);

            //
            // Remove select keys from the API response
            // ------------------------------------------------
            // We remove nested array keys from API result since
            // they contain unnecessary meta data and throw
            // errors with nested array outputs when parsing
            // as a string.
            //

            if(array_key_exists('shared_with_groups', $group)) {
                unset($group['shared_with_groups']);
            }

            // Deprecated and will be removed in API v5
            if(array_key_exists('projects', $group)) {
                unset($group['projects']);
            }

            // Deprecated and will be removed in API v5
            if(array_key_exists('shared_projects', $group)) {
                unset($group['shared_projects']);
            }

            return $group;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return null;
        }
    }

    /**
     *   Update a GitLab Group
     *   https://github.com/GitLabPHP/Client/blob/10.0/src/Api/Groups.php
     *   https://docs.gitlab.com/ee/api/groups.html#update-group
     *
     *   @param  array  $request_data
     *      name|string                 Display name of the group
     *      path|string                 Alpha-dash slug of the group
     *      description|string|nullable Short description of the group
     *      visibility|string|nullable  private|internal|public
     *
     *   @return array              API response
     */
    public function update($id, $request_data = [])
    {
        try {

            $group = $this->gitlab_api_client->groups()->update($id, [
                'name' => $request_data['name'],
                'path' => $request_data['path'],
                'description' => array_key_exists('description', $request_data) ? $request_data['description'] : null,
                'visibility' => array_key_exists('visibility', $request_data) ? $request_data['visibility'] : 'private',
            ]);

            //
            // Remove select keys from the API response
            // ------------------------------------------------
            // We remove nested array keys from API result since
            // they contain unnecessary meta data and throw
            // errors with nested array outputs when parsing
            // as a string.
            //

            if(array_key_exists('shared_with_groups', $group)) {
                unset($group['shared_with_groups']);
            }

            // Deprecated and will be removed in API v5
            if(array_key_exists('projects', $group)) {
                unset($group['projects']);
            }

            // Deprecated and will be removed in API v5
            if(array_key_exists('shared_projects', $group)) {
                unset($group['shared_projects']);
            }

            return $group;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return null;
        }
    }

    /**
     *   Destroy the GitLab group
     *   https://github.com/GitLabPHP/Client/blob/10.0/src/Api/Groups.php
     *   https://docs.gitlab.com/ee/api/groups.html#remove-group
     *
     *   @param  uuid $id       Cloud Realm ID
     *
     *   @return bool
     */
    public function destroy($id)
    {

        try {

            // Use the API to remove a group
            // https://docs.gitlab.com/ee/api/groups.html#remove-group
            // https://github.com/GitLabPHP/Client/blob/10.0/src/Api/Groups.php
            $group = $gitlab_api_client->groups()->remove($id);

            return true;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return false;
        }

    }

    /**
     *   Create Member (Add User) for Group
     *   https://github.com/GitLabPHP/Client/blob/11.4/src/Api/Groups.php#L169-L182
     *   https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project
     *
     *   @param  array  $request_data
     *      gitlab_group_id|int         GitLab Group ID
     *      gitlab_user_id|int          GitLab User ID
     *      access_level|int            Permission level for group
     *                                      10 => Guest access
     *                                      20 => Reporter access
     *                                      30 => Developer access
     *                                      40 => Maintainer access
     *                                      50 => Owner access
     *
     *   @return array
     *      "id": 1,
     *      "username": "raymond_smith",
     *      "name": "Raymond Smith",
     *      "state": "active",
     *      "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
     *      "web_url": "http://192.168.1.8:3000/root",
     *      "expires_at": "2012-10-22T14:13:35Z",
     *      "access_level": 30,
     *      "email": "john@example.com",
     *      "group_saml_identity": null
     */
    public function createMember($request_data = [])
    {
        try {

            // Add GitLab user to group
            $gitlab_group_member = $this->gitlab_api_client->groups()->addMember(
                $request_data['gitlab_group_id'],
                $request_data['gitlab_user_id'],
                $request_data['access_level']
            );

            return $gitlab_group_member;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return false;
        }
    }

    /**
     *   Update Member for Group
     *   https://github.com/GitLabPHP/Client/blob/11.4/src/Api/Groups.php#L169-L182
     *   https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project
     *
     *   @param  array  $request_data
     *      gitlab_group_id|int         Gitlab Group ID
     *      gitlab_user_id|int          GitLab User ID
     *      access_level|int            Permission level for group
     *                                      10 => Guest access
     *                                      20 => Reporter access
     *                                      30 => Developer access
     *                                      40 => Maintainer access
     *                                      50 => Owner access
     *
     *   @return array
     *      "id": 1,
     *      "username": "raymond_smith",
     *      "name": "Raymond Smith",
     *      "state": "active",
     *      "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
     *      "web_url": "http://192.168.1.8:3000/root",
     *      "expires_at": "2012-10-22T14:13:35Z",
     *      "access_level": 30,
     *      "email": "john@example.com",
     *      "group_saml_identity": null
     */
    public function updateMember($request_data = [])
    {
        try {

            // Update GitLab group user
            $gitlab_group_member = $this->gitlab_api_client->groups()->saveMember(
                $request_data['gitlab_group_id'],
                $request_data['gitlab_user_id'],
                $request_data['access_level']
            );

            return $gitlab_group_member;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return false;
        }

    }

    /**
     *   Delete Member from Group
     *   https://github.com/GitLabPHP/Client/blob/11.4/src/Api/Groups.php#L169-L182
     *   https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project
     *
     *   @param  array  $request_data
     *      gitlab_group_id|int         GitLab Group ID
     *      gitlab_user_id|int          GitLab User ID
     *
     *   @return null
     */
    public function destroyMember($request_data = [])
    {
        try {

            // Update GitLab group user
            $gitlab_group_member = $this->gitlab_api_client->groups()->removeMember(
                $request_data['gitlab_group_id'],
                $request_data['gitlab_user_id']
            );

            return null;

        } catch(\Gitlab\Exception\ErrorException $e) {
            $this->handleException($e, get_class(), $request_data['path']);
            return null;
        }
    }

}
