<?php

namespace App\Http\Controllers;

use App\Models;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as LaravelBaseController;

class BaseController extends LaravelBaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $model;
    protected $model_human_name;
    protected $model_eager_relationships = [];
    protected $model_lazy_relationships = [];
    protected $model_order_by;
    protected $service;
    protected $base_view_path = 'admin.path.to.folder';
    protected $base_route_path = 'admin.placeholder';
    protected $dashboard_route = 'admin.dashboard.index';


    /**
     *   Get the default (only) tenant from the database
     *
     *   @return Eloquent Model     AuthTenant
     */
    public function defaultAuthTenant()
    {

        // Auth Tenant Configuration
        // --------------------------------------------------------------------
        // In this version, HackyStack has database support for multiple tenants
        // however this has not been fully implemented yet. It is assumed that
        // there will only be one record in the auth_tenants database.
        //

        // Verify that multiple tenants do not exist
        $auth_tenant_count = Models\Auth\AuthTenant::count();
        if($auth_tenant_count >= 2) {
            $error_message = 'There are multiple authentication tenants. HackyStack does not support multiple tenants yet. To fix this error, delete the newly created tenant using the `auth-provider:delete` CLI command.';

            Log::channel('hackystack-auth')->critical(
                $error_message,
                [
                    'log_event_type' => 'multiple-auth-providers-not-allowed',
                    'log_class' => get_class(),
                    'ip_address' => config('demosys.log.ip_address.enabled') ? $request->ip() : null,
                    'user_agent' => config('demosys.log.user_agent.enabled') ? $request->userAgent() : null,
                ]
            );

            abort('501', $error_message);

        } else {

            // Get the only tenant in the database
            $auth_tenant = Models\Auth\AuthTenant::firstOrFail();

            return $auth_tenant;

        }

    }

    /**
     * Return the model for the resource.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return app($this->model);
    }

    /**
     * Return the human readable name for the resource.
     *
     * @return string
     */
    public function modelHumanName()
    {
        return $this->model_human_name;
    }

    /**
     * Return an array of relationships to eager load for this resource.
     *
     * @return string
     */
    public function modelEagerRelationships()
    {
        return $this->model_eager_relationships;
    }

    /**
     * Return an array of relationships to lazy load and chunk for this resource.
     *
     * @return string
     */
    public function modelLazyRelationships()
    {
        return $this->model_lazy_relationships;
    }

    /**
     * Return the orderBy column name for list of resources.
     *
     * @return string
     */
    public function orderBy()
    {
        return $this->order_by;
    }

    /**
     * Return the service class.
     *
     * @return class \App\Services\V1\ServiceName
     */
    public function service()
    {
        return app($this->service);
    }

    /**
     *   Returns the base view path for the resource
     *
     *   @return string     Ex. admin.path.to.folder
     */
    protected function baseViewPath()
    {
        return $this->base_view_path;
    }

    /**
     *   Returns the base route path for the resource
     *
     *   @return string     Ex. admin.placeholder
     */
    protected function baseRoutePath()
    {
        return $this->base_route_path;
    }

    /**
     *   Returns the dashboard route that is used in a delete request.
     *
     *   @return string     Ex. admin.dashboard.index
     */
    protected function dashboardRoute()
    {
        return $this->dashboard_route;
    }

    public function store(Request $request)
    {
        // Validate form request input matches rules in model
        $validator = Validator::make(
                $request->all(),
                $this->model()->storeRules(),
                $this->model()->validationMessages()
            )->validate();

        // Create record using service method
        $record = $this->service()->store($request->all());

        // Flash message
        $request->session()->flash('alert-success', 'The '.$this->modelHumanName().' has been created. ID '.$record->short_id);

        return redirect()->route($this->base_route_path.'show', ['id' => $record->short_id]);
    }

    public function update($id, Request $request)
    {
        // Get record by ID
        $record = $this->model()
            ->where('id', $id)
            ->orWhere('short_id', $id)
            ->firstOrFail();

        // Validate form request input matches rules in model
        $validator = Validator::make(
                $request->all(),
                $this->model()->updateRules(),
                $this->model()->validationMessages()
            )->validate();

        // Update record using service method
        $record = $this->service()->update($record->id, $request->all());

        // Flash message
        $request->session()->flash('alert-success', 'The '.$this->modelHumanName().' has been updated.');

        return redirect()->route($this->base_route_path.'show', ['id' => $record->short_id]);
    }

    public function delete($id, Request $request)
    {
        // Get record by ID
        $record = $this->model()
            ->where('id', $id)
            ->orWhere('short_id', $id)
            ->firstOrFail();

        // Delete record using service method
        $action = $this->service()->delete($record->id);

        // Flash message
        $request->session()->flash('alert-danger', 'The '.$this->modelHumanName().' has been deleted.');

        return redirect()->route($this->base_route_path.'index');
    }

    public function restore($id, Request $request)
    {
        // Get record by ID
        $record = $this->model()
            ->withTrashed()
            ->where('id', $id)
            ->orWhere('short_id', $id)
            ->firstOrFail();

        // Restore record using service method
        $action = $this->service()->restore($record->id);

        // Flash message
        $request->session()->flash('alert-success', 'The '.$this->modelHumanName().' has been restored.');

        return redirect()->route($this->base_route_path.'show', ['id' => $record->short_id]);
    }

    public function destroy($id, Request $request)
    {
        // Get record by ID
        $record = $this->model()
            ->where('id', $id)
            ->orWhere('short_id', $id)
            ->firstOrFail();

        // Restore record using service method
        $action = $this->service()->destroy($record->id);

        // Flash message
        $request->session()->flash('alert-danger', 'The '.$this->modelHumanName().' has been destroyed.');

        return redirect()->route($this->base_route_path.'index');
    }

}
