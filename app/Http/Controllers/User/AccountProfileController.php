<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AccountProfileController extends BaseController
{
    public function __construct()
    {
        // TODO Additional validation of roles and permissions
    }

    public function index(Request $request)
    {

        $auth_user = Models\Auth\AuthUser::query()
            ->with([
                'authProvider',
                'authTenant',
                'authGroups'
            ])
            ->where('id', $request->user()->id)
            ->firstOrFail();

        $provider_meta_data = collect($auth_user->provider_meta_data)->sortKeys();

        return view('user.account.profile.index', compact([
            'request',
            'auth_user',
            'provider_meta_data'
        ]));

    }

}
