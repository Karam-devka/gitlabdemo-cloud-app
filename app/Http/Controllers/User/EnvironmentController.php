<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class EnvironmentController extends BaseController
{
    public function __construct()
    {
        // TODO Additional validation of roles and permissions
    }

    public function index(Request $request)
    {

        $auth_user_id = $request->user()->id;

        $available_instances = Models\Environment\EnvironmentInstance::query()
            ->whereDoesntHave('environmentInstanceUsers', function (Builder $query) use ($auth_user_id) {
                $query->where('auth_user_id', $auth_user_id);
            })->orderBy('user_base_url')
            ->get();

        $provisioned_instances = Models\Environment\EnvironmentInstance::with([
                // Only load users that match the user ID
                // Be very careful if changing this since credentials could be exposed
                'environmentInstanceUsers' => function($query) use ($auth_user_id) {
                    $query->where('auth_user_id', $auth_user_id);
                }
            ])->whereHas('environmentInstanceUsers', function (Builder $query) use ($auth_user_id) {
                // Only load instances that has a user relationship that matches the ID
                $query->where('auth_user_id', $auth_user_id);
            })->orderBy('created_at', 'DESC')
            ->get();

        return view('user.environments.index', compact([
            'request',
            'available_instances',
            'provisioned_instances'
        ]));

    }

}
