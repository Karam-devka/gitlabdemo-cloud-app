<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class EnvironmentInstanceUserController extends BaseController
{
    public function __construct()
    {
        // TODO Additional validation of roles and permissions
    }

    public function store(Request $request)
    {

        $auth_user_id = $request->user()->id;

        // Get user account to verify ID belongs to a real user
        $auth_user = Models\Auth\AuthUser::query()
            ->where('id', $auth_user_id)
            ->firstOrFail();

        // Get environment instance and verify that user is not already attached
        $environment_instance = Models\Environment\EnvironmentInstance::query()
            ->where('id', $request->environment_instance_id)
            ->whereDoesntHave('environmentInstanceUsers', function (Builder $query) use ($auth_user_id) {
                $query->where('auth_user_id', $auth_user_id);
            })->firstOrFail();

        // Initialize service
        $environmentInstanceUserService = new Services\V1\Environment\EnvironmentInstanceUserService();

        // Use service to create record
        $record = $environmentInstanceUserService->store([
            'auth_tenant_id' => $auth_user->auth_tenant_id,
            'auth_user_id' => $auth_user->id,
            'environment_instance_id' => $environment_instance->id
        ]);

        // Flash message
        $request->session()->flash('alert-success', 'Your credentials have been successfully generated (ID '.$record->short_id.'). Click the View Credentials button for instructions on using your credentials.');

        // Redirect to the environments dashboard
        return redirect()->route('user.environments.index');

    }

}
