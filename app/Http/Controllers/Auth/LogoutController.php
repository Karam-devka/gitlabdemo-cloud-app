<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LogoutController extends BaseController
{
    use AuthenticatesUsers;

    public function __construct()
    {
        //
    }

    public function endSession(Request $request)
    {

        // Use the method in AuthenicatesUsers to handle logout
        $this->logout($request);

        return redirect()->route('auth.login');

    }
}
