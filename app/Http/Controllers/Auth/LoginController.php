<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class LoginController extends BaseController
{
    public function __construct()
    {
        //
    }

    public function show(Request $request)
    {

        // SSO Provider Variables and Configuration Validation
        // --------------------------------------------------------------------
        // SSO providers are configured in the auth_providers database table.
        //
        $enabled_sso_providers = Models\Auth\AuthProvider::query()
            ->where('auth_tenant_id', $this->defaultAuthTenant()->id)
            ->where('slug', '!=', 'local')
            ->where('flag_enabled', 1)
            ->get();

        // Validate that Provider Variables are set in `.env` file
        // --------------------------------------------------------------------
        // Loop through providers to ensure that variables are set and return
        // 501 (Not Implemented) error messages that are shown to developers
        // when `APP_DEBUG=true` is set in the `.env` file. In a production
        // environment when `APP_DEBUG=false`, the errors below will be sent
        // to the logs or 3rd party bug report service (ex. Bugsnag or Sentry)
        // and the user will see a generic 501 Error message.
        //
        // Oops! An Error Occurred
        // The server returned a "501 Not Implemented".
        // Something is broken. Please let us know what you were doing when
        // this error occurred. We will fix it as soon as possible. Sorry for
        // any inconvenience caused.
        //
        foreach($enabled_sso_providers as $provider) {
            if($provider->base_url == null) {

                $error_message = 'The '.config('demosys.auth.'.$provider->slug.'.human_name').' authentication provider is enabled but the `base_url` is not configured in the database. You should also check that the `client_id` and `client_secret` variables are set. You can bypass this error by disabling this authentication provider by setting the `flag_enabled` value to `0`. You can edit this provider using the `auth-provider:edit '.$provider->short_id.'` CLI command.';

                Log::channel('hackystack-auth')->critical(
                    $error_message,
                    [
                        'log_event_type' => 'provider-base-url-not-configured',
                        'log_class' => get_class(),
                        'ip_address' => config('demosys.log.ip_address.enabled') ? $request->ip() : null,
                        'user_agent' => config('demosys.log.user_agent.enabled') ? $request->userAgent() : null,
                        'auth_tenant_id' => $this->defaultAuthTenant()->id,
                        'auth_provider_id' => $provider->id,
                        'auth_provider_slug' => $provider->slug,
                    ]
                );

                abort('501', $error_message);

            }
            if($provider->client_id == null) {

                $error_message = 'The '.config('demosys.auth.'.$provider->slug.'.human_name').' authentication provider is enabled but the `client_id` is not configured in the database. You should also check that the `base_url` and `client_secret` variables are set. You can bypass this error by disabling this authentication provider by setting the `flag_enabled` value to `0`. You can edit this provider using the `auth-provider:edit '.$provider->short_id.'` CLI command.';

                Log::channel('hackystack-auth')->critical(
                    $error_message,
                    [
                        'log_event_type' => 'provider-client-id-not-configured',
                        'log_class' => get_class(),
                        'ip_address' => config('demosys.log.ip_address.enabled') ? $request->ip() : null,
                        'user_agent' => config('demosys.log.user_agent.enabled') ? $request->userAgent() : null,
                        'auth_tenant_id' => $this->defaultAuthTenant()->id,
                        'auth_provider_id' => $provider->id,
                        'auth_provider_slug' => $provider->slug,
                    ]
                );

                abort('501', $error_message);

            }
            if($provider->client_secret == null) {

                $error_message = 'The '.config('demosys.auth.'.$provider->slug.'.human_name').' authentication provider is enabled but the `client_secret` is not configured in the database. You should also check that the `base_url` and `client_id` variables are set. You can bypass this error by disabling this authentication provider by setting the `flag_enabled` value to `0`. You can edit this provider using the `auth-provider:edit '.$provider->short_id.'` CLI command.';

                Log::channel('hackystack-auth')->critical(
                    $error_message,
                    [
                        'log_event_type' => 'provider-client-secret-not-configured',
                        'log_class' => get_class(),
                        'ip_address' => config('demosys.log.ip_address.enabled') ? $request->ip() : null,
                        'user_agent' => config('demosys.log.user_agent.enabled') ? $request->userAgent() : null,
                        'auth_tenant_id' => $this->defaultAuthTenant()->id,
                        'auth_provider_id' => $provider->id,
                        'auth_provider_slug' => $provider->slug,
                    ]
                );

                abort('501', $error_message);

            }

        }

        // Local Authentication Form
        // --------------------------------------------------------------------
        // If 'form' config variable is set to local authentication, show the
        // login form and a list of enabled authentication providers
        //
        if(config('demosys.auth.form') == 'local') {

            // Get local authentication provider from database
            $local_provider = Models\Auth\AuthProvider::query()
                ->where('auth_tenant_id', $this->defaultAuthTenant()->id)
                ->where('slug', 'local')
                ->firstOrFail();

            if($local_provider->flag_enabled == 0) {

                $error_message = 'Local authentication is not enabled. Set the `local` provider `flag_enabled` value to `1` in the `auth_providers` database table by using the `auth-provider:edit '.$local_provider->short_id.' --flag_enabled=1` CLI command.';

                Log::channel('hackystack-auth')->critical(
                    $error_message,
                    [
                        'log_event_type' => 'local-auth-provider-not-enabled',
                        'log_class' => get_class(),
                        'ip_address' => config('demosys.log.ip_address.enabled') ? $request->ip() : null,
                        'user_agent' => config('demosys.log.user_agent.enabled') ? $request->userAgent() : null,
                        'auth_tenant_id' => $this->defaultAuthTenant()->id,
                        'auth_provider_id' => $local_provider->id,
                        'auth_provider_slug' => $local_provider->slug,
                    ]
                );

                abort('501', $error_message);

            }

            return view('auth.local.login', compact([
                'request',
                'enabled_sso_providers',
            ]));

        }

        // SSO Providers List
        // --------------------------------------------------------------------
        // If 'form' config variable is set to providers, show a page without a
        // login form, and show a list of enabled authentication providers that
        // a user can select to authenticate with.
        //
        elseif(config('demosys.auth.form') == 'providers') {

            // Check that at least provider has been enabled
            if($enabled_sso_providers->count() == 0) {

                $error_message = 'No authentication providers are enabled. Set `flag_enabled` value to `1` for a provider in the `auth_providers` database table by using the `auth-provider:list` CLI command to get a list of providers and `auth-provider:edit a1b2c3d4` to edit a provider.';

                Log::channel('hackystack-auth')->critical(
                    $error_message,
                    [
                        'log_event_type' => 'no-auth-providers-enabled',
                        'log_class' => get_class(),
                        'ip_address' => config('demosys.log.ip_address.enabled') ? $request->ip() : null,
                        'user_agent' => config('demosys.log.user_agent.enabled') ? $request->userAgent() : null,
                        'auth_tenant_id' => $this->defaultAuthTenant()->id,
                    ]
                );

                abort('501', $error_message);

            }

            return view('auth.providers.login', compact([
                'request',
                'enabled_sso_providers'
            ]));
        }

        // Default SSO Provider Redirect
        // --------------------------------------------------------------------
        // If `DEMOSYS_AUTH_FORM` is set to any other value, assume that this is an
        // SSO provider, validate that the provider is enabled and redirect to
        // respective route that handles the redirection to the SSO provider
        // login form in Controllers\Auth\{Provider}\LoginController.
        //
        else {

            $sso_provider = Models\Auth\AuthProvider::query()
                ->where('slug', config('demosys.auth.form'))
                ->first();

            if($sso_provider == null) {

                $error_message = 'The `DEMOSYS_AUTH_FORM` value `'.config('demosys.auth.form').'` in your `.env` file is not a valid provider. Use the `auth-provider:list` CLI command to get a list of providers.';

                Log::channel('hackystack-auth')->critical(
                    $error_message,
                    [
                        'log_event_type' => 'invalid-env-value-for-hs-auth-form',
                        'log_class' => get_class(),
                        'ip_address' => config('demosys.log.ip_address.enabled') ? $request->ip() : null,
                        'user_agent' => config('demosys.log.user_agent.enabled') ? $request->userAgent() : null,
                        'auth_tenant_id' => $this->defaultAuthTenant()->id,
                        'hs_auth_form' => config('demosys.auth.form')
                    ]
                );

                abort('501', $error_message);

            }

            return redirect()->route('auth.'.config('demosys.auth.form').'.login');

        }

    }

    // FAQ: Where are the form post methods?
    // ------------------------------------------------------------------------
    // This controller serves the universal login form for the application. The
    // local authentication methods and controllers for each provider can be
    // found in the Controllers\Auth\{Provider}\LoginController. For local
    // authentication, you will find the RegisterController, ResetController,
    // VerifyController in Controllers\Auth\Local\*.

}
