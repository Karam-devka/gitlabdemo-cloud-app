<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthGroupUserDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group-user:delete
                            {short_id? : The short ID of the group user.}
                            {--G|auth_group_slug= : The slug of the Authentication Group}
                            {--U|auth_user_short_id= : The short ID of the Authentication User}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an Authentication Group User by ID or slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug and user short ID
        $auth_group_slug = $this->option('auth_group_slug');
        $auth_user_short_id = $this->option('auth_user_short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $auth_group_slug == null) {
            $this->error('You did not specify the short_id or slug to lookup the record.');
            $this->line('You can get a list of group users using `auth-group:get --slug=my-group`');
            $this->line('You can get a list of user groups using `auth-user:get --short_id=a1b2c3d4`');
            $this->line('You can delete by short ID using `auth-group-user:delete a1b2c3d4`');
            $this->line('You can delete by slug using `auth-group-user:delete --auth_group_slug=my-group --auth_user_short_id=a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_group_user = Models\Auth\AuthGroupUser::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($auth_group_slug != null) {

            $auth_group = Models\Auth\AuthGroup::query()
                ->where('slug', $auth_group_slug)
                ->first();

            // If record not found, return an error message
            if($auth_group == null) {
                $this->error('No group was found with that slug.');
                $this->error('');
                die();
            }

            $auth_user = Models\Auth\AuthUser::query()
                ->where('short_id', $auth_user_short_id)
                ->first();

            // If record not found, return an error message
            if($auth_user == null) {
                $this->error('No user was found with that ID.');
                $this->error('');
                die();
            }

            $auth_group_user = Models\Auth\AuthGroupUser::query()
                ->where('auth_group_id', $auth_group->id)
                ->where('auth_user_id', $auth_user->id)
                ->first();

        }

        // If record not found, return an error message
        if($auth_group_user == null) {
            $this->error('No group user relationship record found.');
            $this->error('');
            die();
        }

        // Call the get method to display the tables of values for the record.
        $this->call('auth-group-user:get', [
            'short_id' => $auth_group_user->short_id,
        ]);

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $authGroupUserService = new Services\V1\Auth\AuthGroupUserService();

        // Use service to delete record
        $authGroupUserService->destroy($auth_group_user->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
