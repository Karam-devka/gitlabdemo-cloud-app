<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;

class AuthProviderGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider:get
                            {short_id? : The short ID of the provider.}
                            {--T|auth_tenant_slug= : The slug of the tenant that the provider is associated with.}
                            {--P|auth_provider_slug= : The slug of the provider.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an authentication provider by ID or slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Auth Provider - Get Record');

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');
        $auth_provider_slug = $this->option('auth_provider_slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $auth_provider_slug == null) {
            $this->error('You did not specify the short_id or slug to lookup the record.');
            $this->comment('You can use the `auth-provider:get a1b2c3d4` command to lookup a provider by ID.');
            $this->comment('You can use the `auth-provider-get --auth_tenant_slug={tenant} --auth_provider_slug={provider}` command to lookup a provider with a slug.');
            $this->comment('You can use the `auth-provider:list` command to get a list of records.');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_provider = Models\Auth\AuthProvider::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($auth_provider_slug != null) {
            $auth_provider = Models\Auth\AuthProvider::query()
                ->whereHas('authTenant', function(Builder $query) use ($auth_tenant_slug) {
                    $query->where('slug', $auth_tenant_slug);
                })
                ->where('slug', $auth_provider_slug)
                ->first();
        }

        // If record not found, return an error message
        if($auth_provider == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Provider Values
        $this->comment('');
        $this->comment('Auth Provider');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_provider->id
                ],
                [
                    'short_id',
                    $auth_provider->short_id
                ],
                [
                    'auth_tenant_id',
                    $auth_provider->auth_tenant_id
                ],
                [
                    'slug',
                    $auth_provider->slug
                ],
                [
                    'base_url',
                    $auth_provider->base_url
                ],
                [
                    'client_id',
                    $auth_provider->client_id ? decrypt($auth_provider->client_id) : null
                ],
                [
                    'client_secret',
                    $auth_provider->client_secret ? decrypt($auth_provider->client_secret) : null
                ],
                [
                    'flag_enabled',
                    $auth_provider->flag_enabled
                ],
                [
                    'created_at',
                    $auth_provider->created_at
                ],
                [
                    'updated_at',
                    $auth_provider->updated_at
                ],
            ]
        );

        $this->line('');

    }

}
