<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class AuthProviderFieldList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider-field:list
                            {--T|auth_tenant_slug= : The slug of the tenant that provider fields belong to}
                            {--P|auth_provider_slug= : The slug of the Authentication Provider to filter by}
                            {--search=* : Wildcard search of provider_key and user_column.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Authentication Provider Fields';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get auth provider slug
        $auth_provider_slug = $this->option('auth_provider_slug');

        // Get search keywords
        $search_array = $this->option('search');

        // Get list of records from database
        $records = Models\Auth\AuthProviderField::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($auth_provider_slug) {
                if($auth_provider_slug) {
                    $query->whereHas('authProvider', function (Builder $query) use ($auth_provider_slug) {
                        $query->where('slug', $auth_provider_slug);
                    });
                }
            })->where(function($query) use ($search_array) {
                // If search keywords were specified, the array will not be empty
                if(count($search_array) > 0) {
                    foreach($search_array as $search_option) {
                        $query->where('provider_key', 'LIKE', '%$search_option%');
                        $query->orWhere('user_column', 'LIKE', '%$search_option%');
                    }
                }
            })
            ->orderBy('created_at')
            ->get();

        // Loop through fields in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $provider_field_record) {
            $provider_field_rows[] = [
                'short_id' => $provider_field_record->short_id,
                'auth_tenant' => '['.$provider_field_record->authTenant->short_id.'] '.$provider_field_record->authTenant->slug,
                'auth_provider' => '['.$provider_field_record->authProvider->short_id.'] '.$provider_field_record->authProvider->slug,
                'provider_key' => $provider_field_record->provider_key,
                'user_column' => $provider_field_record->user_column,
                'created_at' => $provider_field_record->created_at->toIso8601String(),
            ];
        }

        $this->comment('');
        $this->comment('Auth Provider Fields - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Provider', 'Provider Key', 'User Column', 'Created at'];
            $this->table($headers, $provider_field_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
