<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthUserGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-user:get
                            {short_id? : The short ID of the user.}
                            {--R|without-child-relationships}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Authentication User by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of users using `auth-user:list`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_user = Models\Auth\AuthUser::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($auth_user == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Group Values
        $this->comment('');
        $this->comment('Auth User');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_user->id
                ],
                [
                    'short_id',
                    $auth_user->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$auth_user->authTenant->short_id.'] '.$auth_user->authTenant->slug
                ],
                [
                    '[relationship] authProvider',
                    '['.$auth_user->authProvider->short_id.'] '.$auth_user->authProvider->slug
                ],
                [
                    'full_name',
                    $auth_user->full_name
                ],
                [
                    'job_title',
                    $auth_user->job_title
                ],
                [
                    'organization_name',
                    $auth_user->organization_name
                ],
                [
                    'email',
                    $auth_user->email
                ],
                [
                    'email_recovery',
                    $auth_user->email_recovery
                ],
                [
                    'count_current_failed_logins',
                    $auth_user->count_current_failed_logins
                ],
                [
                    'flag_passwordless',
                    $auth_user->flag_passwordless
                ],
                [
                    'flag_2fa_enabled',
                    $auth_user->flag_2fa_enabled
                ],
                [
                    'flag_must_change_password',
                    $auth_user->flag_must_change_password
                ],
                [
                    'flag_account_expired',
                    $auth_user->flag_account_expired
                ],
                [
                    'flag_account_locked',
                    $auth_user->flag_account_locked
                ],
                [
                    'flag_account_verified',
                    $auth_user->flag_account_verified
                ],
                [
                    'flag_terms_accepted',
                    $auth_user->flag_terms_accepted
                ],
                [
                    'flag_privacy_accepted',
                    $auth_user->flag_privacy_accepted
                ],
                [
                    'expires_at',
                    $auth_user->expires_at ? $auth_user->expires_at->toIso8601String() : ''
                ],
                [
                    'locked_at',
                    $auth_user->locked_at ? $auth_user->locked_at->toIso8601String() : ''
                ],
                [
                    'verified_at',
                    $auth_user->verified_at ? $auth_user->verified_at->toIso8601String() : ''
                ],
                [
                    'locked_at',
                    $auth_user->locked_at ? $auth_user->locked_at->toIso8601String() : ''
                ],
                [
                    'terms_accepted_at',
                    $auth_user->terms_accepted_at ? $auth_user->terms_accepted_at->toIso8601String() : ''
                ],
                [
                    'privacy_accepted_at',
                    $auth_user->privacy_accepted_at ? $auth_user->privacy_accepted_at->toIso8601String() : ''
                ],
                [
                    'password_changed_at',
                    $auth_user->password_changed_at ? $auth_user->password_changed_at->toIso8601String() : ''
                ],
                [
                    'last_successful_login_at',
                    $auth_user->last_successful_login_at ? $auth_user->last_successful_login_at->toIso8601String() : ''
                ],
                [
                    'last_failed_login_at',
                    $auth_user->last_failed_login_at ? $auth_user->last_failed_login_at->toIso8601String() : ''
                ],
                [
                    'last_activity_at',
                    $auth_user->last_activity_at ? $auth_user->last_activity_at->toIso8601String() : ''
                ],
                [
                    'created_at',
                    $auth_user->created_at ? $auth_user->created_at->toIso8601String() : ''
                ],
                [
                    'updated_at',
                    $auth_user->updated_at ? $auth_user->updated_at->toIso8601String() : ''
                ],
                [
                    'deleted_at',
                    $auth_user->deleted_at ? $auth_user->deleted_at->toIso8601String() : ''
                ],
                [
                    'state',
                    $auth_user->state
                ],
            ]
        );

        // Check if option passed to not show child relationships
        if($this->option('without-child-relationships') == false) {

            // Call the list methods to display the tables of values for the record.

            $this->call('auth-group-user:list', [
                '--auth_user_short_id' => $auth_user->short_id
            ]);

        }

        $this->comment('');

    }
}
