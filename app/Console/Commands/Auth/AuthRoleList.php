<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class AuthRoleList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-role:list
                            {--T|auth_tenant_slug= : The slug of the tenant that providers belong to}
                            {--search=* : Wildcard search of name, slug, or description.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Authentication Roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get search keywords
        $search_array = $this->option('search');

        // Get list of records from database
        $records = Models\Auth\AuthRole::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($search_array) {
                // If search keywords were specified, the array will not be empty
                if(count($search_array) > 0) {
                    foreach($search_array as $search_option) {
                        $query->where('name', 'LIKE', '%$search_option%');
                        $query->orWhere('slug', 'LIKE', '%$search_option%');
                        $query->orWhere('description', 'LIKE', '%$search_option%');
                    }
                }
            })
            ->orderBy('created_at')
            ->get();

        // Loop through roles in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $role_record) {
            $role_rows[] = [
                'short_id' => $role_record->short_id,
                'auth_tenant' => '['.$role_record->authTenant->short_id.'] '.$role_record->authTenant->slug,
                'groups' => $role_record->authGroups()->count(),
                'name' => $role_record->name,
                'slug' => $role_record->slug,
                'created_at' => $role_record->created_at->toIso8601String(),
            ];
        }

        $this->comment('');
        $this->comment('Auth Roles - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Groups', 'Name', 'Slug', 'Created at'];
            $this->table($headers, $role_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
