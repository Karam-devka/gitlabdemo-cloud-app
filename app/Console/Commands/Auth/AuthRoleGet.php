<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthRoleGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-role:get
                            {short_id? : The short ID of the role.}
                            {--slug= : The slug of the role.}
                            {--R|without-child-relationships}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Authentication Role by ID or slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $slug = $this->option('slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short id or slug to lookup the record.');
            $this->comment('You can lookup by short ID using `auth-role:get a1b2c3d4`');
            $this->comment('You can lookup by slug using `auth-role:get --slug=example`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_role = Models\Auth\AuthRole::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($slug != null) {
            $auth_role = Models\Auth\AuthRole::query()
                ->where('slug', $slug)
                ->first();
        }

        // If record not found, return an error message
        if($auth_role == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Role Values
        $this->comment('');
        $this->comment('Auth Role');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_role->id
                ],
                [
                    'short_id',
                    $auth_role->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$auth_role->authTenant->short_id.'] '.$auth_role->authTenant->slug
                ],
                [
                    'name',
                    $auth_role->name
                ],
                [
                    'slug',
                    $auth_role->slug
                ],
                [
                    'created_at',
                    $auth_role->created_at->toIso8601String()
                ],
            ]
        );

        $this->comment('');

        // Check if option passed to not show child relationships
        if($this->option('without-child-relationships') == false) {

            // Loop through array of permissions and format for console table
            $permission_rows = [];
            foreach($auth_role->permissions as $permission) {
                $permission_rows[] = [$permission];
            }

            // Auth Role Permissions
            $this->table(
                ['Permissions'],
                $permission_rows
            );

            // Call the list methods to display the tables of values for the record.

            $this->call('auth-group-role:list', [
                '--auth_role_short_id' => $auth_role->short_id
            ]);

        }

    }
}
