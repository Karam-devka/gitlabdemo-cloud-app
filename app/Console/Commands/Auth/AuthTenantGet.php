<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthTenantGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-tenant:get
                            {short_id? : The short ID of the tenant.}
                            {--slug= : The slug of the tenant.}
                            {--R|without-child-relationships}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Authentication Tenant by ID or slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $slug = $this->option('slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short id or slug to lookup the record.');
            $this->comment('You can lookup by short ID using `auth-tenant:get a1b2c3d4`');
            $this->comment('You can lookup by slug using `auth-tenant:get --slug=example`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($slug != null) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $slug)
                ->first();
        }

        // If record not found, return an error message
        if($auth_tenant == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Tenant Values
        $this->comment('');
        $this->comment('Auth Tenant');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_tenant->id
                ],
                [
                    'short_id',
                    $auth_tenant->short_id
                ],
                [
                    'name',
                    $auth_tenant->name
                ],
                [
                    'slug',
                    $auth_tenant->slug
                ],
                [
                    'git_provider_type',
                    $auth_tenant->git_provider_type
                ],
                [
                    'git_provider_base_url',
                    $auth_tenant->git_provider_base_url
                ],
                [
                    'git_provider_base_group_id',
                    $auth_tenant->git_provider_base_group_id
                ],
                [
                    'created_at',
                    $auth_tenant->created_at->toIso8601String()
                ],
            ]
        );

        // Check if option passed to not show child relationships
        if($this->option('without-child-relationships') == false) {

            if($auth_tenant->git_meta_data != null)
            {
                $this->comment('');
                $this->comment('Git Provider Group Meta Data');
                $git_meta_data_rows = [];

                foreach($auth_tenant->git_meta_data as $meta_data_key => $meta_data_value) {
                    $git_meta_data_rows[] = [
                        $meta_data_key,
                        $meta_data_value
                    ];
                }

                $this->table(
                    ['Column', 'Value'],
                    $git_meta_data_rows
                );
            }

            // Call the list methods to display the tables of values for the record.

            $this->call('auth-group:list', [
                '--auth_tenant_slug' => $auth_tenant->slug
            ]);

            $this->call('auth-role:list', [
                '--auth_tenant_slug' => $auth_tenant->slug
            ]);

            $this->call('auth-provider:list', [
                '--auth_tenant_slug' => $auth_tenant->slug
            ]);

            $this->call('auth-user:list', [
                '--auth_tenant_slug' => $auth_tenant->slug
            ]);

        }

        $this->comment('');

    }
}
