<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class AuthProviderGroupList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider-group:list
                            {--T|auth_tenant_slug= : The slug of the tenant that provider groups belong to}
                            {--P|auth_provider_slug= : The slug of the Authentication Provider to filter by}
                            {--G|auth_group_slug= : The slug of the Authentication Group to filter by}
                            {--type= : The type of group association for provider users to filter by (default|meta)}
                            {--search=* : Wildcard search of meta_key and meta_value.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Authentication Provider Groups';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get auth provider slug
        $auth_provider_slug = $this->option('auth_provider_slug');

        // Get auth group slug
        $auth_group_slug = $this->option('auth_group_slug');

        // Get type
        $type = $this->option('type');

        // Get search keywords
        $search_array = $this->option('search');

        // Get list of records from database
        $records = Models\Auth\AuthProviderGroup::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($auth_provider_slug) {
                if($auth_provider_slug) {
                    $query->whereHas('authProvider', function (Builder $query) use ($auth_provider_slug) {
                        $query->where('slug', $auth_provider_slug);
                    });
                }
            })->where(function($query) use ($auth_group_slug) {
                if($auth_group_slug) {
                    $query->whereHas('authGroup', function (Builder $query) use ($auth_group_slug) {
                        $query->where('slug', $auth_group_slug);
                    });
                }
            })->where(function($query) use ($type) {
                if($type) {
                    $query->where('type', $type);
                }
            })->where(function($query) use ($search_array) {
                // If search keywords were specified, the array will not be empty
                if(count($search_array) > 0) {
                    foreach($search_array as $search_option) {
                        $query->where('provider_key', 'LIKE', '%$search_option%');
                        $query->orWhere('user_column', 'LIKE', '%$search_option%');
                    }
                }
            })
            ->orderBy('created_at')
            ->get();

        // Loop through groups in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $provider_group_record) {
            $provider_group_rows[] = [
                'short_id' => $provider_group_record->short_id,
                'auth_tenant' => '['.$provider_group_record->authTenant->short_id.'] '.$provider_group_record->authTenant->slug,
                'auth_provider' => '['.$provider_group_record->authProvider->short_id.'] '.$provider_group_record->authProvider->slug,
                'auth_group' => '['.$provider_group_record->authGroup->short_id.'] '.$provider_group_record->authGroup->slug,
                'type' => $provider_group_record->type,
                'meta_key' => $provider_group_record->meta_key,
                'meta_value' => $provider_group_record->meta_value,
                'created_at' => $provider_group_record->created_at->toIso8601String(),
            ];
        }

        $this->comment('');
        $this->comment('Auth Provider Groups - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Provider', 'Group', 'Type', 'Meta Key', 'Meta Value', 'Created at'];
            $this->table($headers, $provider_group_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
