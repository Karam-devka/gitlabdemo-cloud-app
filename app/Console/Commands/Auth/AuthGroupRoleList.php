<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class AuthGroupRoleList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group-role:list
                            {--T|auth_tenant_slug= : The slug of the tenant that group user relationships belong to}
                            {--G|auth_group_short_id= : The short ID of the Authentication Group to filter by}
                            {--U|auth_role_short_id= : The short ID of the Authentication Role to filter by}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Authentication Group Role Relationships';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get auth group short id
        $auth_group_short_id = $this->option('auth_group_short_id');

        // Get auth role short id
        $auth_role_short_id = $this->option('auth_role_short_id');

        // Get list of records from database
        $records = Models\Auth\AuthGroupRole::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($auth_group_short_id) {
                if($auth_group_short_id) {
                    $query->whereHas('authGroup', function (Builder $query) use ($auth_group_short_id) {
                        $query->where('short_id', $auth_group_short_id);
                    });
                }
            })->where(function($query) use ($auth_role_short_id) {
                if($auth_role_short_id) {
                    $query->whereHas('authRole', function (Builder $query) use ($auth_role_short_id) {
                        $query->where('short_id', $auth_role_short_id);
                    });
                }
            })
            ->orderBy('created_at')
            ->get();

        // Loop through groups in Eloquent model and add values to array
        foreach($records as $record) {
            $table_rows[] = [
                'short_id' => $record->short_id,
                'auth_tenant' => '['.$record->authTenant->short_id.'] '.$record->authTenant->slug,
                'auth_group' => '['.$record->authGroup->short_id.'] '.$record->authGroup->slug,
                'auth_role' => '['.$record->authRole->short_id.'] '.$record->authRole->slug,
                'created_at' => $record->created_at->toIso8601String(),
                'expires_at' => $record->expires_at ? $record->expires_at->toIso8601String() : 'never'
            ];
        }

        $this->comment('');
        $this->comment('Auth Group Role Relationships - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $table_headers = ['Short ID', 'Tenant', 'Group', 'Role', 'Created at', 'Expires at'];
            $this->table($table_headers, $table_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
