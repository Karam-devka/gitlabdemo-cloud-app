<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthGroupRoleDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group-role:delete
                            {short_id? : The short ID of the group role.}
                            {--G|auth_group_slug= : The slug of the Authentication Group}
                            {--R|auth_role_slug= : The slug of the Authentication Role}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an Authentication Group Role by ID or slugs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slugs
        $auth_group_slug = $this->option('auth_group_slug');
        $auth_role_slug = $this->option('auth_role_slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $auth_group_slug == null) {
            $this->error('You did not specify the short_id or slugs to lookup the record.');
            $this->comment('You can delete by short ID using `auth-group-role:delete a1b2c3d4`');
            $this->comment('You can delete by slug using `auth-group-role:delete --auth_group_slug=my-group --auth_role_slug=my-role`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_group_role = Models\Auth\AuthGroupRole::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($auth_group_slug != null) {

            $auth_group = Models\Auth\AuthGroup::query()
                ->where('slug', $auth_group_slug)
                ->first();

            // If record not found, return an error message
            if($auth_group == null) {
                $this->error('No group was found with that slug.');
                $this->error('');
                die();
            }

            $auth_role = Models\Auth\AuthRole::query()
                ->where('slug', $auth_role_slug)
                ->first();

            // If record not found, return an error message
            if($auth_role == null) {
                $this->error('No role was found with that slug.');
                $this->error('');
                die();
            }

            $auth_group_role = Models\Auth\AuthGroupRole::query()
                ->where('auth_group_id', $auth_group->id)
                ->where('auth_role_id', $auth_role->id)
                ->first();

        }

        // If record not found, return an error message
        if($auth_group_role == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Call the get method to display the tables of values for the record.
        $this->call('auth-group-role:get', [
            'short_id' => $auth_group_role->short_id,
        ]);

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $authGroupRoleService = new Services\V1\Auth\AuthGroupRoleService();

        // Use service to delete record
        $authGroupRoleService->destroy($auth_group_role->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
