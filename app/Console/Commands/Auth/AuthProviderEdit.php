<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;

class AuthProviderEdit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider:edit
                            {short_id? : The short ID of the provider.}
                            {--T|auth_tenant_slug= : The slug of the tenant that the provider is associated with.}
                            {--P|auth_provider_slug= : The slug of the provider.}
                            {--base_url= : The base URL used for authenticating users.}
                            {--client_id= : The client ID for the provider}
                            {--client_secret= : The secret key for the provider.}
                            {--flag_enabled= : A boolean whether provider is enabled.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Edit an authentication provider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Auth Provider - Edit Record');

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');
        $auth_provider_slug = $this->option('auth_provider_slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $auth_provider_slug == null) {
            $this->error('You did not specify the short_id or slug to lookup the record.');
            $this->comment('You can use the `auth-provider:edit a1b2c3d4` command to lookup a provider by ID.');
            $this->comment('You can use the `auth-provider-edit --auth_tenant_slug={tenant} --auth_provider_slug={provider}` command to lookup a provider with a slug.');
            $this->comment('You can use the `auth-provider:list` command to get a list of records.');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_provider = Models\Auth\AuthProvider::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($auth_provider_slug != null) {
            $auth_provider = Models\Auth\AuthProvider::query()
                ->whereHas('authTenant', function(Builder $query) use ($auth_tenant_slug) {
                    $query->where('slug', $auth_tenant_slug);
                })
                ->where('slug', $auth_provider_slug)
                ->first();
        }

        // If record not found, return an error message
        if($auth_provider == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Auth Provider Values
            $this->comment('');
            $this->comment('Auth Provider');
            $this->table(
                ['Column', 'Existing Value'],
                [
                    [
                        'id',
                        $auth_provider->id
                    ],
                    [
                        'short_id',
                        $auth_provider->short_id
                    ],
                    [
                        'auth_tenant_id',
                        $auth_provider->auth_tenant_id
                    ],
                    [
                        'slug',
                        $auth_provider->slug
                    ],
                    [
                        'base_url',
                        $auth_provider->base_url
                    ],
                    [
                        'client_id',
                        $auth_provider->client_id ? decrypt($auth_provider->client_id) : null
                    ],
                    [
                        'client_secret',
                        $auth_provider->client_secret ? decrypt($auth_provider->client_secret) : null
                    ],
                    [
                        'flag_enabled',
                        $auth_provider->flag_enabled
                    ],
                    [
                        'created_at',
                        $auth_provider->created_at
                    ],
                    [
                        'updated_at',
                        $auth_provider->updated_at
                    ],
                ]
            );

        }

        // Initialize service
        $authProviderService = new Services\V1\Auth\AuthProviderService();

        // Base URL
        $base_url = $this->option('base_url');
        if($base_url == null) {
            if($this->confirm('Do you want to change the Base URL?')) {
                $base_url = $this->ask('What is the new base URL?');
            } else {
                $base_url = $auth_provider->base_url;
            }
        }

        // Client ID
        $client_id = $this->option('client_id');
        if($client_id == null) {
            if($this->confirm('Do you want to change the Client ID?')) {
                $client_id = $this->ask('What is the new client ID?');
            } else {
                $client_id = $auth_provider->client_id;
            }
        }

        // Client Secret
        $client_secret = $this->option('client_secret');
        if($client_secret == null) {
            if($this->confirm('Do you want to change the client secret?')) {
                $client_secret = $this->ask('What is the new client secret?');
            } else {
                $client_secret = $auth_provider->client_secret;
            }
        }

        //
        // Flag Enabled
        //

        $flag_enabled = $this->option('flag_enabled');

        // If flag is not set in CLI option, prompt to change state
        if($flag_enabled == null && $auth_provider->flag_enabled == 1) {
            if($this->confirm('Do you want to DISABLE the provider (usually no)?')) {
                $flag_enabled = 0;
            } else {
                $flag_enabled = 1;
            }
        }

        // If flag is not set in CLI option, prompt to change state
        if($flag_enabled == null && $auth_provider->flag_enabled == 0) {
            if($this->confirm('Do you want to ENABLE the provider (usually yes)?')) {
                $flag_enabled = 1;
            } else {
                $flag_enabled = 0;
            }
        }

        // Update database record using service method
        $authProviderService->update($auth_provider->id, [
            'base_url' => $base_url,
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'flag_enabled' => $flag_enabled,
        ]);

        $this->comment('');

        // Show result in console
        $this->comment('Record successfully updated.');
        $this->comment('');

        // Call the get method to display the tables of values for the record.
        $this->call('auth-provider:get', [
            'short_id' => $auth_provider->short_id
        ]);

    }

}
