<?php

namespace App\Console\Commands\Environment;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class EnvironmentInstanceUserGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'environment-instance-user:get
                            {short_id? : The short ID of the environment instance user.}
                            {--P|with-password : The password will be revealed in the output}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Environment Instance User by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->comment('You can lookup by short ID using `environment-instance-user:get a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $environment_instance_user = Models\Environment\EnvironmentInstanceUser::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($environment_instance_user == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Environment Instance User Values
        $this->comment('');
        $this->comment('Environment Instance User');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $environment_instance_user->id
                ],
                [
                    'short_id',
                    $environment_instance_user->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$environment_instance_user->authTenant->short_id.'] '.$environment_instance_user->authTenant->slug
                ],
                [
                    '[relationship] authUser',
                    '['.$environment_instance_user->authUser->short_id.'] '.$environment_instance_user->authUser->full_name.' <'.$environment_instance_user->authUser->email.'>'
                ],
                [
                    '[relationship] environmentInstance',
                    '['.$environment_instance_user->environmentInstance->short_id.'] '.$environment_instance_user->environmentInstance->slug
                ],
                [
                    'instance_username',
                    $environment_instance_user->instance_username ? decrypt($environment_instance_user->instance_username) : null
                ],
                [
                    'instance_password',
                    $this->option('with-password') ? decrypt($environment_instance_user->instance_password) : '[MASKED] Use flag `--with-password` to reveal'
                ],
                [
                    'created_at',
                    $environment_instance_user->created_at->toIso8601String()
                ],
                [
                    'created_by',
                    $environment_instance_user->created_by ? '['.$environment_instance_user->createdBy->short_id.'] '.$environment_instance_user->createdBy->full_name : 'System',
                ],
                [
                    'provisioned_at',
                    $environment_instance_user->provisioned_at ? $environment_instance_user->provisioned_at->toIso8601String() : 'never'
                ],
                [
                    'expires_at',
                    $environment_instance_user->expires_at ? $environment_instance_user->expires_at->toIso8601String() : 'never'
                ],
                [
                    'state',
                    $environment_instance_user->state
                ],
            ]
        );

        if($environment_instance_user->api_meta_data != null) {
            $api_meta_data_values = [];
            foreach($environment_instance_user->api_meta_data as $meta_data_key => $meta_data_value) {
                $api_meta_data_values[] = [
                    $meta_data_key,
                    is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value
                ];
            }

            $this->table(
                ['API Meta Data Column', 'API Value'],
                $api_meta_data_values
            );
        }

        $this->comment('');

    }
}
