<?php

namespace App\Console\Commands\Environment;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;

class EnvironmentInstanceEdit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'environment-instance:edit
                            {short_id? : The short ID of the provider.}
                            {--T|auth_tenant_slug= : The slug of the tenant this instance belongs to}
                            {--N|name= : The display name of this instance}
                            {--S|slug= : The alpha-dash shorthand name for this instance}
                            {--description= : A short description of this instance that appears to the user}
                            {--type= : The type of environment (Ex. gitlab)}
                            {--user_base_url= : The URL that should be exposed to users to authenticate.}
                            {--api_base_url= : The URL that should be used for API calls.}
                            {--api_username= : The username of the API credentials used for provisioning user accounts (may be blank for token-only authentication).}
                            {--api_token= : The password or token of the API credentials used for provisioning user accounts.}
                            {--instance_meta_data= : Optional array of additional information about this instance.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Edit an environment instance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Environment Instance - Edit Record');

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');
        $environment_instance_slug = $this->option('slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short_id or slug to lookup the record.');
            $this->comment('You can use the `environment-instance:edit a1b2c3d4` command to lookup a instance by ID.');
            $this->comment('You can use the `environment-instance:edit --auth_tenant_slug={tenant} --slug={instance}` command to lookup a instance with a slug.');
            $this->comment('You can use the `environment-instance:list` command to get a list of records.');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $environment_instance = Models\Environment\EnvironmentInstance::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($slug != null) {
            $environment_instance = Models\Environment\EnvironmentInstance::query()
                ->whereHas('authTenant', function(Builder $query) use ($auth_tenant_slug) {
                    $query->where('slug', $auth_tenant_slug);
                })
                ->where('slug', $slug)
                ->first();
        }

        // If record not found, return an error message
        if($environment_instance == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Environment Instance Values
            $this->comment('');
            $this->comment('Environment Instance');
            $this->table(
                ['Column', 'Existing Value'],
                [
                    [
                        'id',
                        $environment_instance->id
                    ],
                    [
                        'short_id',
                        $environment_instance->short_id
                    ],
                    [
                        'auth_tenant_id',
                        $environment_instance->auth_tenant_id
                    ],
                    [
                        'name',
                        $environment_instance->name
                    ],
                    [
                        'slug',
                        $environment_instance->slug
                    ],
                    [
                        'user_base_url',
                        $environment_instance->user_base_url
                    ],
                    [
                        'api_base_url',
                        $environment_instance->api_base_url
                    ],
                    [
                        'api_username',
                        $environment_instance->api_username ? decrypt($environment_instance->api_username) : null
                    ],
                    [
                        'api_token',
                        $environment_instance->api_token ? decrypt($environment_instance->api_token) : null
                    ],
                    [
                        'created_at',
                        $environment_instance->created_at->toIso8601String()
                    ],
                    [
                        'updated_at',
                        $environment_instance->updated_at->toIso8601String()
                    ],
                ]
            );

        }

        // Initialize service
        $environmentInstanceService = new Services\V1\Environment\EnvironmentInstanceService();

        // Name
        $name = $this->option('name');
        if($name == null) {
            if($this->confirm('Do you want to change the display name?')) {
                $name = $this->ask('What is the new display name?');
            } else {
                $name = $environment_instance->name;
            }
        }

        // Slug
        $slug = $this->option('slug');
        if($slug == null) {
            if($this->confirm('Do you want to change the slug?')) {
                $slug = $this->ask('What is the new slug?');
            } else {
                $slug = $environment_instance->slug;
            }
        }

        // Description
        $description = $this->option('description');
        if($description == null) {
            if($this->confirm('Do you want to change the description?')) {
                $description = $this->ask('What is the new description?');
            } else {
                $description = $environment_instance->description;
            }
        }

        // User Base URL
        $user_base_url = $this->option('user_base_url');
        if($user_base_url == null) {
            if($this->confirm('Do you want to change the User Base URL?')) {
                $user_base_url = $this->ask('What is the new User Base URL?');
            } else {
                $user_base_url = $environment_instance->base_url;
            }
        }

        // API Base URL
        $api_base_url = $this->option('api_base_url');
        if($api_base_url == null) {
            if($this->confirm('Do you want to change the API Base URL?')) {
                $api_base_url = $this->ask('What is the new API base URL?');
            } else {
                $api_base_url = $environment_instance->api_base_url;
            }
        }

        // API Username
        $api_username = $this->option('api_username');
        if($api_username == null) {
            if($this->confirm('Do you want to change the API Username?')) {
                $api_username = $this->ask('What is the new API Username?');
            } else {
                if($environment_instance->api_username != null) {
                    $api_username = decrypt($environment_instance->api_username);
                } else {
                    $api_username = null;
                }
            }
        }

        // API Token
        $api_token = $this->option('api_token');
        if($api_token == null) {
            if($this->confirm('Do you want to change the API Password or Token?')) {
                $api_token = $this->ask('What is the new API Password or Token?');
            } else {
                if($environment_instance->api_token != null) {
                    $api_token = decrypt($environment_instance->api_token);
                } else {
                    $api_token = null;
                }
            }
        }

        // Update database record using service method
        $environmentInstanceService->update($environment_instance->id, [
            'name' => $name,
            'slug' => $slug,
            'description' => $description,
            'user_base_url' => $user_base_url,
            'api_base_url' => $api_base_url,
            'api_username' => $api_username,
            'api_token' => $api_token
        ]);

        $this->comment('');

        // Show result in console
        $this->comment('Record successfully updated.');
        $this->comment('');

        // Call the get method to display the tables of values for the record.
        $this->call('environment-instance:get', [
            'short_id' => $environment_instance->short_id
        ]);

    }

}
