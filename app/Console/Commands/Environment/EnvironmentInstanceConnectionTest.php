<?php

namespace App\Console\Commands\Environment;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class EnvironmentInstanceConnectionTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'environment-instance:connection-test
                            {short_id? : The short ID of the instance.}
                            {--slug= : The slug of the instance.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test the API connection of an Environment Instance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $slug = $this->option('slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short id or slug to lookup the record.');
            $this->line('You can get a list of environment instances using `environment-instance:list`');
            $this->line('You can test the connection using the short ID using `environment-instance:connection-test a1b2c3d4`');
            $this->line('You can test the connection using the slug using `environment-instance:connection-test --slug=example`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $environment_instance = Models\Environment\EnvironmentInstance::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($slug != null) {
            $environment_instance = Models\Environment\EnvironmentInstance::query()
                ->where('slug', $slug)
                ->first();
        }

        // If record not found, return an error message
        if($environment_instance == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Environment Instance Values
        $this->comment('');
        $this->comment('Environment Instance');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $environment_instance->id
                ],
                [
                    'short_id',
                    $environment_instance->short_id
                ],
                [
                    'auth_tenant_id',
                    $environment_instance->auth_tenant_id
                ],
                [
                    'name',
                    $environment_instance->name
                ],
                [
                    'slug',
                    $environment_instance->slug
                ],
                [
                    'user_base_url',
                    $environment_instance->user_base_url
                ],
                [
                    'api_base_url',
                    $environment_instance->api_base_url
                ],
                [
                    'api_username',
                    $environment_instance->api_username ? decrypt($environment_instance->api_username).' (decrypted)' : null
                ],
                [
                    'api_token',
                    $environment_instance->api_token ? decrypt($environment_instance->api_token).' (decrypted)' : null
                ],
                [
                    'created_at',
                    $environment_instance->created_at->toIso8601String()
                ],
                [
                    'updated_at',
                    $environment_instance->updated_at->toIso8601String()
                ],
            ]
        );

        // GitLab Type
        if($environment_instance->type == 'gitlab') {

            try {
                $gitlab_client = new \Gitlab\Client();
                $gitlab_client->authenticate(decrypt($environment_instance->api_token), \Gitlab\Client::AUTH_HTTP_TOKEN);
                $gitlab_client->setUrl($environment_instance->api_base_url);

                $gitlab_version = $gitlab_client->version()->show();

                $this->line('');
                $this->line('GitLab Version: '.$gitlab_version['version']);
                $this->line('<fg=white;bg=green>Connection Test Successful!</>');

            } catch(\Gitlab\Exception\ErrorException $e) {

                $this->line('');
                $this->line('<fg=red>Connection Test Failed!</>');
                $this->handleException($e, get_class(), $request_data['path']);
                $this->line('');
            }

        }

        $this->comment('');

    }

}
