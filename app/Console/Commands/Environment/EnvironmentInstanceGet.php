<?php

namespace App\Console\Commands\Environment;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class EnvironmentInstanceGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'environment-instance:get
                            {short_id? : The short ID of the instance.}
                            {--slug= : The slug of the instance.}
                            {--R|without-child-relationships}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Environment Instance by ID or slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $slug = $this->option('slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short id or slug to lookup the record.');
            $this->line('You can get a list of environment instances using `environment-instance:list`');
            $this->line('You can lookup by short ID using `environment-instance:get a1b2c3d4`');
            $this->line('You can lookup by slug using `environment-instance:get --slug=example`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $environment_instance = Models\Environment\EnvironmentInstance::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($slug != null) {
            $environment_instance = Models\Environment\EnvironmentInstance::query()
                ->where('slug', $slug)
                ->first();
        }

        // If record not found, return an error message
        if($environment_instance == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Environment Instance Values
        $this->comment('');
        $this->comment('Environment Instance');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $environment_instance->id
                ],
                [
                    'short_id',
                    $environment_instance->short_id
                ],
                [
                    'auth_tenant_id',
                    $environment_instance->auth_tenant_id
                ],
                [
                    'name',
                    $environment_instance->name
                ],
                [
                    'slug',
                    $environment_instance->slug
                ],
                [
                    'user_base_url',
                    $environment_instance->user_base_url
                ],
                [
                    'api_base_url',
                    $environment_instance->api_base_url
                ],
                [
                    'api_username',
                    $environment_instance->api_username ? decrypt($environment_instance->api_username).' (decrypted)' : null
                ],
                [
                    'api_token',
                    $environment_instance->api_token ? decrypt($environment_instance->api_token).' (decrypted)' : null
                ],
                [
                    'created_at',
                    $environment_instance->created_at->toIso8601String()
                ],
                [
                    'updated_at',
                    $environment_instance->updated_at->toIso8601String()
                ],
            ]
        );

        // Check if option passed to not show child relationships
        if($this->option('without-child-relationships') == false) {

            //
            // Environment Instance - Child Relationship - Roles
            // --------------------------------------------------------------------
            // Loop through roles in Eloquent model and add calculated values to
            // array. The dot notation for pivot relationships doesn't work with a
            // get([]) so this is the best way to handle this.
            //

            // Loop through roles and add values to array
            $role_rows = [];
            foreach($environment_instance->authRoles as $role) {
                $role_rows[] = [
                    'environment_instance_role_short_id' => $role->environmentInstanceRole->short_id,
                    'role_short_id' => $role->short_id,
                    'role_slug' => $role->slug,
                    'expires_at' => $role->environmentInstanceRole->expires_at ? $role->environmentInstanceRole->expires_at->toIso8601String() : 'never'
                ];
            }

            $this->comment('');
            $this->comment('Environment Instance Roles');

            // If rows exists, render a table or return a no results found message
            if(count($role_rows) > 0) {
                $this->table(
                    ['Instance Role Short ID', 'Role Short ID', 'Role Slug', 'Expires at'],
                    $role_rows
                );
            } else {
                $this->error('No roles have been attached.');
            }

            //
            // Environment Instance - Child Relationship - Users
            // --------------------------------------------------------------------
            // Loop through roles in Eloquent model and add calculated values to
            // array. The dot notation for pivot relationships doesn't work with a
            // get([]) so this is the best way to handle this.
            //

            // Loop through users and add values to array
            $user_rows = [];
            foreach($environment_instance->authUsers as $user) {
                $user_rows[] = [
                    'environment_instance_user_short_id' => $user->environmentInstanceUser->short_id,
                    'user_short_id' => $user->short_id,
                    'name' => $user->full_name,
                    'email' => $user->email,
                    'instance_username' => $user->environmentInstanceUser->instance_username ? decrypt($user->environmentInstanceUser->instance_username) : null,
                    'expires_at' => $user->environmentInstanceUser->expires_at ? $user->environmentInstanceUser->expires_at->toIso8601String() : 'never'
                ];
            }

            $this->comment('');
            $this->comment('Environment Instance Users');

            // If rows exists, render a table or return a no results found message
            if(count($user_rows) > 0) {
                $this->table(
                    ['Instance User Short ID', 'User Short ID', 'Name', 'Email', 'Username', 'Expires at'],
                    $user_rows
                );
            } else {
                $this->error('No users have been attached.');
            }

        }

        $this->comment('');

    }
}
