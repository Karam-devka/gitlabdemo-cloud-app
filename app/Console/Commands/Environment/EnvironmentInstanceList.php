<?php

namespace App\Console\Commands\Environment;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class EnvironmentInstanceList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'environment-instance:list
                            {--T|auth_tenant_slug= : The slug of the tenant that environments belong to}
                            {--type=* : The type of instances to list.}
                            {--search=* : Wildcard search of name, slug, or description.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Environment Instances';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get type(s)
        $types_array = $this->option('type');

        // Get search keywords
        $search_array = $this->option('search');

        // Get list of records from database
        $records = Models\Environment\EnvironmentInstance::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($types_array) {
                // If types were specified, the array will not be empty
                if(count($types_array) > 0) {
                    foreach($types_array as $type_option) {
                        if ($type_option === array_key_first($types_array)) {
                            $query->where('type', $type_option);
                        } else {
                            $query->orWhere('type', $type_option);
                        }
                    }
                }
            })
            ->where(function($query) use ($search_array) {
                // If search keywords were specified, the array will not be empty
                if(count($search_array) > 0) {
                    foreach($search_array as $search_option) {
                        $query->where('name', 'LIKE', '%$search_option%');
                        $query->orWhere('slug', 'LIKE', '%$search_option%');
                        $query->orWhere('description', 'LIKE', '%$search_option%');
                    }
                }
            })
            ->orderBy('created_at')
            ->get();

        // Loop through instances in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $instance_record) {
            $instance_rows[] = [
                'short_id' => $instance_record->short_id,
                'auth_tenant' => '['.$instance_record->authTenant->short_id.'] '.$instance_record->authTenant->slug,
                'roles' => $instance_record->authRoles()->count(),
                'users' => $instance_record->authUsers()->count(),
                'type' => $instance_record->type,
                'name' => $instance_record->name,
                'slug' => $instance_record->slug,
                'user_base_url' => $instance_record->user_base_url,
                'created_at' => $instance_record->created_at->toIso8601String(),
            ];
        }

        $this->comment('');
        $this->comment('Environment Instances - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Roles', 'Users', 'Namespace', 'Name', 'Slug', 'URL', 'Created at'];
            $this->table($headers, $instance_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
