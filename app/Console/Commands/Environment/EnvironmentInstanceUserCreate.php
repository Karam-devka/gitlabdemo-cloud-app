<?php

namespace App\Console\Commands\Environment;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;

class EnvironmentInstanceUserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'environment-instance-user:create
                            {--T|auth_tenant_slug= : The slug of the tenant that the group exists in}
                            {--U|auth_user_short_id= : The short ID of the Authentication User}
                            {--C|environment_instance_short_id= : The short ID of the Environment Instance}
                            {--E|expires_at= : The date (YYYY-MM-DD) that this user expires}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attach a User to an Environment Instance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Environment Instance Users - Create Record');

        //
        // Authentication Tenant
        // --------------------------------------------------------------------
        // Lookup the authentication tenant or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth tenant from console option
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant does the user exist in?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Environment Instance
        // --------------------------------------------------------------------
        // Lookup the environment instance or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get environment instance from console option
        $environment_instance_short_id = $this->option('environment_instance_short_id');

        // If environment instance ID option was specified, lookup by ID
        if($environment_instance_short_id) {
            $environment_instance = Models\Environment\EnvironmentInstance::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('short_id', $environment_instance_short_id)
                ->first();
        }

        // If environment instance ID was not provided, prompt for input
        else {

            // Get list of instances to show in console
            $environment_instances = Models\Environment\EnvironmentInstance::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->table(
                ['Environment Instances'],
                $environment_instances
            );

            $environment_instance_prompt = $this->anticipate('Which environment instance should the user be attached to?', Arr::flatten($environment_instances));

            // Lookup group based on slug provided in prompt
            $environment_instance = Models\Environment\EnvironmentInstance::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $environment_instance_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$environment_instance) {
            $this->error('Error: No environment instance was found.');
            $this->error('');
            die();
        }

        //
        // Authentication User
        // --------------------------------------------------------------------
        // Lookup the authentication users or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth user from console option
        $auth_user_short_id = $this->option('auth_user_short_id');

        // If auth user short ID option was specified, lookup by ID
        if($auth_user_short_id) {
            $auth_user = Models\Auth\AuthUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('short_id', $auth_user_short_id)
                ->first();
        }

        // If auth user short ID was not provided, prompt for input
        else {

            // Get list of users to show in console
            $environment_instance_id = $environment_instance->id;
            $auth_users = Models\Auth\AuthUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->whereDoesntHave('environmentInstanceUsers', function (Builder $query) use ($environment_instance_id) {
                    $query->where('environment_instance_id', $environment_instance_id);
                })->get(['email'])
                ->toArray();

            $this->line('You can view the database table or use this command in a separate terminal window to get a list of users:');
            $this->line('auth-user:list -T '.$auth_tenant->slug);
            $this->line('');

            $auth_user_prompt = $this->anticipate('What is the email address of the user that you want to attach to the environment instance?', Arr::flatten($auth_users));

            // Lookup group based on slug provided in prompt
            $auth_user = Models\Auth\AuthUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('email', $auth_user_prompt)
                ->first();

            // Verify that user does not belong to Cloud Account
            $environment_instance_id = $environment_instance->id;
            $auth_user_existing_environment_instance = Models\Auth\AuthUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('email', $auth_user_prompt)
                ->whereHas('environmentInstanceUsers', function (Builder $query) use ($environment_instance_id) {
                    $query->where('environment_instance_id', $environment_instance_id);
                })->first();

            // Validate that user doesn't already have an existing relationship
            // with this instance, or return error message
            if($auth_user_existing_environment_instance) {
                $this->error('Error: This user is already associated with this environment instance.');
                $this->error('');
                die();
            }

        }

        // Validate that user exists or return error message
        if(!$auth_user) {
            $this->error('Error: No user was found with that short ID.');
            $this->error('');
            die();
        }

        //
        // Additional String Parameters and Options
        // --------------------------------------------------------------------
        //

        // Expires at (not prompted, only as CLI option)
        $expires_at = $this->option('expires_at');
        if($expires_at != null) {
            $expires_at = \Carbon\Carbon::parse($expires_at)->format('Y-m-d');
        } else {
            $expires_at = null;
        }

        //
        // Verify inputs table
        // --------------------------------------------------------------------
        // Show the user the database values that will be added before saving
        // changes.
        //

        $this->table(
            ['Column', 'Value'],
            [
                [
                    'auth_tenant_id',
                    '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                ],
                [
                    'environment_instance_id',
                    '['.$environment_instance->short_id.'] '.$environment_instance->slug
                ],
                [
                    'auth_user_id',
                    '['.$auth_user->short_id.'] '.$auth_user->email
                ],
                [
                    'expires_at',
                    $expires_at ? $expires_at : 'never'
                ],
            ]
        );

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the creation of the record?')) {
            $this->error('Error: You aborted. No record was created.');
            die();
        }

        // Initialize service
        $environmentInstanceUserService = new Services\V1\Environment\EnvironmentInstanceUserService();

        // Use service to create record
        $record = $environmentInstanceUserService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'environment_instance_id' => $environment_instance->id,
            'auth_user_id' => $auth_user->id,
            'expires_at' => $expires_at,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('environment-instance-user:get', [
            'short_id' => $record->short_id,
            '--with-password' => true
        ]);

    }

}
