<?php

namespace App\Console\Commands\Environment;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class EnvironmentInstanceCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'environment-instance:create
                            {--T|auth_tenant_slug= : The slug of the tenant this instance belongs to}
                            {--N|name= : The display name of this instance}
                            {--S|slug= : The alpha-dash shorthand name for this instance}
                            {--description= : A short description of this instance that appears to the user}
                            {--type= : The type of environment (Ex. gitlab)}
                            {--user_base_url= : The URL that should be exposed to users to authenticate.}
                            {--api_base_url= : The URL that should be used for API calls.}
                            {--api_username= : The username of the API credentials used for provisioning user accounts (may be blank for token-only authentication).}
                            {--api_token= : The password or token of the API credentials used for provisioning user accounts.}
                            {--instance_meta_data= : Optional array of additional information about this instance.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an Environment Instance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Environment Instances - Create Record');

        // Get auth tenant from console option
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should this environment belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        // Name
        $name = $this->option('name');
        if($name == null) {
            $name = $this->ask('What is the display name of this instance?');
        }

        // Slug
        $slug = $this->option('slug');
        if($slug == null) {
            $slug = $this->ask('What is the alpha-dash shorthand name for this instance?');
        }

        // Description
        $description = $this->option('description');
        if($description == null) {
            $description = $this->ask('What is a short description of this instance that appears to the user?');
        }

        // Type
        $type = $this->option('type');

        // If type was was not specified, prompt user for input
        if(!$type) {

            // Get list of tenants to show in console
            $enironment_types = [
                'gitlab'
            ];

            $this->line('');
            $this->line('Available types: '.implode(',', Arr::flatten($enironment_types)));

            $type = $this->anticipate('Which type of instance will be provisioned?', Arr::flatten($enironment_types));

        }

        // User Base URL
        $user_base_url = $this->option('user_base_url');
        if($user_base_url == null) {
            $user_base_url = $this->ask('What is the URL that should be exposed to users to authenticate?');
        }

        // User Base URL
        $api_base_url = $this->option('api_base_url');
        if($api_base_url == null) {
            $api_base_url = $this->ask('What is the URL that should be used for API calls?');
        }

        // API Username
        $api_username = $this->option('api_username');
        if($api_username == null) {
            $api_username = $this->ask('What is the username of the API credentials used for provisioning user accounts (may be blank for token-only authentication)?');
        }

        // API Token
        $api_token = $this->option('api_token');
        if($api_token == null) {
            $api_token = $this->ask('What is the password or token of the API credentials used for provisioning user accounts?');
        }

        // Verify inputs table
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'auth_tenant_id',
                    '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                ],
                [
                    'name',
                    $name
                ],
                [
                    'slug',
                    $slug
                ],
                [
                    'description',
                    $description
                ],
                [
                    'type',
                    $type
                ],
                [
                    'user_base_url',
                    $user_base_url
                ],
                [
                    'api_base_url',
                    $api_base_url
                ],
                [
                    'api_username',
                    $api_username
                ],
                [
                    'api_token',
                    $api_token
                ],
            ]
        );

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the creation of the record?')) {
            $this->error('Error: You aborted. No record was created.');
            die();
        }

        // Initialize service
        $environmentInstanceService = new Services\V1\Environment\EnvironmentInstanceService();

        // Use service to create record
        $record = $environmentInstanceService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'name' => $name,
            'slug' => $slug,
            'description' => $description,
            'type' => $type,
            'user_base_url' => $user_base_url,
            'api_base_url' => $api_base_url,
            'api_username' => $api_username,
            'api_token' => $api_token,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('environment-instance:get', [
            'short_id' => $record->short_id
        ]);

    }

}
