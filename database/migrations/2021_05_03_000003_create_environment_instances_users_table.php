<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnvironmentInstancesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('environment_instances_users', function (Blueprint $table) {
            $table->uuid('id')->nullable()->index();
            $table->string('short_id', 8)->nullable()->index();
            $table->uuid('auth_tenant_id')->index();
            $table->uuid('auth_user_id')->index();
            $table->uuid('environment_instance_id')->index();
            $table->json('api_meta_data')->nullable();
            $table->text('instance_username')->nullable()->comment('encrypted');
            $table->text('instance_password')->nullable()->comment('encrypted');
            $table->boolean('flag_provisioned')->default(false)->nullable();
            $table->timestamp('provisioned_at')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('environment_instances_users');
    }
}
