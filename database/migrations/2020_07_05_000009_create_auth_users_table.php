<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_users', function (Blueprint $table) {
            $table->uuid('id')->nullable()->index();
            $table->string('short_id', 8)->nullable()->index();
            $table->uuid('auth_tenant_id')->nullable()->index();
            $table->uuid('auth_provider_id')->nullable()->index();
            $table->string('full_name')->nullable();
            $table->string('job_title')->nullable();
            $table->string('organization_name')->nullable();
            $table->string('email')->index();
            $table->string('email_recovery')->nullable();
            $table->text('password')->nullable()->comment('hashed');
            $table->rememberToken();
            $table->json('provider_meta_data')->nullable();
            $table->text('provider_token')->nullable()->comment('encrypted');
            $table->text('two_factor_secret')->nullable()->comment('encrypted');
            $table->text('two_factor_recovery_codes')->nullable()->comment('encrypted');
            $table->unsignedInteger('phone_number')->nullable();
            $table->unsignedInteger('phone_country_code')->nullable();
            $table->unsignedInteger('count_current_failed_logins')->nullable();
            $table->string('timezone')->default('UTC')->nullable();
            $table->boolean('flag_passwordless')->default(false)->nullable();
            $table->boolean('flag_2fa_enabled')->default(false)->nullable();
            $table->boolean('flag_must_change_password')->default(false)->nullable();
            $table->boolean('flag_account_expired')->default(false)->nullable();
            $table->boolean('flag_account_locked')->default(false)->nullable();
            $table->boolean('flag_account_verified')->default(false)->nullable();
            $table->boolean('flag_terms_accepted')->default(false)->nullable();
            $table->boolean('flag_privacy_accepted')->default(false)->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamp('locked_at')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->timestamp('terms_accepted_at')->nullable();
            $table->timestamp('privacy_accepted_at')->nullable();
            $table->timestamp('password_changed_at')->nullable();
            $table->timestamp('last_successful_login_at')->nullable();
            $table->timestamp('last_failed_login_at')->nullable();
            $table->timestamp('last_activity_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state')->nullable()->index();
            $table->unique(['auth_provider_id', 'email'], 'auth_users_provider_email_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_users');
    }
}
