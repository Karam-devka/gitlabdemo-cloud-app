<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_tenants', function (Blueprint $table) {
            $table->uuid('id')->nullable()->index();
            $table->string('short_id', 8)->nullable()->index();
            $table->string('name');
            $table->string('slug')->nullable()->unique()->index();
            $table->text('description')->nullable();
            $table->string('git_provider_type')->nullable();
            $table->string('git_provider_base_url')->nullable();
            $table->string('git_provider_base_group_id', 55)->nullable();
            $table->text('git_provider_api_token')->nullable()->comment('encrypted');
            $table->json('git_meta_data')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_tenants');
    }
}
