<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthProvidersGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_providers_groups', function (Blueprint $table) {
            $table->uuid('id')->nullable()->index();
            $table->string('short_id', 8)->nullable()->index();
            $table->uuid('auth_tenant_id')->index();
            $table->uuid('auth_provider_id')->index();
            $table->uuid('auth_group_id')->index();
            $table->enum('type', ['default','meta']);
            $table->string('meta_key', '55')->nullable();
            $table->string('meta_value', '255')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_providers_groups');
    }
}
