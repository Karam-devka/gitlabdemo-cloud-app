<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models;
use App\Services;

class AuthBaseSeed extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // Initialize service classes for performing store and update methods
        $authTenantService = new Services\V1\Auth\AuthTenantService();
        $authRoleService = new Services\V1\Auth\AuthRoleService();
        $authGroupService = new Services\V1\Auth\AuthGroupService();
        $authGroupRoleService = new Services\V1\Auth\AuthGroupRoleService();
        $authGroupUserService = new Services\V1\Auth\AuthGroupUserService();
        $authProviderService = new Services\V1\Auth\AuthProviderService();
        $authProviderGroupService = new Services\V1\Auth\AuthProviderGroupService();
        $authUserService = new Services\V1\Auth\AuthUserService();

        // Auth Tenant
        // --------------------------------------------------------------------
        // When HackyStack is installed, we create a default tenant that can
        // be renamed later. Without an initial tenant, we cannot create the
        // other auth resources.
        //

        // Check if tenant already exists
        $default_tenant = Models\Auth\AuthTenant::where('slug', 'default')->first();

        // If tenant does not exist, create new tenant
        if(!$default_tenant) {

            $default_tenant = $authTenantService->store([
                'name' => 'Default Tenant',
                'slug' => 'default',
                'description' => 'This is the default tenant that is created when HackyStack was installed. You can rename this to your organization name.'
            ]);

        }

        // Auth Roles
        // --------------------------------------------------------------------
        // When HackyStack is installed, we create a few default roles to offer
        // least priviledge permissions for users and allows roles to be added
        // to groups with role(s) that have elevated permissions.

        // Define array of default roles
        $default_roles = [
            'auth-super-admin' => [
                'name' => 'Authentication - Super Admin',
                'slug' => 'auth-super-admin',
                'description' => 'This role provides users with unrestricted access to all aspects of authentication including groups, providers, roles, service accounts, tenants, and users. This role should be restricted to your most trusted system administrators.',
                'flag_is_elevated' => 1,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.groups.index',
                    'auth.groups.create',
                    'auth.groups.store',
                    'auth.groups.show',
                    'auth.groups.edit',
                    'auth.groups.update',
                    'auth.groups.delete',
                    'auth.groups.restore',
                    'auth.groups.destroy',
                    'auth.groups.roles.index',
                    'auth.groups.roles.create',
                    'auth.groups.roles.store',
                    'auth.groups.roles.show',
                    'auth.groups.roles.edit',
                    'auth.groups.roles.update',
                    'auth.groups.roles.delete',
                    'auth.groups.roles.restore',
                    'auth.groups.roles.destroy',
                    'auth.groups.users.index',
                    'auth.groups.users.create',
                    'auth.groups.users.store',
                    'auth.groups.users.show',
                    'auth.groups.users.edit',
                    'auth.groups.users.update',
                    'auth.groups.users.delete',
                    'auth.groups.users.restore',
                    'auth.groups.users.destroy',
                    'auth.providers.index',
                    'auth.providers.create',
                    'auth.providers.store',
                    'auth.providers.show',
                    'auth.providers.edit',
                    'auth.providers.update',
                    'auth.providers.delete',
                    'auth.providers.restore',
                    'auth.providers.destroy',
                    'auth.providers.groups.index',
                    'auth.providers.groups.create',
                    'auth.providers.groups.store',
                    'auth.providers.groups.show',
                    'auth.providers.groups.edit',
                    'auth.providers.groups.update',
                    'auth.providers.groups.delete',
                    'auth.providers.groups.restore',
                    'auth.providers.groups.destroy',
                    'auth.roles.index',
                    'auth.roles.create',
                    'auth.roles.store',
                    'auth.roles.show',
                    'auth.roles.edit',
                    'auth.roles.update',
                    'auth.roles.delete',
                    'auth.roles.restore',
                    'auth.roles.destroy',
                    'auth.roles.groups.index',
                    'auth.roles.groups.create',
                    'auth.roles.groups.store',
                    'auth.roles.groups.show',
                    'auth.roles.groups.edit',
                    'auth.roles.groups.update',
                    'auth.roles.groups.delete',
                    'auth.roles.groups.restore',
                    'auth.roles.groups.destroy',
                    'auth.roles.service.accounts.index',
                    'auth.roles.service.accounts.create',
                    'auth.roles.service.accounts.store',
                    'auth.roles.service.accounts.show',
                    'auth.roles.service.accounts.edit',
                    'auth.roles.service.accounts.update',
                    'auth.roles.service.accounts.delete',
                    'auth.roles.service.accounts.restore',
                    'auth.roles.service.accounts.destroy',
                    'auth.service.accounts.index',
                    'auth.service.accounts.create',
                    'auth.service.accounts.store',
                    'auth.service.accounts.show',
                    'auth.service.accounts.edit',
                    'auth.service.accounts.update',
                    'auth.service.accounts.delete',
                    'auth.service.accounts.restore',
                    'auth.service.accounts.destroy',
                    'auth.service.accounts.roles.index',
                    'auth.service.accounts.roles.create',
                    'auth.service.accounts.roles.store',
                    'auth.service.accounts.roles.show',
                    'auth.service.accounts.roles.edit',
                    'auth.service.accounts.roles.update',
                    'auth.service.accounts.roles.delete',
                    'auth.service.accounts.roles.restore',
                    'auth.service.accounts.roles.destroy',
                    'auth.tenants.index',
                    'auth.tenants.create',
                    'auth.tenants.store',
                    'auth.tenants.show',
                    'auth.tenants.edit',
                    'auth.tenants.update',
                    'auth.tenants.delete',
                    'auth.tenants.restore',
                    'auth.tenants.destroy',
                    'auth.users.index',
                    'auth.users.create',
                    'auth.users.store',
                    'auth.users.show',
                    'auth.users.edit',
                    'auth.users.update',
                    'auth.users.delete',
                    'auth.users.restore',
                    'auth.users.destroy',
                    'auth.users.groups.index',
                    'auth.users.groups.create',
                    'auth.users.groups.store',
                    'auth.users.groups.show',
                    'auth.users.groups.edit',
                    'auth.users.groups.update',
                    'auth.users.groups.delete',
                    'auth.users.groups.restore',
                    'auth.users.groups.destroy',
                ],
            ],
            'auth-groups-global-admin' => [
                'name' => 'Authentication Groups - Global Admin',
                'slug' => 'auth-groups-global-admin',
                'description' => 'This role allows users to administer any groups that are part of the same tenant their user account is associated with.',
                'flag_is_elevated' => 1,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.groups.index',
                    'auth.groups.create',
                    'auth.groups.store',
                    'auth.groups.show',
                    'auth.groups.edit',
                    'auth.groups.update',
                    'auth.groups.delete',
                    'auth.groups.restore',
                    'auth.groups.roles.index',
                    'auth.groups.roles.create',
                    'auth.groups.roles.store',
                    'auth.groups.roles.show',
                    'auth.groups.roles.edit',
                    'auth.groups.roles.update',
                    'auth.groups.roles.delete',
                    'auth.groups.roles.restore',
                    'auth.groups.users.index',
                    'auth.groups.users.create',
                    'auth.groups.users.store',
                    'auth.groups.users.show',
                    'auth.groups.users.edit',
                    'auth.groups.users.update',
                    'auth.groups.users.delete',
                    'auth.groups.users.restore',
                ],
            ],
            'auth-groups-global-manager' => [
                'name' => 'Authentication Groups - Global Manager',
                'slug' => 'auth-groups-global-manager',
                'description' => 'This role allows users to manage all groups associated with the tenant. The manager role does not allow the user to add or modify roles for the group that would modify the permissions for group users.',
                'flag_is_elevated' => 1,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.groups.index',
                    'auth.groups.create',
                    'auth.groups.store',
                    'auth.groups.show',
                    'auth.groups.edit',
                    'auth.groups.update',
                    'auth.groups.roles.index',
                    'auth.groups.roles.show',
                    'auth.groups.users.index',
                    'auth.groups.users.create',
                    'auth.groups.users.store',
                    'auth.groups.users.show',
                    'auth.groups.users.edit',
                    'auth.groups.users.update',
                    'auth.groups.users.delete',
                    'auth.groups.users.restore',
                ],
            ],
            'auth-groups-member-manager' => [
                'name' => 'Authentication Groups - Member Manager',
                'slug' => 'auth-groups-member-manager',
                'description' => 'This role allows users to manage any groups that they are a member of and have `flag_is_manager` enabled in the auth_users_groups table. The manager role does not allow deleting records or modifying roles for the group that would modify the permissions for group users.',
                'flag_is_elevated' => 1,
                'flag_is_group_member' => 1,
                'permissions' => [
                    'auth.groups.index',
                    'auth.groups.create',
                    'auth.groups.store',
                    'auth.groups.show',
                    'auth.groups.edit',
                    'auth.groups.update',
                    'auth.groups.roles.index',
                    'auth.groups.roles.show',
                    'auth.groups.users.index',
                    'auth.groups.users.create',
                    'auth.groups.users.store',
                    'auth.groups.users.show',
                    'auth.groups.users.edit',
                    'auth.groups.users.update',
                    'auth.groups.users.delete',
                    'auth.groups.users.restore',
                ],
            ],
            'auth-groups-global-viewer' => [
                'name' => 'Authentication Groups - Global Viewer',
                'slug' => 'auth-groups-global-viewer',
                'description' => 'This role allows users read-only access to all groups associated with the tenant including the associated roles and users.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.groups.index',
                    'auth.groups.show',
                    'auth.groups.roles.index',
                    'auth.groups.users.index',
                ],
            ],
            'auth-groups-member-viewer' => [
                'name' => 'Authentication Groups - Member Viewer',
                'slug' => 'auth-groups-member-viewer',
                'description' => 'This role allows users read-only access to all groups that the user is a member of including the associated roles and users.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 1,
                'permissions' => [
                    'auth.groups.index',
                    'auth.groups.show',
                    'auth.groups.roles.index',
                    'auth.groups.users.index',
                ],
            ],
            'auth-groups-global-list' => [
                'name' => 'Authentication Groups - Global List',
                'slug' => 'auth-groups-global-list',
                'description' => 'This role allows users read-only access to see a list of all groups that they are not a member of. This role cannot see the members or roles of each group.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.groups.index',
                ],
            ],
            'auth-groups-global-list-members' => [
                'name' => 'Authentication Groups - Global List with Members',
                'slug' => 'auth-groups-global-list-members',
                'description' => 'This role allows users read-only access to see a list of all groups that they are not a member of, and all members that are associated with each group. This will expose Personal Identifiable Information (PII) including the name, job title, and email address of group members.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.groups.index',
                    'auth.groups.users.index'
                ],
            ],
            'auth-groups-global-list-roles' => [
                'name' => 'Authentication Groups - Global List with Roles',
                'slug' => 'auth-groups-global-list-roles',
                'description' => 'This role allows users read-only access to see a list of all groups that they are not a member of, and all members that are associated with each group. This will not expose Personal Identifiable Information (PII).',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.groups.index',
                    'auth.groups.roles.index'
                ],
            ],
            'auth-groups-global-viewer' => [
                'name' => 'Authentication Groups - Global Viewers',
                'slug' => 'auth-groups-global-viewer',
                'description' => 'This role allows users read-only access to see a list of all groups that they are not a member of, the roles associated with each group, and all members that are associated with each group. This will expose Personal Identifiable Information (PII) including the name, job title, and email address of group members.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.groups.index',
                    'auth.groups.roles.index',
                    'auth.groups.users.index'
                ],
            ],
            'auth-roles-global-admin' => [
                'name' => 'Authentication Roles - Global Admin',
                'slug' => 'auth-roles-global-admin',
                'description' => 'This role allows users to administer any roles that are associated with the same tenant.',
                'flag_is_elevated' => 1,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.roles.index',
                    'auth.roles.create',
                    'auth.roles.store',
                    'auth.roles.show',
                    'auth.roles.edit',
                    'auth.roles.update',
                    'auth.roles.delete',
                    'auth.roles.restore',
                    'auth.roles.groups.index',
                    'auth.roles.groups.create',
                    'auth.roles.groups.store',
                    'auth.roles.groups.show',
                    'auth.roles.groups.edit',
                    'auth.roles.groups.update',
                    'auth.roles.groups.delete',
                    'auth.roles.groups.restore',
                    'auth.roles.service.accounts.index',
                    'auth.roles.service.accounts.create',
                    'auth.roles.service.accounts.store',
                    'auth.roles.service.accounts.show',
                    'auth.roles.service.accounts.edit',
                    'auth.roles.service.accounts.update',
                    'auth.roles.service.accounts.delete',
                    'auth.roles.service.accounts.restore',
                ],
            ],
            'auth-roles-global-viewer' => [
                'name' => 'Authentication Roles - Global Viewer',
                'slug' => 'auth-roles-global-viewer',
                'description' => 'This role allows users read-only access to all roles associated with the tenant including the associated groups and service accounts.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.roles.index',
                    'auth.roles.show',
                    'auth.roles.groups.index',
                    'auth.roles.groups.index',
                    'auth.roles.service.accounts.index',
                    'auth.roles.service.accounts.index',
                ],
            ],
            'auth-users-global-admin' => [
                'name' => 'Authentication Users - Global Admin',
                'slug' => 'auth-users-global-admin',
                'description' => 'This role allows users to administer any users that are associated with the same tenant.',
                'flag_is_elevated' => 1,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.users.index',
                    'auth.users.create',
                    'auth.users.store',
                    'auth.users.show',
                    'auth.users.edit',
                    'auth.users.update',
                    'auth.users.delete',
                    'auth.users.restore',
                    'auth.users.groups.index',
                    'auth.users.groups.create',
                    'auth.users.groups.store',
                    'auth.users.groups.show',
                    'auth.users.groups.edit',
                    'auth.users.groups.update',
                    'auth.users.groups.delete',
                    'auth.users.groups.restore',
                ],
            ],
            'auth-users-global-manager' => [
                'name' => 'Authentication Users - Global Manager',
                'slug' => 'auth-users-global-manager',
                'description' => 'This role allows users to manage all users associated with the tenant. The manager role allows the user to perform support actions such as password resets and account unlocks. New users created by a manager will inherit the default roles for the authentication provider. The manager role does not allow deleting records or modifying groups for the user that would modify the roles and permissions.',
                'flag_is_elevated' => 1,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.users.index',
                    'auth.users.create',
                    'auth.users.store',
                    'auth.users.show',
                    'auth.users.edit',
                    'auth.users.update',
                    'auth.users.groups.index',
                    'auth.users.groups.show',
                ],
            ],
            'auth-users-global-viewer' => [
                'name' => 'Authentication Users - Global Viewer',
                'slug' => 'auth-users-global-viewer',
                'description' => 'This role allows users read-only access to all users associated with the tenant including the associated groups.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.users.index',
                    'auth.users.show',
                    'auth.users.groups.index',
                ],
            ],
            'auth-service-accounts-global-admin' => [
                'name' => 'Authentication Service Accounts - Global Admin',
                'slug' => 'auth-service-accounts-global-admin',
                'description' => 'This role allows users to administer any service accounts that are associated with the same tenant.',
                'flag_is_elevated' => 1,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.service.accounts.index',
                    'auth.service.accounts.create',
                    'auth.service.accounts.store',
                    'auth.service.accounts.show',
                    'auth.service.accounts.edit',
                    'auth.service.accounts.update',
                    'auth.service.accounts.delete',
                    'auth.service.accounts.restore',
                    'auth.service.accounts.roles.index',
                    'auth.service.accounts.roles.create',
                    'auth.service.accounts.roles.store',
                    'auth.service.accounts.roles.show',
                    'auth.service.accounts.roles.edit',
                    'auth.service.accounts.roles.update',
                    'auth.service.accounts.roles.delete',
                    'auth.service.accounts.roles.restore',
                ],
            ],
            'auth-service-accounts-global-creator' => [
                'name' => 'Authentication Service Accounts - Global Creator',
                'slug' => 'auth-service-accounts-global-creator',
                'description' => 'This role allows users to create new service accounts and attach the appropriate roles. This role does not provide access to existing service accounts.',
                'flag_is_elevated' => 1,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.service.accounts.create',
                    'auth.service.accounts.store',
                    'auth.service.accounts.show',
                ],
            ],
            'auth-service-accounts-global-viewer' => [
                'name' => 'Authentication Service Accounts - Global Viewer',
                'slug' => 'auth-service-accounts-global-viewer',
                'description' => 'This role allows users read-only access to all service accounts associated with the tenant including the associated roles.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'auth.service.accounts.index',
                    'auth.service.accounts.show',
                    'auth.service.accounts.roles.index',
                    'auth.service.accounts.roles.show',
                ],
            ],
            'cloud-account-role' => [
                'name' => 'Cloud - Account Role',
                'slug' => 'cloud-account-role',
                'description' => 'This role provides users with access to the features for creating and managing and cloud accounts in organization units they have access to.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'user.cloud.index',
                    'user.cloud.accounts.create',
                    'user.cloud.accounts.store',
                    'user.cloud.accounts.edit',
                    'user.cloud.accounts.update',
                    'user.cloud.accounts.show',
                    'user.cloud.accounts.delete',
                ],
            ],
            'user-baseline-role' => [
                'name' => 'User - Baseline Role',
                'slug' => 'user-baseline-role',
                'description' => 'This role provides users with access to all standard features after signing in. Additional roles should be assigned for managing or viewing groups. This role should be assigned to all users by default.',
                'flag_is_elevated' => 0,
                'flag_is_group_member' => 0,
                'permissions' => [
                    'user.dashboard.index',
                    'user.cloud.index',
                    'user.environments.index',
                    'user.environments.instances.users.store',
                    'user.analytics.index',
                    'user.account.profile.index',
                ],
            ],
        ];

        // Loop through array of default roles
        foreach($default_roles as $role_slug => $role_values) {

            // Check if role already exists
            $role = Models\Auth\AuthRole::query()
                ->where('slug', $role_slug)
                ->first();

            // If role does not exist, create new role
            if(!$role) {

                $role = $authRoleService->store([
                    'auth_tenant_id' => $default_tenant->id,
                    'name' => $role_values['name'],
                    'slug' => $role_values['slug'],
                    'description' => $role_values['description'],
                    'flag_is_elevated' => $role_values['flag_is_elevated'],
                    'flag_is_group_member' => $role_values['flag_is_group_member'],
                    'permissions' => $role_values['permissions'],
                ]);

            }
        }

        // Auth Groups
        // --------------------------------------------------------------------
        // When HackyStack is installed, we create a few default groups to offer
        // best practice roles and permissions for users.
        //
        $default_groups = [
            'auth-super-admin' => [
                'name' => 'Authentication - Super Admins',
                'slug' => 'auth-super-admin',
                'description' => 'This group provides users with unrestricted access to all aspects of authentication including groups, providers, roles, service accounts, tenants, and users. This group should be restricted to your most trusted system administrators.',
                'roles' => [
                    'auth-super-admin',
                ]
            ],
            'auth-global-manager' => [
                'name' => 'Authentication - Global Managers',
                'slug' => 'auth-global-manager',
                'description' => 'This group allows users to manage groups and users and perform day-to-day operations such as account creation and password assistance. The manager role does not allow the user to add or modify roles for groups and users. This group is designed for IT support staff.',
                'roles' => [
                    'auth-groups-global-manager',
                    'auth-users-global-manager',
                ]
            ],
            'auth-global-viewer' => [
                'name' => 'Authentication - Global Viewers',
                'slug' => 'auth-global-viewer',
                'description' => 'This group allows users read-only access to all authentication records including groups, roles, service accounts, and users.',
                'roles' => [
                    'auth-groups-global-viewer',
                    'auth-roles-global-viewer',
                    'auth-service-accounts-global-viewer',
                    'auth-users-global-viewer',
                ]
            ],
            'auth-groups-member-manager' => [
                'name' => 'Authentication Groups - Member Managers',
                'slug' => 'auth-groups-member-manager',
                'description' => 'This group allows users that are designated as managers of groups that they are associated with (`flag_is_manager`) to manage the group and users and perform day-to-day operations such as adding or removing members from the group. The manager role does not allow the user to add or modify roles for groups and users. This group is designed for team leaders.',
                'roles' => [
                    'auth-groups-member-manager',
                ]
            ],
            'auth-groups-member-viewer' => [
                'name' => 'Authentication Groups - Member Viewers',
                'slug' => 'auth-groups-member-viewer',
                'description' => 'This group allows users read-only access to all groups that the user is a member of including the associated roles and users.',
                'roles' => [
                    'auth-groups-member-viewer',
                ]
            ],
            'auth-groups-global-list' => [
                'name' => 'Authentication Groups - Global List Viewers',
                'slug' => 'auth-groups-global-list',
                'description' => 'This group allows users read-only access to see a list of all groups that they are not a member of. The members of this group cannot see the members of each group unless they have the auth-groups-global-viewer role assigned to one of their groups. This group is designed for organizations that allow users to create a self-service request to join a group that should not see members or roles.',
                'roles' => [
                    'auth-groups-global-list',
                ]
            ],
            'auth-groups-global-list-members' => [
                'name' => 'Authentication Groups - Global List with Member Viewers',
                'slug' => 'auth-groups-global-list-members',
                'description' => 'This group allows users read-only access to see a list of all groups that they are not a member of, and all members that are associated with each group. This will expose Personal Identifiable Information (PII) including the name, job title, and email address of group members. This group is designed for organizations that allow users to create a self-service request to join a group that should see members but not roles associated.',
                'roles' => [
                    'auth-groups-global-list-members',
                ]
            ],
            'auth-groups-global-list-roles' => [
                'name' => 'Authentication Groups - Global List with Roles Viewers',
                'slug' => 'auth-groups-global-list-roles',
                'description' => 'This group allows users read-only access to see a list of all groups that they are not a member of, and all roles that are associated with each group. This will not expose Personal Identifiable Information (PII). This group is designed for organizations that allow users to create a self-service request to join a group that should see roles but not members associated.',
                'roles' => [
                    'auth-groups-global-list-roles',
                ]
            ],
            'auth-groups-global-viewer' => [
                'name' => 'Authentication Groups - Global Viewers',
                'slug' => 'auth-groups-global-viewer',
                'description' => 'This group allows users read-only access to see a list of all groups that they are not a member of, the roles associated with each group, and all members that are associated with each group. This will expose Personal Identifiable Information (PII) including the name, job title, and email address of group members. This group is designed for organizations that allow users to create a self-service request to join a group that should see members and roles associated.',
                'roles' => [
                    'auth-groups-global-viewer',
                ]
            ],
            'auth-roles-global-admin' => [
                'name' => 'Authentication Roles - Global Admins',
                'slug' => 'auth-roles-global-admin',
                'description' => 'This group allows users to administer all roles and permissions. This group is restricted to trusted IT security staff and system administrators.',
                'roles' => [
                    'auth-roles-global-admin',
                ]
            ],
            'auth-service-accounts-global-admin' => [
                'name' => 'Authentication Service Accounts - Global Admins',
                'slug' => 'auth-service-accounts-global-admin',
                'description' => 'This group allows users to manage all service accounts, including association of roles and permissions. This group is restricted to trusted IT security staff and system administrators.',
                'roles' => [
                    'auth-service-accounts-global-admin',
                ]
            ],
            'cloud-account-group' => [
                'name' => 'Cloud Account Group',
                'slug' => 'cloud-account-group',
                'description' => 'This group provides users with access to the features for creating and managing and cloud accounts in organization units they have access to.',
                'roles' => [
                    'cloud-account-role'
                ],
            ],
            'user-baseline-group' => [
                'name' => 'User - Baseline Group',
                'slug' => 'user-baseline-group',
                'description' => 'This group provides users with baseline roles and permissions for accessing the User UI and any additional roles that all users can access. This group is assigned to all users by default when they sign in.',
                'roles' => [
                    'user-baseline-role',
                ]
            ],
        ];

        foreach($default_groups as $group_slug => $group_values) {

            // Check if group already exists
            $group = Models\Auth\AuthGroup::query()
                ->where('slug', $group_slug)
                ->first();

            // If group does not exist, create new group
            if(!$group) {

                $group = $authGroupService->store([
                    'auth_tenant_id' => $default_tenant->id,
                    'name' => $group_values['name'],
                    'slug' => $group_values['slug'],
                    'description' => $group_values['description']
                ]);

                // Loop through all roles associated with group
                foreach($group_values['roles'] as $role_slug) {

                    $role = Models\Auth\AuthRole::query()
                        ->where('slug', $role_slug)
                        ->firstOrFail();

                    // Attach role to group
                    $group_role = $authGroupRoleService->store([
                        'auth_tenant_id' => $default_tenant->id,
                        'auth_group_id' => $group->id,
                        'auth_role_id' => $role->id,
                    ]);

                }
            }
        }

        //
        // Attach user baseline group to each authentication provider
        // --------------------------------------------------------------------
        // When a user signs in, they need to have a role that defines which
        // routes that they can access. We use the user-baseline-group group
        // that has the user-baseline-role and user.* route permissions.
        //
        // We need to attach this as a default group that gets associated with
        // all users when they sign in with an authentication provider.
        //

        // Get user baseline group
        $user_baseline_group = Models\Auth\AuthGroup::query()
            ->where('auth_tenant_id', $default_tenant->id)
            ->where('slug', 'user-baseline-group')
            ->firstOrFail();

        // Get list of authentication providers for default tenant
        $auth_providers = Models\Auth\AuthProvider::query()
            ->where('auth_tenant_id', $default_tenant->id)
            ->get();

        // Loop through each authentication provider
        foreach($auth_providers as $provider) {

            // Check if provider group exists
            $existing_group = Models\Auth\AuthProviderGroup::query()
                ->where('auth_tenant_id', $default_tenant->id)
                ->where('auth_provider_id', $provider->id)
                ->where('auth_group_id', $user_baseline_group->id)
                ->where('type', 'default')
                ->first();

            if(!$existing_group) {

                $provider_group = $authProviderGroupService->store([
                    'auth_tenant_id' => $default_tenant->id,
                    'auth_provider_id' => $provider->id,
                    'auth_group_id' => $user_baseline_group->id,
                    'type' => 'default'
                ]);

            } // if(!$existing_group)
        } // foreach($auth_providers

    }
}
